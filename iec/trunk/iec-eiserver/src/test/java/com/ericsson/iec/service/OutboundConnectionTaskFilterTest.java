package com.ericsson.iec.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.energyict.mdc.channels.ip.InboundIpConnectionType;
import com.energyict.mdc.channels.ip.datagrams.OutboundUdpConnectionType;
import com.energyict.mdc.channels.ip.socket.OutboundTcpIpConnectionType;
import com.energyict.mdc.tasks.OutboundConnectionTask;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class OutboundConnectionTaskFilterTest {

	@Mock
	OutboundTcpIpConnectionType outboundTcpIpConnectionTypeMock;
	
	@Mock
	OutboundUdpConnectionType outboundUdpConnectionTypeMock;
	
	@Mock
	OutboundConnectionTask outboundConnectionTaskMock1;
	
	@Mock
	OutboundConnectionTask outboundConnectionTaskMock2;
	
	
	@Test
	public void testFilterOutboundTcpIpConnectionsWithOneOutboundTcpIpConnection() {
		List<OutboundConnectionTask> challenge = new ArrayList<OutboundConnectionTask>();
		Mockito.when(outboundConnectionTaskMock1.getConnectionType()).thenReturn(outboundTcpIpConnectionTypeMock);
		challenge.add(outboundConnectionTaskMock1);
		OutboundConnectionTaskFilter filter = new OutboundConnectionTaskFilter();

		Assert.assertEquals(1, filter.filterOutboundTcpIpConnections(challenge).size());
	}
	
	@Test
	public void testFilterOutboundTcpIpConnectionsWithNoOutboundConnections() {
		List<OutboundConnectionTask> challenge = new ArrayList<OutboundConnectionTask>();
		OutboundConnectionTaskFilter filter = new OutboundConnectionTaskFilter();

		Assert.assertEquals(0, filter.filterOutboundTcpIpConnections(challenge).size());
	}
	
	@Test
	public void testFilterOutboundTcpIpConnectionsWithOneOutboundUdpConncetions() {
		List<OutboundConnectionTask> challenge = new ArrayList<OutboundConnectionTask>();
		Mockito.when(outboundConnectionTaskMock1.getConnectionType()).thenReturn(outboundUdpConnectionTypeMock);
		challenge.add(outboundConnectionTaskMock1);
		OutboundConnectionTaskFilter filter = new OutboundConnectionTaskFilter();

		Assert.assertEquals(0, filter.filterOutboundTcpIpConnections(challenge).size());
	}
	
	@Test
	public void testFilterOutboundTcpIpConnectionsWithTwoOutboundTcpIpConncetions() {
		List<OutboundConnectionTask> challenge = new ArrayList<OutboundConnectionTask>();
		Mockito.when(outboundConnectionTaskMock1.getConnectionType()).thenReturn(outboundTcpIpConnectionTypeMock);
		Mockito.when(outboundConnectionTaskMock2.getConnectionType()).thenReturn(outboundTcpIpConnectionTypeMock);
		challenge.add(outboundConnectionTaskMock1);
		challenge.add(outboundConnectionTaskMock2);
		OutboundConnectionTaskFilter filter = new OutboundConnectionTaskFilter();

		Assert.assertEquals(2, filter.filterOutboundTcpIpConnections(challenge).size());
	}
	
	@Test
	public void testFilterOutboundTcpIpConnectionsWithOneOutboundTcpIpConncetionsAndOneOutboundUdpConnection() {
		List<OutboundConnectionTask> challenge = new ArrayList<OutboundConnectionTask>();
		Mockito.when(outboundConnectionTaskMock1.getConnectionType()).thenReturn(outboundTcpIpConnectionTypeMock);
		Mockito.when(outboundConnectionTaskMock2.getConnectionType()).thenReturn(outboundUdpConnectionTypeMock);
		challenge.add(outboundConnectionTaskMock1);
		challenge.add(outboundConnectionTaskMock2);
		OutboundConnectionTaskFilter filter = new OutboundConnectionTaskFilter();

		Assert.assertEquals(1, filter.filterOutboundTcpIpConnections(challenge).size());
	}
}

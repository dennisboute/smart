package com.ericsson.iec.service;

import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.GenericMeterDevice;

public interface MeterDeviceUpdateService {

	void updateIpAddress(GenericMeterDevice device, String ipAddress) throws IecException;

}

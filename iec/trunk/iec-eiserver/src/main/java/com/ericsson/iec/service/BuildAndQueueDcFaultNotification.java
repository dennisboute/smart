package com.ericsson.iec.service;

import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.model.DataConcentrator;
import com.ericsson.iec.model.DataConcentratorDeployment;

import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;


public class BuildAndQueueDcFaultNotification extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment> {

    public static final String TRANSITION_NAME = "Build And Queue DC Fault Notification";

    public BuildAndQueueDcFaultNotification(Logger logger) {
        super(logger);
    }

    @Override
    protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
        try {
            DcFaultNotificationRequestBuilder builder = new DcFaultNotificationRequestBuilder();
            String serialId = dataConcentratorDeployment.getDataConcentrator().getSerialId();
            String deploymentStatus = dataConcentratorDeployment.getDeploymentStatus();
            ResponseMessage responseMessage = builder.build(serialId, "TO DO", deploymentStatus);
            ServiceRequest serviceRequest = ((SapResponseMessage) responseMessage).getServiceRequest();
            dataConcentratorDeployment.setServiceRequest(serviceRequest);
            new IecClassInjector().getMessageSender().sendMessage(responseMessage);
        } catch (IecException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
        }
    }

    @Override
    protected String getTransitionName() {
        return TRANSITION_NAME;
    }

}

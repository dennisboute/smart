package com.ericsson.iec.mdus.handler.servicerequest;

import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.requesthandler.AbstractBulkRequestHandler;
import com.ericsson.iec.mdus.IecMarshallerHelper;

public abstract class IecBulkRequestHandler<R> extends AbstractBulkRequestHandler<R, R, MarshallingException> {

	public IecBulkRequestHandler() throws MarshallingException {
		super(IecMarshallerHelper.getInstance());
	}
}

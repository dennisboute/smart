package com.ericsson.iec.model;

import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceWrapperImpl;

public class GenericMeterDeviceImpl extends DeviceWrapperImpl implements GenericMeterDevice {

	private static final long serialVersionUID = 2647039815641819991L;

	public GenericMeterDeviceImpl(DeviceAdapter deviceAdapter) {
		super(deviceAdapter);
	}
}

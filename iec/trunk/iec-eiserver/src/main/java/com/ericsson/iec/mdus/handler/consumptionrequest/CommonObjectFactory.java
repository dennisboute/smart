package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_VALIDATION_FAILED;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.energyict.coordinates.DegreesWorldCoordinate;
import com.energyict.coordinates.SpatialCoordinates;
import com.energyict.mdw.core.Folder;
import com.energyict.mdw.core.FolderType;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.model.CellularGrid;
import com.ericsson.iec.model.City;
import com.ericsson.iec.model.CtMeterGrid;
import com.ericsson.iec.model.DataConcentrator;
import com.ericsson.iec.model.District;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.MeterLocation;
import com.ericsson.iec.model.PlcGrid;
import com.ericsson.iec.model.PodAssignment;
import com.ericsson.iec.model.PointOfDelivery;
import com.ericsson.iec.model.Premise;
import com.ericsson.iec.model.Region;
import com.ericsson.iec.model.Transformer;
import com.ericsson.iec.model.TransformerStation;
import com.ericsson.iec.service.DataConcentratorDeviceUpdateService;

public class CommonObjectFactory {

	public enum ActionType {
		INSERT("Insert"),
		UPDATE("Update"),
		REMOVE("Remove"),
		;
		
		private String actionWsName;
		
		ActionType(String actionName) {
			actionWsName = actionName;
		}
		
		public String getActionName() {
			return actionWsName;
		}
		
		public static ActionType getActionByName(String actionName) {
			for (ActionType a : ActionType.values()) {
				if (a.getActionName().equals(actionName))
					return a;
			}
			return null;
		}
	}

	//------------------------------------------------------------------------------------------------------

	public static FolderType getDataConcentratorFolderType() {
		return IecWarehouse.getInstance().getDataConcentratorFactory().getFolderType();
	}

	public static FolderType getPremiseFolderType() {
		return IecWarehouse.getInstance().getPremiseFactory().getFolderType();
	}

	public static FolderType getMeterLocationFolderType() {
		return IecWarehouse.getInstance().getMeterLocationFactory().getFolderType();
	}

	public static FolderType getPlcMeterFolderType() {
		return IecWarehouse.getInstance().getPlcMeterFactory().getFolderType();
	}

	public static FolderType getCellularMeterFolderType() {
		return IecWarehouse.getInstance().getCellularMeterFactory().getFolderType();
	}

	//------------------------------------------------------------------------------------------------------

	public static District getDistrict(String districtCode) {
		return IecWarehouse.getInstance().getDistrictFactory().findByKey(districtCode);
	}
	
	public static Region getRegion(String regionCode) {
		return IecWarehouse.getInstance().getRegionFactory().findByKey(regionCode);
	}

	public static City getCity(String cityCode) {
		return IecWarehouse.getInstance().getCityFactory().findByKey(cityCode);
	}

	public static TransformerStation getTransformerStation(String stationNumerator) {
		return IecWarehouse.getInstance().getTransformerStationFactory().findByKey(stationNumerator);
	}

	public static Transformer getTransformer(String transformerExternalName) {
		return IecWarehouse.getInstance().getTransformerFactory().findByKey(transformerExternalName);
	}
	
	public static DataConcentrator getDataConcentrator(String dcId) {
		return IecWarehouse.getInstance().getDataConcentratorFactory().findByKey(dcId);
	}

	public static DataConcentratorDeviceUpdateService getDataConcentratorDeviceUpdateService() {
		return IecWarehouse.getInstance().getIecServiceProvider().getDataConcentratorDeviceUpdateService();
	}

	public static Premise getPremise(String premiseCode) {
		return IecWarehouse.getInstance().getPremiseFactory().findByKey(premiseCode);
	}

	public static GenericMeter getMeter(String deviceId) {
		return IecWarehouse.getInstance().getMeterFactory().findByKey(deviceId);
	}
	
	public static MeterLocation getMeterLocation(String locationId) {
		return IecWarehouse.getInstance().getMeterLocationFactory().findByKey(locationId);
	}

	public static PointOfDelivery getPointOfDelivery(String podId) {
		return IecWarehouse.getInstance().getPointOfDeliveryFactory().findByKey(podId);
	}

	public static List<PodAssignment> getPodAssignment(GenericMeter meter) {
		List<PodAssignment> podAssignmentList = IecWarehouse.getInstance().getPodAssignmentFactory().findByReferences(meter);
		return podAssignmentList;
	}

	public static CellularGrid getCellularGrid(String cellularGridId) {
		return IecWarehouse.getInstance().getCellularGridFactory().findByKey(cellularGridId);
	}

	public static PlcGrid getPlcGrid(String plcGridId) {
		return IecWarehouse.getInstance().getPlcGridFactory().findByKey(plcGridId);
	}

	public static CtMeterGrid getCtMeterGrid(String ctMeterGridId) {
		return IecWarehouse.getInstance().getCtMeterGridFactory().findByKey(ctMeterGridId);
	}

	
	public static int getTransformerCountByTs(TransformerStation ts) {
//		int transformerCount = (ts != null) ? IecWarehouse.getInstance().getTransformerFactory().findByTransformerStation(ts).size() : 0;
		int transformerCount = (ts != null) ? ts.getFolder().findChildren(IecWarehouse.getInstance().getTransformerFactory().getFolderType()).size() : 0;
		return transformerCount;
	}

	public static int getDcCountByTransformer(Transformer transformer) {
		int dcCount = (transformer != null) ? transformer.getFolder().findChildren(IecWarehouse.getInstance().getDataConcentratorFactory().getFolderType()).size() : 0;
		return dcCount;
	}

	public static int getPremiseCountByFolder(Folder folder) {
		int premiseCount = (folder != null) ? folder.findChildren(IecWarehouse.getInstance().getPremiseFactory().getFolderType()).size() : 0;
		return premiseCount;
	}

	public static int getMeterCountByFolder(Folder folder) {
//		FolderType plcMeterFolderType = IecWarehouse.getInstance().getPlcMeterFactory().getFolderType();
//		FolderType cellularMeterFolderType = IecWarehouse.getInstance().getCellularMeterFactory().getFolderType();

		int plcMeterCount = (folder != null) ? folder.findChildren(IecWarehouse.getInstance().getPlcMeterFactory().getFolderType()).size() : 0;
		int cellularMeterCount = (folder != null) ? folder.findChildren(IecWarehouse.getInstance().getCellularMeterFactory().getFolderType()).size() : 0;

		int meterCount = plcMeterCount + cellularMeterCount;
		return meterCount;
	}

	public static Folder calcPremiseParentFolder(Premise premise) {
		String transformerExternalName = premise.getTransformer().getExternalName();
		String externalNameSuffix = transformerExternalName.substring(transformerExternalName.indexOf("/") + 1);
		if (premise.getMeterUsageType().equals("NU")) {
			if (premise.getPremiseType().equals("C"))
				return CommonObjectFactory.getCellularGrid(externalNameSuffix).getFolder();
			else // "P"
				return CommonObjectFactory.getPlcGrid(externalNameSuffix).getFolder();
		} else { // "CT"
			return CommonObjectFactory.getCtMeterGrid(externalNameSuffix).getFolder();
		}
	}

	public static String getTransformerExternalName(String transformerName, TransformerStation ts) {
		if (ts != null)
			return ts.getExternalName().substring(ts.getExternalName().indexOf("/") + 1) + "/" + transformerName;
		return null;
	}
	
	public static SpatialCoordinates getCoordinates(BigDecimal coordinateX, BigDecimal coordinateY) {
		SpatialCoordinates sc = new SpatialCoordinates();
		sc.setLatitude(new DegreesWorldCoordinate(coordinateX));
		sc.setLongitude(new DegreesWorldCoordinate(coordinateY));
		return sc;
	}

	public static Date parseDateString(String dateStr) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.parse(dateStr);
	}
	
	public static Date getMaxDate() {
		try {
			return parseDateString("31/12/9999");
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static XMLGregorianCalendar renderXMLGregorianCalendar(Date date, String fieldName) throws AbstractValidationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			throw new FatalValidationException(e, MESSAGE_VALIDATION_FAILED, "date parsing failed for field " + fieldName, date.toString());
		}
	}
}

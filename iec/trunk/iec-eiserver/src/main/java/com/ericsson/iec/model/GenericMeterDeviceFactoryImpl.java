package com.ericsson.iec.model;

import com.energyict.mdw.core.Device;
import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceWrapperFactoryImpl;
import com.energyict.projects.dateandtime.Clock;

public class GenericMeterDeviceFactoryImpl extends DeviceWrapperFactoryImpl<GenericMeterDevice> implements GenericMeterDeviceFactory {

	public GenericMeterDeviceFactoryImpl() {
		super(new IecDeviceAdapterFactory());
	}
	
	@Override
	public GenericMeterDevice createNew(DeviceAdapter adapter) {
		return new GenericMeterDeviceImpl(adapter);
	}

	public GenericMeterDevice findByDevice(Device device) {
		return new GenericMeterDeviceImpl(new IecDeviceAdapterFactory().createNew(device, Clock.INSTANCE.get().now()));
	}

}

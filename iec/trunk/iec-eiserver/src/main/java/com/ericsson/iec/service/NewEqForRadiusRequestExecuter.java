package com.ericsson.iec.service;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

import com.ericsson.iec.core.exception.AbstractExecutionException;
import com.ericsson.iec.core.exception.FatalExecutionException;

import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusResponse;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.ObjectFactory;
import iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_service.BasicFaultMessage;
import iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_service.WSMDMRAD01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_rad_01_service.WSMDMRAD01Service;

public class NewEqForRadiusRequestExecuter {

    private ObjectFactory objectFactory = new ObjectFactory();

    public String getMeterAddressFromRadius(String meterSerialId) throws FatalExecutionException {
    	try {
	        NewEqForRadiusRequest newEqForRadiusRequest = objectFactory.createNewEqForRadiusRequest();
	        NewEqForRadiusRequest.Request newEqForRadiusRequestRequest = objectFactory.createNewEqForRadiusRequestRequest();
	        NewEqForRadiusRequest.Request.EquipmentList equipmentList = objectFactory.createNewEqForRadiusRequestRequestEquipmentList();

	        equipmentList.setSerialID(meterSerialId);
	        newEqForRadiusRequestRequest.getEquipmentList().add(equipmentList);
	        newEqForRadiusRequest.setRequest(newEqForRadiusRequestRequest);
			
			WSMDMRAD01Service service = new WSMDMRAD01Service();
			WSMDMRAD01PortType port = service.getWSMDMRAD01PortTypeEndpoint();
			NewEqForRadiusResponse newEqForRadiusResponse = port.newEqForRadiusOperation(newEqForRadiusRequest);
			
			NewEqForRadiusResponse.Response.EquipmentList responseEquipmentList = newEqForRadiusResponse.getResponse().getEquipmentList().get(0);
			String meterAddress = responseEquipmentList.getIP();
			String errorCode = responseEquipmentList.getErrorCode();
			String errorDescription = responseEquipmentList.getErrorDescription();
			if (!errorCode.equals("0")) {
				throw new FatalExecutionException(MESSAGE_PROCESSING_FAILED, "Radius message for Meter", meterSerialId + "', Error: " + errorCode + ", Description: '" + errorDescription);
			}
        	return meterAddress;
        } catch (BasicFaultMessage | AbstractExecutionException e) {
            throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", meterSerialId);
        }
    }
}

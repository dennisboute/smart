package com.ericsson.iec.mdus.handler.consumptionrequest;

import java.util.Arrays;

public class AllowedValues {
	
	private Object[] allowedValues;
	
	public AllowedValues(Object... allowedValues) {
		this.allowedValues = allowedValues;
	}
	
	public boolean contains(Object value) {
		return Arrays.asList(allowedValues).contains(value);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (Object value : allowedValues) {
			if (!builder.toString().equals("[")) {
				builder.append(",");
			}
			builder.append(value.toString());
		}
		builder.append("]");
		return builder.toString();
	}
}

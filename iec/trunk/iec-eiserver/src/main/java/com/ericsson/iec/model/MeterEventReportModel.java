package com.ericsson.iec.model;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MeterEventReportModel {
    private String utilitiesDeviceId;
    private String eventCode;
    private String additionalInfo;
    private Date date;
    private String logicalEventSubGroup;
    private String logicalEventGroup;

    public String getUtilitiesDeviceId() {
        return utilitiesDeviceId;
    }

    public void setUtilitiesDeviceId(String utilitiesDeviceId) {
        this.utilitiesDeviceId = utilitiesDeviceId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Date getDate() {
        return date;
    }

    public XMLGregorianCalendar getXmlGregorianCalendar() {
        XMLGregorianCalendar xmlGregorianCalendar = null;
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date);
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
        }
        return xmlGregorianCalendar;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogicalEventSubGroup() {
        return logicalEventSubGroup;
    }

    public void setLogicalEventSubGroup(String logicalEventSubGroup) {
        this.logicalEventSubGroup = logicalEventSubGroup;
    }

    public String getLogicalEventGroup() {
        return logicalEventGroup;
    }

    public void setLogicalEventGroup(String logicalEventGroup) {
        this.logicalEventGroup = logicalEventGroup;
    }
}

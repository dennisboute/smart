package com.ericsson.iec.model;

import com.energyict.projects.common.model.relation.RelationWrapperFactory;

public interface NocEventDetailsFactory extends RelationWrapperFactory<NocEventDetails> {
    NocEventDetails findByCode(String eventCodeAndLogicalEventGroup);
}

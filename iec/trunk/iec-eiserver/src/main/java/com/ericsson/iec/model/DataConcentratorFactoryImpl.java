package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class DataConcentratorFactoryImpl extends FolderVersionWrapperFactoryImpl<DataConcentrator> implements DataConcentratorFactory {

	public DataConcentratorFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public DataConcentratorFactoryImpl() {
		super();
	}

	@Override
	public DataConcentrator createNew(FolderVersionAdapter arg0) {
		return new DataConcentratorImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.DATA_CONCENTRATOR.name;
	}
	
	@Override
	public DataConcentrator findByKey(String key) {
		return findByExternalName(FolderTypes.DATA_CONCENTRATOR.buildExternalName(key), new Date());
	}
}

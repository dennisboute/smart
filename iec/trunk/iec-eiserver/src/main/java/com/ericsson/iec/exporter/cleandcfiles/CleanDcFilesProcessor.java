package com.ericsson.iec.exporter.cleandcfiles;

import com.energyict.cbo.BusinessException;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.core.UserFile;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.logging.Logger;

public class CleanDcFilesProcessor {
    private Logger logger;

    public CleanDcFilesProcessor(Logger logger) {
        this.logger = logger;
    }

    public void cleanDcFiles(Integer retentionDaysDcFiles, Integer folderId) throws SQLException, BusinessException {
        LocalDate dateFromWhenFilesCanBeRemoved = LocalDate.now().minusDays(retentionDaysDcFiles);
        List<UserFile> userFiles = MeteringWarehouse.getCurrent().getUserFileFactory().findByFolder(folderId);
        for (UserFile userFile : userFiles) {
            LocalDate modDate = userFile.getModDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (modDate.isBefore(dateFromWhenFilesCanBeRemoved)) {
                userFile.delete();
                logger.info("User File " + userFile.getName() + " has been deleted.");
            }
        }
    }

}

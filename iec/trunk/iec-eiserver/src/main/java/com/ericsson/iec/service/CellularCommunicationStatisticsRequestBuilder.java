package com.ericsson.iec.service;

import java.io.StringWriter;
import java.util.Collections;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusResourceBusyException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.MdusWebservice;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.CellularCommunicationStatisticsRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.CellularCommunicationStatisticsRequest.Request;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;

public class CellularCommunicationStatisticsRequestBuilder {

	private ObjectFactory objectFactory;
	private ClassInjector classInjector;

	public CellularCommunicationStatisticsRequestBuilder() {
	}
	
	public SapResponseMessage build(Request request) throws SystemObjectNotDefined, SystemParameterIncorrectValue, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
		ServiceRequest serviceRequest = new ServiceRequestCreator().createServiceRequest(MdusWebservice.CELLULAR_COMMUNICATION_STATISTICS_REQUEST, UUID.randomUUID().toString(), Collections.emptyList(), null);
		CellularCommunicationStatisticsRequest cellularCommunicationStatisticsRequest = buildCellularCommunicationStatisticsRequest();
		cellularCommunicationStatisticsRequest.setRequest(request);
		String message = marshall(cellularCommunicationStatisticsRequest);
		SapResponseMessage responseMessage = new SapResponseMessage(MdusWebservice.CELLULAR_COMMUNICATION_STATISTICS_REQUEST, message, serviceRequest.getId());
		return responseMessage;
	}
	
	private CellularCommunicationStatisticsRequest buildCellularCommunicationStatisticsRequest() {
		return getSEObjectFactory().createCellularCommunicationStatisticsRequest();
	}
	
	private String marshall(CellularCommunicationStatisticsRequest cellularCommunicationStatisticsRequest) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(CellularCommunicationStatisticsRequest.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();
			marshaller.marshal(cellularCommunicationStatisticsRequest, writer);
			return writer.toString();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unused")
	private ClassInjector getClassInjector() {
		if (classInjector == null) {
			classInjector = new IecClassInjector();
		}
		return classInjector;
	}
	
	private ObjectFactory getSEObjectFactory() {
		if (objectFactory == null) {
			objectFactory = new ObjectFactory();
		}
		return objectFactory;
	}
}

package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.exceptionhandling.ProcessCaseWrapperWithExceptionHandling;
import com.ericsson.iec.model.GenericMeter;

public interface PlcMeterRegistration extends ProcessCaseWrapperWithExceptionHandling {

	void setRegistrationStatus(String registrationStatus);
	String getRegistrationStatus();

	void setMeter(GenericMeter meter);
	GenericMeter getMeter();
}

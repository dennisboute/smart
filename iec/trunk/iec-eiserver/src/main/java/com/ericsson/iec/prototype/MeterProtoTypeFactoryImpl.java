package com.ericsson.iec.prototype;

import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypes;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public class MeterProtoTypeFactoryImpl extends AbstractProtoTypeFactoryImpl implements MeterProtoTypeFactory {

	private String meterModel;
	
	public MeterProtoTypeFactoryImpl(String meterModel) {
		super();
		this.meterModel = meterModel;
	}
	
	@Override
	public MeterProtoType find() throws FatalConfigurationException {
		return new MeterProtoTypeImpl(getCopySpec());
	}

	@Override
	public String getCopySpecName() {
		return ProtoTypes.METER.getName() + meterModel; // SMP/512, SMP/061, ...
	}

	@Override
	public ProtoTypeObjectType getObjectType() {
		try {
			String folderTypeName = getCopySpec().getFolder().getFolderType().getName();
			return (folderTypeName.equals("CellularMeter")) ? ProtoTypeObjectType.CELLULAR_METER : ProtoTypeObjectType.PLC_METER;
		} catch (FatalConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getMeterModel() {
		return meterModel;
	}
}

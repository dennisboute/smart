package com.ericsson.iec.constants;

public enum FlagConstants {
    PROCESSED(0),
    SEND(1),
    EVENT_RECEIVED_FROM_NOC(2);

    private int bitIndex;

    FlagConstants(int bitIndex) {
        this.bitIndex = bitIndex;
    }

    public int getBitIndex() {
        return bitIndex;
    }
}

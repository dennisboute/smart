package com.ericsson.iec.service;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.mdw.core.MeteringWarehouse;
import com.ericsson.iec.model.DataConcentratorDeployment;

import java.util.logging.Logger;

public class CheckForS15Message {
	
	public static final String TRANSITION_NAME = "Check For S15";
	
	private Logger logger;
	
	public CheckForS15Message(Logger logger) {
		this.logger = logger;
	}

	public final boolean process(DataConcentratorDeployment dataConcentratorDeployment) {
        DeviceEventFilter filter = new DeviceEventFilter();
        filter.setDeviceCode(1);
        filter.setLogBookType(MeteringWarehouse.getCurrent().getLogBookTypeFactory().find(120));
        Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
        return !device.getEvents(filter).isEmpty();
	}

}

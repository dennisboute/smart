package com.ericsson.iec.exporter.meterkey;

import com.energyict.mdw.core.Device;
import com.ericsson.iec.model.GenericMeter;

class MeterKeyModel {
    private GenericMeter genericMeter;
    private String authenticationKey;
    private String encryptionKey;

    public GenericMeter getGenericMeter() {
        return genericMeter;
    }

    public void setGenericMeter(GenericMeter genericMeter) {
        this.genericMeter = genericMeter;
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }
}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

import java.math.BigDecimal;

public interface EventGroupMapping extends FolderVersionWrapper {

	BigDecimal getEventGroup();
}

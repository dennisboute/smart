package com.ericsson.iec.exporter.meterkey;

import com.energyict.cbo.BusinessException;
import com.energyict.mdw.export.ActionHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

class MeterKeyShipmentWriter implements MeterKeyWriter {
    private Logger logger;
    private  ActionHandler actionHandler;

    public MeterKeyShipmentWriter(Logger logger, ActionHandler actionHandler) {
        this.logger = logger;
        this.actionHandler = actionHandler;
    }

    public void write(MeterKeyItem meterKeyItem) throws IOException, SQLException, BusinessException {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = getRootElement(doc);

            Element header = doc.createElement("Header");
            header.setTextContent("");
            rootElement.appendChild(header);

            Element body = doc.createElement("Body");
            rootElement.appendChild(body);

            Element signature = doc.createElement("Signature");
            rootElement.appendChild(signature);

            for (MeterKeyModel model : meterKeyItem.getMeterKeyModels()) {
                Element device = doc.createElement("Device");
                body.appendChild(device);

                Element serialNumber = doc.createElement("SerialNumber");
                serialNumber.setTextContent(model.getGenericMeter().getDevice().getDevice().getSerialNumber());
                device.appendChild(serialNumber);

                Element key1 = doc.createElement("Key");
                key1.setAttribute("name", "GUEK");
                key1.setAttribute("WrapKeyLabel", "GUEK_Wrap_Key");
                device.appendChild(key1);

                Element xencCipherData1 = doc.createElement("xenc:CipherData");
                key1.appendChild(xencCipherData1);

                Element xencCipherValue1 = doc.createElement("xenc:CipherValue");
                xencCipherValue1.setTextContent(model.getEncryptionKey());
                xencCipherData1.appendChild(xencCipherValue1);

                Element key2 = doc.createElement("Key");
                key2.setAttribute("name", "GAK");
                key2.setAttribute("WrapKeyLabel", "GAK_Wrap_Key");
                device.appendChild(key2);

                Element xencCipherData2 = doc.createElement("xenc:CipherData");
                key2.appendChild(xencCipherData2);

                Element xencCipherValue2 = doc.createElement("xenc:CipherValue");
                xencCipherValue2.setTextContent(model.getAuthenticationKey());
                xencCipherData2.appendChild(xencCipherValue2);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            actionHandler.startNewFile("ShipmentFile_");
            StreamResult result = new StreamResult(actionHandler.getPrintWriter());
            transformer.transform(source, result);
        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

    private Element getRootElement(Document doc) {
        Element rootElement = doc.createElement("Shipment");
        rootElement.setAttribute("xmlns:xenc", "http://www.w3.org/2001/04/xmlenc#");
        rootElement.setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
        rootElement.setAttribute("xmlns", "http://customization.elster.com/shipment");
        doc.appendChild(rootElement);
        return rootElement;
    }
}

package com.ericsson.iec.mdus.responder;

import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.ericsson.iec.mdus.MdusSapEndpoint;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.HandleNotificationMeterDataRequest;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.BasicFaultMessage;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;

public abstract class HandleNotificationRequestResponder extends IecMdusSapResponder<WSMDMNOC01ServicesServiceagent, WSMDMNOC01PortType> {


    public HandleNotificationRequestResponder(MdusSapEndpoint mdusSapEndpoint) throws MarshallingException {
        super(mdusSapEndpoint, WSMDMNOC01ServicesServiceagent.class, WSMDMNOC01PortType.class);
    }

    @Override
    public void respond(ResponseMessage message) {

        try {
            HandleNotificationMeterDataRequest handleNotificationMeterDataRequest = unmarshal(message.getMessage(), HandleNotificationMeterDataRequest.class);
            updateOutputRequest(message);
            sendMessage(handleNotificationMeterDataRequest);
            markServiceRequestSuccess(message);

        } catch (Exception e) {
            try {
                markServiceRequestFailed(message);
                logError(message, e.getMessage());
            } catch (Exception ex) {
                //ToDo If we throw an exception the jobServer does system.exit(1).
            }
        }

    }

    protected abstract void sendMessage(HandleNotificationMeterDataRequest handleNotificationMeterDataRequest) throws com.energyict.cbo.BusinessException, BasicFaultMessage;
}

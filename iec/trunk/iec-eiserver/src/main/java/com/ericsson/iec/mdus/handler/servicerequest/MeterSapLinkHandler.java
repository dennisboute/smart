package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest;

public class MeterSapLinkHandler extends IecSingleRequestHandler<MeterSapLinkRequest>{

	public MeterSapLinkHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		MeterSapLinkRequest request = unmarshall(sapRequestMessage.getMessage(), MeterSapLinkRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<MeterSapLinkRequest> parameters = 
				new RequestHandlerParameters<MeterSapLinkRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<MeterSapLinkRequest> parameters) throws BusinessException {
		MeterSapLinkRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(MeterSapLinkRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForMeterSapLinkRequest(singleRequest);
		
		shadow.setName(singleRequest.getMessageContent().getDeviceId().toString());
		shadow.setExternalName(singleRequest.getMessageContent().getDeviceId().toString());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(MeterSapLinkRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
				
		return shadow;
	}
}

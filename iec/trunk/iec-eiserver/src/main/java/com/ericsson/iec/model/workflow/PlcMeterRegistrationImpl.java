package com.ericsson.iec.model.workflow;

import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.METER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.REGISTRATION_STATUS;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.ProcessCaseWithExceptionHandling;


public class PlcMeterRegistrationImpl extends ProcessCaseWithExceptionHandling implements PlcMeterRegistration {

	private static final long serialVersionUID = -2801903355948795694L;

	protected PlcMeterRegistrationImpl(ProcessCaseAdapter adapter) {
		super(adapter);
	}
	
	@Override
	public GenericMeter getMeter() {
		return getFolderVersionWrapperAttribute(METER.name, IecWarehouse.getInstance().getPlcMeterFactory());
	}

	@Override
	public void setMeter(GenericMeter meter) {
		setFolderVersionWrapperAttribute(METER.name, meter);
	}
	
	@Override
	public String getRegistrationStatus() {
		return getStringAttribute(REGISTRATION_STATUS.name);
	}
	
	@Override
	public void setRegistrationStatus(String registrationStatus) {
		setStringAttribute(REGISTRATION_STATUS.name, registrationStatus);
	}
}

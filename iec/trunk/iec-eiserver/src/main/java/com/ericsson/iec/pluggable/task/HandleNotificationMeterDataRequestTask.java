package com.ericsson.iec.pluggable.task;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.SqlBuilder;
import com.energyict.eisexport.core.AbstractExporter;
import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdw.core.DeviceEvent;
import com.energyict.mdw.core.LogBook;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.GenericMeterDevice;
import com.ericsson.iec.model.MeterEventReportModel;
import com.ericsson.iec.model.NocEventDetails;
import com.ericsson.iec.service.HandleNotificationPullRequestBuilder;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HandleNotificationMeterDataRequestTask extends AbstractExporter {

//    public static final String NUMBER_OF_EVENTS_TO_SEND_TO_NOC = "Number of events to send to noc";
//    public static final String NUMBER_OF_EVENTS_TO_SEND_TO_NOC_VALUE = "1000";
//    public static final String NUMBER_EVENTS_TO_FETCH_FROM_QUERY = "Number of events to fetch from query";
//    public static final String NUMBER_EVENTS_TO_FETCH_FROM_QUERY_VALUE = "1000";

    @Override
    protected void export() throws BusinessException, SQLException {
        List<MeterEventReportModel> meterEventReportModelList = new ArrayList<>();
        Integer numberOfEventsToSendToNoc = IecSystemParameters.MAX_PULL_MSG_TO_NOC.getIntValue();
//        Integer numberOfEventsToSendToNoc = parseIntProperty(getProperty(NUMBER_OF_EVENTS_TO_SEND_TO_NOC, NUMBER_OF_EVENTS_TO_SEND_TO_NOC_VALUE));
        List<DeviceEvent> listOfDeviceEvents;
        while (getListOfDeviceEvents().size() != 0) {
            listOfDeviceEvents = getListOfDeviceEvents();
            for (int i = 0; i < listOfDeviceEvents.size(); i++) {
                DeviceEvent deviceEvent = listOfDeviceEvents.get(i);
                deviceEvent.setFlag(0);
                NocEventDetails nocEventDetails = IecWarehouse.getInstance().getNocEventDetails(new BigDecimal(deviceEvent.getDeviceCode()),new BigDecimal(getEventGroup(deviceEvent)));
                if (nocEventDetails != null) {
                    GenericMeter genericMeter = getGenericMeter(deviceEvent);
                    if (genericMeter != null) {
                        MeterEventReportModel meterEventReportModel = createMeterEventReportModel(deviceEvent, genericMeter);
                        meterEventReportModelList.add(meterEventReportModel);
                        deviceEvent.setFlag(1);
                    }
                }
                if (meterEventReportModelList.size() == numberOfEventsToSendToNoc || (i == listOfDeviceEvents.size() - 1 && meterEventReportModelList.size() > 0)) {
                    sendToNoc(meterEventReportModelList);
                    meterEventReportModelList.clear();
                }
            }
        }
    }

    private List<DeviceEvent> getListOfDeviceEvents() {
        Integer numberOfEventsToSendToNoc = IecSystemParameters.NUMBER_OF_PULL_EVENTS_TO_HANDLE_AT_ONCE.getIntValue();
//        Integer numberOfEventsToSendToNoc = parseIntProperty(getProperty(NUMBER_EVENTS_TO_FETCH_FROM_QUERY, NUMBER_EVENTS_TO_FETCH_FROM_QUERY_VALUE));
        IecDeviceEventResultBuilder iecDeviceEventResultBuilder = new IecDeviceEventResultBuilder();
        SqlBuilder sqlBuilder = new SqlBuilder();
        sqlBuilder.append(" where flags = ? and rownum < ?");
        sqlBuilder.bindInt(0);
        sqlBuilder.bindInt(numberOfEventsToSendToNoc + 1);
        IecWarehouse.getInstance().getMeteringWarehouse().getDeviceEventFactory().findWhere(sqlBuilder, iecDeviceEventResultBuilder);
        return iecDeviceEventResultBuilder.getListOfDeviceEvents();
    }

    private MeterEventReportModel createMeterEventReportModel(DeviceEvent deviceEvent, GenericMeter genericMeter) {
        MeterEventReportModel meterEventReportModel = new MeterEventReportModel();
        String message = deviceEvent.getMessage();
        meterEventReportModel.setAdditionalInfo(message.substring(0, Math.min(254, message.length())));
        meterEventReportModel.setEventCode(String.valueOf(deviceEvent.getDeviceCode()));
        meterEventReportModel.setUtilitiesDeviceId(genericMeter.getSapId());
        meterEventReportModel.setDate(deviceEvent.getDate());
        meterEventReportModel.setLogicalEventGroup(getEventGroup(deviceEvent));
        meterEventReportModel.setLogicalEventSubGroup("0");
        return meterEventReportModel;
    }

    private String getEventGroup(DeviceEvent deviceEvent) {
        LogBook logBook = MeteringWarehouse.getCurrent().getLogBookFactory().find(deviceEvent.getLogBookId());
        return IecWarehouse.getInstance().getEventGroupMappingFactory().findByKey(logBook.getLogBookType().getName()).getEventGroup().toString();
    }

    private void sendToNoc(List<MeterEventReportModel> meterEventReportModelList) throws BusinessException {
        if (meterEventReportModelList.size() > 0) {
            HandleNotificationPullRequestBuilder builder = new HandleNotificationPullRequestBuilder();
            ResponseMessage responseMessage = builder.build(meterEventReportModelList);
            ServiceRequest serviceRequest = ((SapResponseMessage) responseMessage).getServiceRequest();
            new IecClassInjector().getMessageSender().sendMessage(responseMessage);
            getLogger().info("ServiceRequest: '" + serviceRequest.getExternalName() + "' has been created.");
        }
    }

    private GenericMeter getGenericMeter(DeviceEvent deviceEvent) {
        GenericMeterDevice genericMeterDevice = IecWarehouse.getInstance().getGenericMeterDeviceFactory().findByDevice(deviceEvent.getDevice());
        GenericMeter genericMeter = IecWarehouse.getInstance().getMeterFactory().findByGenericMeterDevice(genericMeterDevice);
        return genericMeter;
    }

//    private Integer parseIntProperty(String value) {
//        return Integer.valueOf(value);
//    }

//    @Override
//    public List<PropertySpec> getRequiredProperties() {
//        List<PropertySpec> requiredProperties = new ArrayList<>();
//        requiredProperties.add(PropertySpecFactory.stringPropertySpec(NUMBER_OF_EVENTS_TO_SEND_TO_NOC, NUMBER_OF_EVENTS_TO_SEND_TO_NOC_VALUE));
//        requiredProperties.add(PropertySpecFactory.stringPropertySpec(NUMBER_EVENTS_TO_FETCH_FROM_QUERY, NUMBER_EVENTS_TO_FETCH_FROM_QUERY_VALUE));
//        return requiredProperties;
//    }

    @Override
    public String getDescription() {
        return "Task that looks which events have to be sent to NOC.";
    }

    @Override
    public String getVersion() {
        return "";
    }
}

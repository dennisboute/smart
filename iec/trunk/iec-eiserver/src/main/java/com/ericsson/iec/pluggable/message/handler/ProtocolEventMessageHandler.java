package com.ericsson.iec.pluggable.message.handler;

import com.energyict.cbo.BusinessException;
import com.energyict.cim.*;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdc.protocol.queue.EventDataContainer;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdw.core.LogBook;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.messaging.MessageHandler;
import com.energyict.mdw.shadow.DeviceEventShadow;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.model.*;
import com.ericsson.iec.model.workflow.CellularMeterRegistration;
import com.ericsson.iec.model.workflow.PlcMeterRegistration;
import com.ericsson.iec.service.HandleNotificationPushRequestBuilder;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class ProtocolEventMessageHandler implements MessageHandler {

    private LogBook logBook;

    @Override
    public void processMessage(Message message, Logger logger) throws JMSException, BusinessException, SQLException {
        ObjectMessage objectMessage = (ObjectMessage) message;
        CheckIfInstanceofEventDataContainer(objectMessage);
    }

    private void CheckIfInstanceofEventDataContainer(ObjectMessage objectMessage) throws JMSException, BusinessException, SQLException {
        if (objectMessage.getObject() instanceof EventDataContainer) {
            EventDataContainer container = (EventDataContainer) objectMessage.getObject();
            GenericMeter genericMeter = IecWarehouse.getInstance().getMeterFactory().findBySerialId(container.getSlaveDeviceSerialNumber());
            if (container.getEventCode() == 1) {
                if (genericMeter instanceof PlcMeter) {
                    PlcMeterRegistration workflow = IecWarehouse.getInstance().getPlcMeterRegistrationFactory().createNew();
                    workflow.setMeter(genericMeter);
                    workflow.saveChanges();
                }
                if (genericMeter instanceof CellularMeter) {
                    CellularMeterRegistration workflow = IecWarehouse.getInstance().getCellularMeterRegistrationFactory().createNew();
                    workflow.setMeter(genericMeter);
                    workflow.saveChanges();
                }
            } else {
                MeterEventReportModel model = getMeterEventReportModel(container);
                sendToNoc(model);
                storeEventInLogbook(model);
            }
        }
    }

    private MeterEventReportModel getMeterEventReportModel(EventDataContainer container) {
        MeterEventReportModel model = new MeterEventReportModel();
        model.setUtilitiesDeviceId(container.getSlaveDeviceSerialNumber());
        model.setDate(container.getEventTimestamp());
        model.setEventCode(container.getEventCode() + "");
        model.setAdditionalInfo("");
        model.setLogicalEventSubGroup("0");
        container.getMasterDeviceSerialNumber();
        GenericMeter genericMeter = IecWarehouse.getInstance().getMeterFactory().findBySerialId(container.getSlaveDeviceSerialNumber());
        DataConcentrator concentrator = IecWarehouse.getInstance().getDataConcentratorFactory().findByKey(container.getMasterDeviceSerialNumber());
        if (concentrator != null) {
            logBook = MeteringWarehouse.getCurrent().getLogBookFactory().findByDeviceAndObisCode(concentrator.getDevice().getDevice(), container.getLogbookObis());
            model.setLogicalEventGroup(IecWarehouse.getInstance().getEventGroupMappingFactory().findByKey(logBook.getLogBookType().getName()).getEventGroup().toString());
        } else {
            logBook = MeteringWarehouse.getCurrent().getLogBookFactory().findByDeviceAndObisCode(genericMeter.getDevice().getDevice(), container.getLogbookObis());
            model.setLogicalEventGroup(IecWarehouse.getInstance().getEventGroupMappingFactory().findByKey(logBook.getLogBookType().getName()).getEventGroup().toString());
        }
        return model;
    }

    private void storeEventInLogbook(MeterEventReportModel model) throws SQLException, BusinessException {
        DeviceEventShadow deviceEventShadow = new DeviceEventShadow();
        deviceEventShadow.setDeviceCode((Integer.valueOf(model.getEventCode())));
        deviceEventShadow.setDate(model.getDate());
        deviceEventShadow.setMessage(model.getAdditionalInfo());
        deviceEventShadow.setFlags(3);
        deviceEventShadow.setEventType(getEndDeviceEventType());
        logBook.addEvent(deviceEventShadow);
    }

    private void sendToNoc(MeterEventReportModel model) throws BusinessException {
        HandleNotificationPushRequestBuilder builder = new HandleNotificationPushRequestBuilder();
        SapResponseMessage responseMessage = builder.build(Arrays.asList(model));
//        ServiceRequest serviceRequest = ((SapResponseMessage) responseMessage).getServiceRequest();
        new IecClassInjector().getMessageSender().sendMessage(responseMessage);
    }

    private static EndDeviceEventType getEndDeviceEventType() {
        EndDeviceEventType deviceEventType = new EndDeviceEventType(EndDeviceType.NOT_APPLICABLE, EndDeviceDomain.NOT_APPLICABLE, EndDeviceSubdomain.NOT_APPLICABLE, EndDeviceEventOrAction.NOT_APPLICABLE);
        return deviceEventType;
    }

    @Override
    public void addProperties(TypedProperties arg0) {
    }

    @Override
    public String getVersion() {
        return "";
    }

    @Override
    public List<PropertySpec> getOptionalProperties() {
        return Collections.emptyList();
    }

    @Override
    public List<PropertySpec> getRequiredProperties() {
        return Collections.emptyList();
    }


}

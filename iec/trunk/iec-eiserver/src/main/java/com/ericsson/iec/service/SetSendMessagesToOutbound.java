package com.ericsson.iec.service;

import com.energyict.mdc.shadow.tasks.ComTaskExecutionShadow;
import com.energyict.mdc.tasks.ConnectionTask;
import com.energyict.mdc.tasks.overview.CommunicationTask;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.workflow.PlcMeterRegistration;

import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;


public class SetSendMessagesToOutbound extends AbstractServiceTransitionHandlerWithExceptionHandling<PlcMeterRegistration> {

    public static final String TRANSITION_NAME = "Set Send Messages To Outbound";

    public SetSendMessagesToOutbound(Logger logger) {
        super(logger);
    }

    @Override
    protected void doProcess(PlcMeterRegistration plcMeterRegistration) throws IecException {
        try {
            GenericMeter genericMeter = plcMeterRegistration.getMeter();
            for (CommunicationTask communicationTask : genericMeter.getDevice().getDevice().getCommunicationOverview().getCommunicationTasks()) {
                if (communicationTask.getComTask().getName().equals("Send Messages")) {
                    ComTaskExecutionShadow comTaskExecutionShadow = communicationTask.getComTaskExecution().getShadow();
                    for (ConnectionTask connectionTask : genericMeter.getDevice().getDevice().getAllConnectionTasks()) {
                        if (connectionTask.getName().contains("Outbound")) {
                            comTaskExecutionShadow.setConnectionTaskName(connectionTask.getName());
                            comTaskExecutionShadow.setConnectionTaskId(connectionTask.getId());
                            communicationTask.getComTaskExecution().update(comTaskExecutionShadow);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
        }
    }

    @Override
    protected String getTransitionName() {
        return TRANSITION_NAME;
    }

}

package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_CONTAINS_NON_NUMERIC_VALUE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_ANY_VALUE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_FORMAT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_PATTERN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_THE_VALUE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_MUST_BE_FILLED_IN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_NOT_FOUND_IN_LOOKUP_TABLE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_VALIDATION_FAILED;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.energyict.mdw.core.FolderType;
import com.energyict.mdw.core.StringLookup;
import com.energyict.mdw.relation.RelationAttributeType;
import com.energyict.mdw.relation.RelationType;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory.ActionType;

public class CommonValidatorFactory {

	public static void validateValueList(String value, String[] validValues, String attributeName) throws FatalValidationException {
		validateMandatoryValue(value, attributeName);
		List<String> validValuesList = Arrays.asList(validValues);
		if (!validValuesList.contains(value))
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, attributeName, value, validValuesList.toString());
	}

	public static void validateValue(String value, String validValue, String attributeName) throws FatalValidationException {
		validateMandatoryValue(value, attributeName);
		if (!value.equals(validValue))
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_THE_VALUE, attributeName, value, "", validValue);
	}

	public static void validateValue(Date value, String validValue, String attributeName) throws FatalValidationException {
		validateMandatoryValue(value, attributeName);
		try {
			Date validDate = CommonObjectFactory.parseDateString(validValue);
			if (!value.equals(validDate))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_THE_VALUE, "EndDate", value, "", validValue);
		} catch (ParseException e) {
			throw new FatalValidationException(e, MESSAGE_VALIDATION_FAILED, "EndDate parsing failed", validValue);
		}
	}

	public static void validateMandatoryValue(String value, String attributeName) throws FatalValidationException {
		if (value == null || value.isEmpty()) 
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, attributeName);
	}

	public static void validateMandatoryValue(Date value, String attributeName) throws FatalValidationException {
		if (value == null) 
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, attributeName);
	}

	public static void validateMandatoryValue(BigDecimal value, String attributeName) throws FatalValidationException {
		if (value == null) 
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, attributeName);
	}

	public static String validateLookup(String key, String keyName, String attributeName, FolderType folderType) throws FatalValidationException {
		RelationType relationType = folderType.getDefaultRelationType();
		RelationAttributeType attrivuteType = relationType.getAttributeType(attributeName);
		StringLookup lookup = attrivuteType.getStringLookup();
		String value = lookup.getValue(key);
		if (value == null) {
			throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, keyName, key);
		}
		return value;
	}

	public static String validateLookupByValue(String value, String keyName, String attributeName, FolderType folderType) throws FatalValidationException {
		RelationType relationType = folderType.getDefaultRelationType();
		RelationAttributeType attrivuteType = relationType.getAttributeType(attributeName);
		StringLookup lookup = attrivuteType.getStringLookup();
		@SuppressWarnings("deprecation")
		String key = lookup.getKey(value);
		if (key == null) {
			throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, keyName, value);
		}
		return key;
	}

	public static void validateCoordinate(BigDecimal stationCoordinate, String attributeName) throws FatalValidationException {
		if (stationCoordinate != null && !(stationCoordinate.toPlainString().matches("[0-9]{1,3}\\.[0-9]{1,6}")))
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_PATTERN, attributeName, stationCoordinate.toPlainString(), "[3 digits].[6 digits]");
	}

	public static void validateYear(BigInteger year, String attributeName) throws FatalValidationException {
		if (year == null)
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, attributeName);
		if (year.toString().length() != 4)
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_FORMAT, attributeName, year.toString(), "YYYY");
	}
	
	public static void validateStationNumerator(String stationNumerator) throws FatalValidationException {
		if (stationNumerator == null || stationNumerator.isEmpty())
			throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "STATION_NUMERATOR");
		if (!stationNumerator.matches("[0-9]{10,10}"))
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_FORMAT, "STATION_NUMERATOR", stationNumerator, "10 digits");
	}
	
	public static void validateNumTransInSchema(String numTransInSchema) throws FatalValidationException {
		int maxNumberOfTransformers = IecSystemParameters.NIS_MAX_NUMBER_OF_TRANSFORMERS.getIntValue();
		try {
			int transformerId = Integer.valueOf(numTransInSchema);
			if (!(transformerId >= 1 && transformerId <= maxNumberOfTransformers))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "NUM_TRANS_IN_SCHEMA", numTransInSchema, "[1,2,...," + maxNumberOfTransformers + "]");
		} catch (NumberFormatException e) {
			throw new FatalValidationException(FIELD_CONTAINS_NON_NUMERIC_VALUE, "NUM_TRANS_IN_SCHEMA", numTransInSchema);
		}
	}

	public static void validatePlcCell(String plcCell) throws FatalValidationException {
		String[] validValues = new String[] { "P", "C" };
		CommonValidatorFactory.validateValueList(plcCell, validValues, "PLC_CELL");
	}

	public static void validateAction(ActionType action) throws FatalValidationException {
		if (action == null)
			throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "ACTION", action, "[Insert,Update,Remove]");
	}

}

package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface District extends FolderVersionWrapper {

	Date getLatestNisUpdateDate();
	void setDistrict(Date latestNisUpdateDate);
}

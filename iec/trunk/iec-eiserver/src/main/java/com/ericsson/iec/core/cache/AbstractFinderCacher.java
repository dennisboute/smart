package com.ericsson.iec.core.cache;

public abstract class AbstractFinderCacher<K, V> extends AbstractCacher<K, V> implements Finder<K, V> {

    protected AbstractFinderCacher() {

    }

    public AbstractFinderCacher(String businessObjectName) {
        this.businessObjectName = businessObjectName;
    }

    @Override
    public V getValue(K key) {
        V value = this.cache.get(key);
        if (value == null) {
            synchronized (cache) {
                value = this.cache.get(key);
                if (value == null) {
                    value = findValue(key);
                    if (value == null) {
                        return null;
                    }
                    cache.put(key, value);
                }
            }
        }
        return value;
    }

}

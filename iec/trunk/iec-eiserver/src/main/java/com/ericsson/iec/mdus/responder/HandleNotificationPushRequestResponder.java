package com.ericsson.iec.mdus.responder;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.ericsson.iec.mdus.MdusSapEndpoint;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.HandleNotificationMeterDataRequest;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.BasicFaultMessage;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;

public class HandleNotificationPushRequestResponder extends HandleNotificationRequestResponder {


    public HandleNotificationPushRequestResponder() throws MarshallingException {
        super(MdusSapEndpoint.HANDLE_NOTIFICATION_METER_DATA_PUSH_REQUEST);
    }


    @Override
    protected void sendMessage(HandleNotificationMeterDataRequest handleNotificationMeterDataRequest) throws BusinessException, BasicFaultMessage {
        WSMDMNOC01PortType port = getPort();
        port.handleNotificationPushMeterDataOperation(handleNotificationMeterDataRequest);
    }
}

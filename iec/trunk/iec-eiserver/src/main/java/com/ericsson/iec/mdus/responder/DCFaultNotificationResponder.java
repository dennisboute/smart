package com.ericsson.iec.mdus.responder;

import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.ericsson.iec.mdus.MdusSapEndpoint;

import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationRequest;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;

public class DCFaultNotificationResponder extends IecMdusSapResponder<WSMDMNOC01ServicesServiceagent, WSMDMNOC01PortType> {

	public DCFaultNotificationResponder() throws MarshallingException {
		super(MdusSapEndpoint.DC_FAULT_NOTIFICATION, WSMDMNOC01ServicesServiceagent.class, WSMDMNOC01PortType.class);
	}

	@Override
	public void respond(ResponseMessage message) throws MdusBusinessException {
		
		try {
			DCFaultNotificationRequest dcFaultNotificationRequest = unmarshal(message.getMessage(), DCFaultNotificationRequest.class);
			WSMDMNOC01PortType port = getPort();
			updateOutputRequest(message);
			port.dcFaultNotificationOperation(dcFaultNotificationRequest);
			markServiceRequestSuccess(message);
			
		} catch (Exception e) {
			try {
				markServiceRequestFailed(message);
				logError(message, e.getMessage());
			}
			catch (Exception ex) {
			}
		}

	}

}

package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceConfiguration;
import com.energyict.projects.common.model.device.DeviceAdapter;
import com.energyict.projects.common.model.device.DeviceAdapterFactoryImpl;

public class IecDeviceAdapterFactory extends DeviceAdapterFactoryImpl {

    @Override
    public DeviceAdapter createNew(Device device, Date date) {
        return new IecDeviceAdapter(device, date);
    }
    
    @Override
    public DeviceAdapter createNew(DeviceConfiguration deviceConfiguration, Date activeDate) {
        return new IecDeviceAdapter(deviceConfiguration, activeDate);
    }

}

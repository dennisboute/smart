package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import nismdm01.interfaces.mdm.iec.TransformerRequest;

public class TransformerHandler extends IecSingleRequestHandler<TransformerRequest>{

	public TransformerHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		TransformerRequest request = unmarshall(sapRequestMessage.getMessage(), TransformerRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<TransformerRequest> parameters = 
				new RequestHandlerParameters<TransformerRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<TransformerRequest> parameters) throws BusinessException {
		TransformerRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(TransformerRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForTransformerRequest(singleRequest);
		
		shadow.setName(singleRequest.getMessageContent().getSTATIONNUMERATOR().toString());
		shadow.setExternalName(singleRequest.getMessageContent().getSTATIONNUMERATOR().toString());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(TransformerRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
				
		return shadow;
	}
}

package com.ericsson.iec.core;

import com.energyict.hsm.worldline.CustomizationModule;
import com.energyict.hsm.worldline.HsmModelModule;
import com.energyict.mdus.MdusModule;
import com.energyict.projects.AbstractCustomerModule;
import com.energyict.projects.LibraryModule;
import com.energyict.projects.coreextensions.CoreExtensionsLibraryModule;
import com.energyict.projects.wrappers.WrappersLibraryModule;
import com.ericsson.iec.core.exception.FatalExecutionException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class IecCustomerModule extends AbstractCustomerModule {

    public static final String MODULE_NAME = "IEC";
    private static final String CUSTOMER_NAME = "IEC";

    @Override
    public String getErrorCodeBundleName() {
        return "";
    }

    @Override
    public String getName() {
        return MODULE_NAME;
    }

    @Override
    public String getAboutPanelCustomer() {
        return CUSTOMER_NAME;
    }

    @Override
    public String getAboutPanelVersion() {
        return "1.4";
    }

    @Override
    public Collection<LibraryModule> getLibraryModules() {
        List<LibraryModule> modules = new ArrayList<>();
        modules.add(new IecLibraryModule());
        modules.add(new MdusModule());
        modules.add(new CoreExtensionsLibraryModule());
        modules.add(new WrappersLibraryModule());
        modules.add(new HsmModelModule());
        modules.add(new CustomizationModule());
        return modules;
    }

    @Override
    public void initialize() {
        try {
            new SystemParameterDefaultValueLoader().loadDefaultValues(this);
        } catch (FatalExecutionException e) {
            e.printStackTrace();
        }
    }

}

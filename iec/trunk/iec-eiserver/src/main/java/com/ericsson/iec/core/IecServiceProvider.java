package com.ericsson.iec.core;

import com.ericsson.iec.service.DataConcentratorDeviceUpdateService;
import com.ericsson.iec.service.DataConcentratorDeviceUpdateServiceImpl;

public class IecServiceProvider {
	
	private DataConcentratorDeviceUpdateService dataConcentratorDeviceUpdateService;
	
	public DataConcentratorDeviceUpdateService getDataConcentratorDeviceUpdateService() {
		if (dataConcentratorDeviceUpdateService == null) {
			dataConcentratorDeviceUpdateService = new DataConcentratorDeviceUpdateServiceImpl();
		}
		return dataConcentratorDeviceUpdateService;
	}

}

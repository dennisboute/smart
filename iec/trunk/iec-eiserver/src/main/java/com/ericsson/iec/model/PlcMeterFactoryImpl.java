package com.ericsson.iec.model;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.PLC_METER;

import com.energyict.mdw.core.Folder;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.coreextensions.FolderTypeSearchFilter;
import com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes;
import com.ericsson.iec.core.IecWarehouse;

public class PlcMeterFactoryImpl extends GenericMeterFactoryImpl implements PlcMeterFactory {

	public PlcMeterFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public PlcMeterFactoryImpl() {
		super();
	}

	@Override
	public PlcMeter createNew(FolderVersionAdapter arg0) {
		return new PlcMeterImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return PLC_METER.name;
	}
	
	@Override
	public String buildExternalName(String key) {
		return PLC_METER.buildExternalName(key);
	}

	@Override
	public GenericMeter findBySerialId(String serialId) {
		FolderTypeSearchFilter filter = new FolderTypeSearchFilter(getFolderType(), IecWarehouse.getInstance().getMeteringWarehouse().getRootFolder());
        filter.addCriterium(MeterAttributes.SERIAL_ID.name, serialId);

        for (Folder folder : filter.findMatchingFolders()) {
            return createNewForLastVersion(folder);
        }
        return null;
    }

    public GenericMeter findByGenericMeterDevice(GenericMeterDevice genericMeterDevice){
		FolderTypeSearchFilter filter = new FolderTypeSearchFilter(getFolderType(), IecWarehouse.getInstance().getMeteringWarehouse().getRootFolder());
		filter.addCriterium(MeterAttributes.DEVICE.name, genericMeterDevice.getDevice());

		for (Folder folder : filter.findMatchingFolders()) {
			return createNewForLastVersion(folder);
		}
		return null;
	}
}

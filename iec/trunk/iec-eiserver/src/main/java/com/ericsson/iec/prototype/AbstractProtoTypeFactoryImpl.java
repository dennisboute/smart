package com.ericsson.iec.prototype;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.PROTOTYPE_NOT_FOUND;

import java.util.List;

import com.energyict.mdw.template.CopySpec;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public abstract class AbstractProtoTypeFactoryImpl {

	public AbstractProtoTypeFactoryImpl()  {
	}
	
	protected CopySpec getCopySpec() throws FatalConfigurationException {
		String requiredCopySpecExternalName = getCopySpecName();
		List<CopySpec> copySpecList = IecWarehouse.getInstance().getMeteringWarehouse().getCopySpecFactory().findAll();
		for (CopySpec copySpec : copySpecList) {
			String copySpecExternalName = copySpec.getFolder().getExternalName();
			if (copySpecExternalName != null && copySpecExternalName.equals(requiredCopySpecExternalName))
				return copySpec;
		}
		throw new FatalConfigurationException(PROTOTYPE_NOT_FOUND, requiredCopySpecExternalName);
	}

	public abstract String getCopySpecName();
}

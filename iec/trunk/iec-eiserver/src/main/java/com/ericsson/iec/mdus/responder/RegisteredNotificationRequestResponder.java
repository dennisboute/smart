package com.ericsson.iec.mdus.responder;

import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.ericsson.iec.mdus.MdusSapEndpoint;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.RegisteredNotificationRequest;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;

public class RegisteredNotificationRequestResponder extends IecMdusSapResponder<WSMDMNOC01ServicesServiceagent, WSMDMNOC01PortType> {

    public RegisteredNotificationRequestResponder() throws MarshallingException {
        super(MdusSapEndpoint.REGISTERED_NOTIFICATION_REQUEST, WSMDMNOC01ServicesServiceagent.class, WSMDMNOC01PortType.class);
    }

    @Override
    public void respond(ResponseMessage message) throws MdusBusinessException {

        try {
            RegisteredNotificationRequest registeredNotificationRequest = unmarshal(message.getMessage(), RegisteredNotificationRequest.class);
            WSMDMNOC01PortType port = getPort();
            updateOutputRequest(message);
            port.registeredNotificationOperation(registeredNotificationRequest);
            markServiceRequestSuccess(message);

        } catch (Exception e) {
            try {
                markServiceRequestFailed(message);
                logError(message, e.getMessage());
            } catch (Exception ex) {
            }
        }

    }

}

package com.ericsson.iec.mdus.handler.servicerequest;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;
import nocmdm01.interfaces.mdm.iec.DCAssetRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest.*;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest.MessageContent.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class DcEventReportHandler extends IecBulkRequestHandler<DCEventReportRequest> {


    public DcEventReportHandler() throws MarshallingException {
    }


    @Override
    protected List<ConsumptionRequestShadow> getConsumptionRequestShadows(RequestHandlerParameters<DCEventReportRequest> requestHandlerParameters) throws BusinessException {
        DCEventReportRequest bulkRequest = requestHandlerParameters.getRequest();
        try {
            return buildConsumptionRequestShadows(bulkRequest);
        } catch (JAXBException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
        DCEventReportRequest request = unmarshall(sapRequestMessage.getMessage(), DCEventReportRequest.class);
        String uuid = request.getMessageHeader().getMessageID();
        RequestHandlerParameters<DCEventReportRequest> parameters =
                new RequestHandlerParameters<>(sapRequestMessage, request, uuid);
        handle(parameters);
    }

    private List<ConsumptionRequestShadow> buildConsumptionRequestShadows(DCEventReportRequest bulkRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
        ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
        TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForDcEventReportRequest(bulkRequest);
        List<ConsumptionRequestShadow> consumptionRequestShadows = new ArrayList<>();
        for (DCEventReportRequest.MessageContent.DCEventReport dcEventReport : bulkRequest.getMessageContent().getDCEventReport()) {
            shadow.setName(dcEventReport.getDCID().toString());
            shadow.setExternalName(dcEventReport.getDCID().toString());
            shadow.setFrom(validityPeriod.getFrom());
            shadow.setTo(validityPeriod.getTo());

            JAXBElement<DCEventReport> element = new JAXBElement<>(new QName("DCEventReport"), DCEventReport.class, dcEventReport);
            JAXBContext jaxbContext = JAXBContext.newInstance(DCEventReport.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter writer = new StringWriter();
            marshaller.marshal(element, writer);
            shadow.setRequest(writer.toString());
            consumptionRequestShadows.add(shadow);
        }

        return consumptionRequestShadows;
    }


}

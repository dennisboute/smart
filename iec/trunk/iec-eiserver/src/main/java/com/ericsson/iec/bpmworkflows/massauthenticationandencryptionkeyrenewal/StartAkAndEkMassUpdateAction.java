package com.ericsson.iec.bpmworkflows.massauthenticationandencryptionkeyrenewal;

import com.energyict.massupdateactions.model.MassUpdateProvider;
import com.energyict.massupdateactions.model.action.MassUpdateAction;
import com.energyict.massupdateactions.model.workflow.IndividualWorkflow;
import com.energyict.massupdateactions.model.workflow.MassUpdateWorkflow;
import com.energyict.massupdateactions.util.CreateMassUpdateActionService;
import com.energyict.massupdateactions.util.StartNextMassUpdateItemsCommand;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;

import java.util.logging.Logger;

public class StartAkAndEkMassUpdateAction<T extends IndividualWorkflow> extends AbstractServiceTransitionHandlerWithExceptionHandling<MassUpdateWorkflow<T>>{
    public static final String TRANSITION_NAME = "Start Ak and EK mass update action";
    private MassUpdateProvider<T> provider;
    public StartAkAndEkMassUpdateAction(Logger logger, MassUpdateProvider<T> provider) {
        super(logger);
        this.provider = provider;
    }

    protected void doProcess(MassUpdateWorkflow<T> massUpdateWorkflow) throws Exception {
        MassUpdateAction<T> massUpdateAction = ((CreateMassUpdateActionService)CreateMassUpdateActionService.INSTANCE.get()).createNew(massUpdateWorkflow, this.provider);
        (new StartNextMassUpdateItemsCommand(massUpdateAction, this.provider)).execute();
        massUpdateWorkflow.setMassUpdateAction(massUpdateAction.getServiceRequest());
    }

    protected String getTransitionName() {
        return TRANSITION_NAME;
    }
}
package com.ericsson.iec.constants;

public class ProtoTypeConstants {

	public enum ProtoTypes {
		CIRCUTOR_DATACONCENTRATOR("DC/Circutor DC"), // external name of the prototype object
		POINT_OF_DELIVERY("POD/Point Of Delivery"), // external name of the prototype object
		METER("SMP/"), // external name of the prototype object (concatenated with meterModel)
		;
		
		private String protoTypeName;
		
		ProtoTypes(String protoTypeName) {
			this.protoTypeName = protoTypeName;
		}
		
		public String getName() {
			return protoTypeName;
		}
		
		public String getProtoTypeName() {
			return protoTypeName;
		}
		
		static public ProtoTypes getByNamePrefix(String namePrefix) {
			for (ProtoTypes p : ProtoTypes.values()) {
				if (p.getProtoTypeName().startsWith(namePrefix))
					return p;
			}
			return null;
		}
	}
	
	public enum ProtoTypeObjectType {
		PLC_METER,
		CELLULAR_METER,
		DC,
		POD,
		;
	}
	
	public enum CircutorDCParameters {
		DATACONCENTRATOR_ID("DC_ID"),
		SERIAL_NUMBER("Serial Number"),
		MANUFACTURER("Manufacturer"),
		MANUFACTURING_YEAR("Manufacturing Year"),
		MODEL_DESCRIPTION("Model description"),
		MAC_ADDRESS_PLC("MAC Address PLC"),
		FIRMWARE_VERSION("Firmware Version"),
		;
		
		private String parameterName;
		
		CircutorDCParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}

	public enum PointOfDeliveryParameters {
		POD_ID("podId"),
		;
		
		private String parameterName;
		
		PointOfDeliveryParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}

	public enum MeterParameters {
		SERIAL_ID("Serial ID"),
		BATCH("Batch"),
		MAC_ADDRESS("MacAddress"),
		DELIVERY_DATE("Delivery Date"),
		FIRMWARE_VERSION("Firmware Version"),
		;
		
		private String parameterName;
		
		MeterParameters(String parameterName) {
			this.parameterName = parameterName;
		}
		
		public String getName() {
			return parameterName;
		}
	}
}

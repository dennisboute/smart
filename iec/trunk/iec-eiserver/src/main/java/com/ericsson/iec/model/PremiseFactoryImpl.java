package com.ericsson.iec.model;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.PREMISE;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;

public class PremiseFactoryImpl extends FolderVersionWrapperFactoryImpl<Premise> implements PremiseFactory {

	public PremiseFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public PremiseFactoryImpl() {
		super();
	}

	@Override
	public Premise createNew(FolderVersionAdapter arg0) {
		return new PremiseImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return PREMISE.name;
	}
	
	@Override
	public Premise findByKey(String key) {
		return findByExternalName(PREMISE.buildExternalName(key), new Date());
	}
}

package com.ericsson.iec.model;

import com.energyict.mdw.core.Folder;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

import java.math.BigDecimal;

public interface EventGroupMappingFactory extends FolderVersionWrapperFactory<EventGroupMapping> {

	EventGroupMapping findByKey(String key);

	Folder findByDcEventGroup(BigDecimal eventCode);
}

package com.ericsson.iec.bpmworkflows.massauthenticationandencryptionkeyrenewal;

import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.WorkItem;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapper;
import com.energyict.massupdateactions.model.MassUpdateProvider;
import com.energyict.massupdateactions.workflowservice.AbstractMassUpdateWorkflowService;

import java.util.logging.Logger;

public class MassAuthenticationAndEncryptionKeyRenewalService extends AbstractMassUpdateWorkflowService<KeyRenewalProcessCaseWrapper> {


    public MassAuthenticationAndEncryptionKeyRenewalService() {
    }


    public String getVersion() {
        return "$Date: 2017-06-27 13:46:02 +0200 (Tue, 27 Jun 2017) $";
    }

    @ServiceMethod(
            name = "Start mass update action"
    )
    public CaseAttributes startMassUpdateAction(WorkItem item, Logger logger) {
        return this.process(item, new StartAkAndEkMassUpdateAction(logger, this.getProvider()));
    }

    protected MassUpdateProvider<KeyRenewalProcessCaseWrapper> getProvider() {
        return (MassUpdateProvider) MassKeyRenewalUpdateProviderImpl.INSTANCE.get();
    }
}

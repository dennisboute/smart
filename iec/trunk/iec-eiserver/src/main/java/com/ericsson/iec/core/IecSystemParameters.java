package com.ericsson.iec.core;

import java.util.List;

import com.energyict.mdw.core.SystemParameter;

public enum IecSystemParameters {

	NIS_MAX_NUMBER_OF_TRANSFORMERS("NIS_MAX_NUMBER_OF_TRANSFORMERS","Nis Max Number Of Transformers", "4 or 8", "Iec Config", "8", null),
	MAX_CELL_COMM_STATISTICS_MSG_TO_NOC("MAX_CELL_COMM_STATISTICS_MSG_TO_NOC", "Max Cellular Communication Statistics Messages To NOC", "Max cellular communication statistics message count for NOC in a single request", "Iec Config", "1000", null),
	MAX_PULL_MSG_TO_NOC("MAX_PULL_MSG_TO_NOC", "Max Pull Messages To NOC", "Max pull message count for NOC in a single request", "Iec Config", "1000", null),
	NUMBER_OF_PULL_EVENTS_TO_HANDLE_AT_ONCE("NUMBER_OF_PULL_EVENTS_TO_HANDLE_AT_ONCE", "Number Of Pull Events To Handle At Once", "Number of pull events count for DB Query", "Iec Config", "1000", null),
	FIRST_SHIPMENT_FILE_BATCH_ID("FIRST_SHIPMENT_FILE_BATCH_ID", "First Shipment File Batch Id", "First batch id has unique validation logic", "Iec Config", "KAIFA_IEC_001", null),
	METER_KEY_EXPORTER_ALLOWED_SERVERS("METER_KEY_EXPORTER_ALLOWED_SERVERS", "Meter Key Exporter Allowed Servers", "Allowed server IPs for executing meter key exporter", "Iec Config", "", null),
	;
	
	private String key;
	private String displayName;
	private String description;
	private String category;
	private String value;
	private List<String> values;
	
	IecSystemParameters(String key, String displayName, String description, String category, String value, List<String> values) {
		this.key = key;
		this.displayName = displayName;
		this.description = description;
		this.category = category;
		this.value = value;
		this.values = values;	
	}
	
	public IecSystemParameterSpec toParameterSpec() {
		return new IecSystemParameterSpec(key, displayName, description, category, value, values);
	}
	
	public String getStringValue() {
		SystemParameter param = IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().find(key);
		return (param != null) ? param.getValue() : "";
	}
	
	public int getIntValue() {
		SystemParameter param = IecWarehouse.getInstance().getMeteringWarehouse().getSystemParameterFactory().find(key);
		return (param != null) ? Integer.valueOf(param.getValue()) : 0;
	}
}

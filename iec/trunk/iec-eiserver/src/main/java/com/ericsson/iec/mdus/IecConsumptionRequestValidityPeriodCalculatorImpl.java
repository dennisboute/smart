package com.ericsson.iec.mdus;

import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.MdusConsumptionRequestValidityPeriodCalculator;
import com.energyict.projects.dateandtime.Clock;

import nismdm01.interfaces.mdm.iec.PremiseRequest;
import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nocmdm01.interfaces.mdm.iec.DCAssetRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest;
import sapmdm01.interfaces.mdm.iec.MeterLocationRequest;
import sapmdm01.interfaces.mdm.iec.MeterPodRequest;
import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest;
import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest;

public class IecConsumptionRequestValidityPeriodCalculatorImpl extends MdusConsumptionRequestValidityPeriodCalculator
implements IecConsumptionRequestValidityPeriodCalculator {

	@Override
	public TimePeriod calculateForTransformerStationRequest(TransformerStationRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.TRANSFORMER_STATION_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForTransformerRequest(TransformerRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.TRANSFORMER_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForPremiseRequest(PremiseRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.PREMISE_REQUEST, Clock.INSTANCE.get().now());
	}
	
	@Override
	public TimePeriod calculateForDcAssetRequest(DCAssetRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.DC_ASSET_REQUEST, Clock.INSTANCE.get().now());
	}
	
	@Override
	public TimePeriod calculateForDcEventReportRequest(DCEventReportRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.DC_EVENT_REPORT_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForFieldActivityNotificationRequest(FieldActivityNotificationRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.FIELD_ACTIVITY_NOTIFICATION_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForMeterSapLinkRequest(MeterSapLinkRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.METER_SAP_LINK_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForMeterLocationRequest(MeterLocationRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.METER_LOCATION_REQUEST, Clock.INSTANCE.get().now());
	}

	@Override
	public TimePeriod calculateForMeterPodRequest(MeterPodRequest request) {
		return calculateBasedOnDelayInSeconds(MdusWebservice.METER_POD_REQUEST, Clock.INSTANCE.get().now());
	}

}

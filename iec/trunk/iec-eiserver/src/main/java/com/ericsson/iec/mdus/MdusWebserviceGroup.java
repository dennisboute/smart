package com.ericsson.iec.mdus;

import com.energyict.mdus.core.SapWebServiceGroup;

public enum MdusWebserviceGroup implements SapWebServiceGroup {
	
	NIS("Mdus\\Nis"), NOC("Mdus\\Noc"), WFM("Mdus\\Wfm"), SAP("Mdus\\Sap"), RADIUS("Mdus\\Radius");
	
	private String name;
	
	MdusWebserviceGroup(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

}

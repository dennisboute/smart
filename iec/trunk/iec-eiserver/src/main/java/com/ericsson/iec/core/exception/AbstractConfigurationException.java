package com.ericsson.iec.core.exception;

import com.energyict.mdus.core.BusinessErrorCodeDelegate;
import com.energyict.mdus.core.BusinessErrorSeverity;
import com.energyict.mdus.core.MdusModuleHelper;
import com.energyict.mdus.core.SapProcessingResultCode;
import com.ericsson.iec.core.IecMdusCategoryCodes;
import com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage;

public abstract class AbstractConfigurationException extends IecException {

	private static final long serialVersionUID = -7148154634024589822L;

	public AbstractConfigurationException(ConfigurationExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(exceptionCode, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.EXECUTION_FAILURE)
				));
	}

	public AbstractConfigurationException(Throwable cause, ConfigurationExceptionMessage exceptionCode, boolean fatal, Object... messageParameters) {
		super(cause, exceptionCode, fatal, messageParameters);
		setFatal(fatal);
		setBusinessErrorCode(new BusinessErrorCodeDelegate(
								SapProcessingResultCode.FAILED, 
								BusinessErrorSeverity.ERROR,
								getMessage(),
								MdusModuleHelper.INSTANCE.get().getDefaultSapTypeId(),
								String.valueOf(IecMdusCategoryCodes.EXECUTION_FAILURE)
				));
	}

}

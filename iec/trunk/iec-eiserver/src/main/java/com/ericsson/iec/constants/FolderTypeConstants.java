package com.ericsson.iec.constants;

public class FolderTypeConstants {
	
	public enum FolderTypes {
		CELLULAR_GRID("CellularGrid", "CELLG"),
		CELLULAR_METER("CellularMeter", "CM"),
		CITY("City", "CCO"),
		CT_METER_GRID("CtMeterGrid", "CTG"),
		DATA_CONCENTRATOR("DataConcentrator", "DC"),
		DISTRICT("District", "DCO"),
		FIRMWARE_VERSION("FirmwareVersion", "FWV"),
		METER_LOCATION("MeterLocation", "LOC"),
		PLC_GRID("PlcGrid", "PLCG"),
		PLC_METER("PlcMeter", "PLCM"),
		PREMISE("Premise", "PC"),
		REGION("Region", "RCO"),
		TRANSFORMER("Transformer", "TR"),
		TRANSFORMER_STATION("TransformerStation", "TS"),
		POINT_OF_DELIVERY("PointOfDelivery", "POD"),
		POD_ASSIGNMENT("PodAssignment", "PODA"),
		NOC_EVENT_CONFIGURATION("NocEventConfiguration","NEC"),
		NOC_EVENT_DETAILS("NocEventDetails","NED"),
		EVENT_GROUP_MAPPING("EventGroupMapping","EGM")
		;
		
		public String name;
		private String prefix;
		
		FolderTypes(String name, String prefix) {
			this.name = name;
			this.prefix = prefix;
		}
		
		public String getName() {
			return name;
		}
		
		public String getPrefix() {
			return prefix;
		}
		
		public String buildExternalName(String key) {
			return prefix + "/" + key;
		}

		public static String extractFolderKey(String externalName) {
			if (externalName != null && !externalName.isEmpty())
				return externalName.substring(externalName.indexOf("/") + 1);
			else
				return null;
		}
	}
}
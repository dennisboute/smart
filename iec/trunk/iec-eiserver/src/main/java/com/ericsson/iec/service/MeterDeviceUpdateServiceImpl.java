package com.ericsson.iec.service;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.NO_OUTBOUND_TCP_IP_CONNECTION_TASK_CONFIGUTED_FOR_DEVICE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.TypedProperties;
import com.energyict.mdc.shadow.tasks.OutboundConnectionTaskShadow;
import com.energyict.mdc.tasks.OutboundConnectionTask;
import com.energyict.mdw.core.Device;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.GenericMeterDevice;

public class MeterDeviceUpdateServiceImpl implements MeterDeviceUpdateService {

	private static final String IP_ADDRESS_PROPERTY_NAME = "host";
	
	@Override
	public void updateIpAddress(GenericMeterDevice device, String ipAddress) throws IecException {
		List<OutboundConnectionTask> outboundConnectionTasks = getOutboundTcpIpConnectionTasks(device);
		if (outboundConnectionTasks.isEmpty()) {
			throw new FatalConfigurationException(NO_OUTBOUND_TCP_IP_CONNECTION_TASK_CONFIGUTED_FOR_DEVICE, device.getName(), device.getExternalName());
		}
		for (OutboundConnectionTask task : outboundConnectionTasks) {
			try {
				updateOutboundConnectionTask(task, ipAddress);
			}
			catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, new Object[] { }); //TODO Parameters
			}
		}
	}
	
	private void updateOutboundConnectionTask(OutboundConnectionTask task, String ipAddress) throws BusinessException, SQLException {
		OutboundConnectionTaskShadow shadow = task.getShadow();
		TypedProperties typedProperties = task.getTypedProperties();
		typedProperties.setProperty(IP_ADDRESS_PROPERTY_NAME, ipAddress);
		shadow.setProperties(typedProperties);
		task.update(shadow);
	}
	
	private List<OutboundConnectionTask> getOutboundTcpIpConnectionTasks(GenericMeterDevice meter) {
		Device device = meter.getDevice();
		if (device == null) {
			return Collections.emptyList();
		}
		return new OutboundConnectionTaskFilter().filterOutboundTcpIpConnections(device.getOutboundConnectionTasks());
	}
	
}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface NocEventConfiguration extends FolderVersionWrapper {

	String getDescription();
}

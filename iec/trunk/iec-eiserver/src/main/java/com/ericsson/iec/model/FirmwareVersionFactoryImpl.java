package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class FirmwareVersionFactoryImpl extends FolderVersionWrapperFactoryImpl<FirmwareVersion> implements FirmwareVersionFactory {

	public FirmwareVersionFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public FirmwareVersionFactoryImpl() {
		super();
	}

	@Override
	public FirmwareVersion createNew(FolderVersionAdapter arg0) {
		return new FirmwareVersionImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.FIRMWARE_VERSION.name;
	}
	
	@Override
	public FirmwareVersion findByKey(String key) {
		return findByExternalName(FolderTypes.FIRMWARE_VERSION.buildExternalName(key), new Date());
	}
}

package com.ericsson.iec.service.meterregistration;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.ENDPOINT_NOT_CONFIGUTED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

import java.io.StringWriter;
import java.net.URL;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.model.folder.EndpointConfig;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotification;

import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.RegisteredNotificationRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.RegisteredNotificationResponse;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01PortType;
import iec.tibco.resources.wsdl.concrete.ws_mdm_noc_01_services.WSMDMNOC01ServicesServiceagent;


public class SendNocNotification extends AbstractServiceTransitionHandlerWithExceptionHandling<MeterRegistrationNocNotification>{

	public static final String TRANSITION_NAME = "Send Noc Notification";
	private static final String ENDPOINTTOKEN = "WSMDMNOC01";
		
	public SendNocNotification(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(MeterRegistrationNocNotification meterRegistrationNocNotification) throws IecException {
		try {
			String endPoint = getEndPoint();
			getLogger().info("using endpoint : " + endPoint);
			WSMDMNOC01ServicesServiceagent service = new WSMDMNOC01ServicesServiceagent(new URL(endPoint));
			WSMDMNOC01PortType port = service.getWSMDMNOC01PortTypeEndpoint();
			
			meterRegistrationNocNotification.setRetry(false);
			
			GenericMeter meter = meterRegistrationNocNotification.getMeter();
			if (meter == null)
				return; // TBD

			ObjectFactory factory = new ObjectFactory();			
			RegisteredNotificationRequest request = factory.createRegisteredNotificationRequest();			
			request.setRequest(factory.createRegisteredNotificationRequestRequest());
			request.getRequest().setSerialNumberID(meter.getName());
			request.getRequest().setComFWVersion("");
			request.getRequest().setIP("");
			request.getRequest().setMeterFWVersion(meter.getFirmwareVersion().getFirmwareVersion());
			request.getRequest().setMeterManufacturingYear("");
			request.getRequest().setUtilitiesDeviceID(meter.getExternalName());
			
			RegisteredNotificationResponse response = port.registeredNotificationOperation(request);		
			if (response == null) {
				getLogger().info("Response NULL");
			} else {
				getLogger().info(response.getResponse().toString());
				JAXBContext jaxbContext = JAXBContext.newInstance(RegisteredNotificationResponse.class);
				Marshaller marshaller = jaxbContext.createMarshaller();
				StringWriter writer = new StringWriter();
				marshaller.marshal(response, writer);
				getLogger().info(writer.toString());
			}
		} catch (IecException e) {
			throw e;
		} catch(Exception e) {
			e.printStackTrace();
			throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "SendNocNotification", "");
		}
	}
	
	private String getEndPoint() throws FatalConfigurationException {
		EndpointConfig config = MdusWarehouse.getCurrent().getEndPointConfigFactory()
			.findByExternalName(ENDPOINTTOKEN, true); // endpoint external id is epc/ + ENDPOINTTOKEN
		if (config == null) {
			throw new FatalConfigurationException(ENDPOINT_NOT_CONFIGUTED, "MeterRegistration");
		} else {
			return config.getUrlString();
		}			
	}

	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}
}

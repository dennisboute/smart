package com.ericsson.iec.pluggable.workflow.meterregistration;

import com.energyict.annotations.processcaseadapter.WorkFlowService;
import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.WorkItem;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.workflow.PlcMeterRegistration;
import com.ericsson.iec.model.workflow.PlcMeterRegistrationFactory;
import com.ericsson.iec.service.SetSendMessagesToOutbound;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@WorkFlowService(version = "1.0")
public class PlcMeterRegistrationWorkflowService extends AbstractWorkflowServiceWithExceptionHandling<PlcMeterRegistration>  {

	private static final String PROPERTY_USER_FILE_ID = "User File ID";
	
	@Override
	public String getVersion() {
		return "Version 1.0";
	}

	@ServiceMethod(name= SetSendMessagesToOutbound.TRANSITION_NAME)
	public CaseAttributes setSendMessagesToOutbound(WorkItem workItem, Logger logger) {
		return process(workItem, new SetSendMessagesToOutbound(logger));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getRequiredProperties() {
		return Arrays.asList(PropertySpecFactory.bigDecimalPropertySpec(PROPERTY_USER_FILE_ID));
	}

	@Override
	public PlcMeterRegistrationFactory getProcessCaseWrapperFactory() {
		return IecWarehouse.getInstance().getPlcMeterRegistrationFactory();
	}

}

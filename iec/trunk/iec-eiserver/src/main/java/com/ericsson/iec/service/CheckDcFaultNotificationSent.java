package com.ericsson.iec.service;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.mdw.service.ServiceRequestState;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.DataConcentratorDeployment;

import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

public class CheckDcFaultNotificationSent extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment> {

    public static final String TRANSITION_NAME = "CheckDcFaultNotificationSent";

    public CheckDcFaultNotificationSent(Logger logger) {
        super(logger);
    }

    @Override
    protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
        ServiceRequest serviceRequest = dataConcentratorDeployment.getServiceRequest();
        if (serviceRequest.getState() != ServiceRequestState.SUCCESS) {
            //ToDo service request wrappen en service request message meegeven in exception (kijken in mdus)
            throw new FatalExecutionException(MESSAGE_PROCESSING_FAILED, "Failed Service Request", serviceRequest);
        }
    }

    @Override
    protected String getTransitionName() {
        return TRANSITION_NAME;
    }

}

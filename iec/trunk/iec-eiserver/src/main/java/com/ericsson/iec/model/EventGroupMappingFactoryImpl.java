package com.ericsson.iec.model;

import com.energyict.mdw.core.Folder;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.energyict.projects.coreextensions.FolderTypeSearchFilter;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventGroupMappingFactoryImpl extends FolderVersionWrapperFactoryImpl<EventGroupMapping> implements EventGroupMappingFactory {

    public EventGroupMappingFactoryImpl() {
        super();
    }

    @Override
    public EventGroupMapping createNew(FolderVersionAdapter arg0) {
        return new EventGroupMappingImpl(arg0);
    }

    @Override
    protected String getFolderTypeName() {
        return FolderTypes.EVENT_GROUP_MAPPING.getName();
    }

    @Override
    public EventGroupMapping findByKey(String key) {
        return findByExternalName(FolderTypes.EVENT_GROUP_MAPPING.buildExternalName(key), new Date());
    }

    @Override
    public Folder findByDcEventGroup(BigDecimal eventGroup) {
        List<Folder> eventGroupMappings = new ArrayList<>();
        if (eventGroup != null) {
            FolderTypeSearchFilter searchFilter = new FolderTypeSearchFilter(getFolderType(), MeteringWarehouse.getCurrent().getRootFolder());
            searchFilter.addCriterium("eventGroup", eventGroup);
            searchFilter.addCriterium("meterOrConcentrator", "Concentrator");
            eventGroupMappings = searchFilter.findMatchingFolders();
            return eventGroupMappings.get(0);
        }
        return null;
    }
}
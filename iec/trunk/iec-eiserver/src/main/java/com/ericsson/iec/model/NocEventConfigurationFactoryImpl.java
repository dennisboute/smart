package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

import java.util.Date;

public class NocEventConfigurationFactoryImpl extends FolderVersionWrapperFactoryImpl<NocEventConfiguration> implements NocEventConfigurationFactory {

	public NocEventConfigurationFactoryImpl() {
		super();
	}

	public NocEventConfigurationFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public NocEventConfiguration createNew(FolderVersionAdapter arg0) {
		return new NocEventConfigurationImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.NOC_EVENT_CONFIGURATION.getName();
	}
	
	@Override
	public NocEventConfiguration findByKey(String key) {
		return findByExternalName(FolderTypes.NOC_EVENT_CONFIGURATION.buildExternalName(key), new Date());
	}
}

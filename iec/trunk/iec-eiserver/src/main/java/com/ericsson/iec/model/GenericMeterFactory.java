package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface GenericMeterFactory extends FolderVersionWrapperFactory<GenericMeter> {

	String buildExternalName(String key);

	GenericMeter findBySerialId(String serialId);

	GenericMeter findByGenericMeterDevice(GenericMeterDevice genericMeterDevice);
}

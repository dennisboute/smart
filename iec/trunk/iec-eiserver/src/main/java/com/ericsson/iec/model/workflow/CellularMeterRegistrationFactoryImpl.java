package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactoryImpl;
import com.ericsson.iec.constants.ProcessConstants;

public class CellularMeterRegistrationFactoryImpl extends ProcessCaseWrapperFactoryImpl<CellularMeterRegistration> 
			 									   implements CellularMeterRegistrationFactory {

	@Override
	public String getProcessName() {
		return ProcessConstants.Processes.CELLULAR_METER_REGISTRATION.name;
	}

	@Override
	public CellularMeterRegistration createNew(ProcessCaseAdapter adapter) {
		return new CellularMeterRegistrationImpl(adapter);
	}

	//ToDo findByMeter(GenericMeter / cellular)

}

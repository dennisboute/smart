package com.ericsson.iec.bpmworkflows.keyrenewal;

import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.WorkItem;
import com.energyict.cpo.PropertySpec;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class AuthenticationAndEncryptionKeyRenewalWorkflowService extends AbstractWorkflowServiceWithExceptionHandling<AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper> {

    @ServiceMethod(name=FindKeyTypes.TRANSITION_NAME)
    public CaseAttributes findKeyTypes(WorkItem workItem, Logger logger) {
        return process(workItem, new FindKeyTypes(logger));
    }

    @Override
    public ProcessCaseWrapperFactory<AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper> getProcessCaseWrapperFactory() {
        return IecWarehouse.getInstance().getAuthenticationAndEncryptionKeyRenewalFactory();
    }

    @Override
    public String getVersion() {
        return "Version 1.0";
    }

    @Override
    public List<PropertySpec> getRequiredProperties() {
        return Collections.emptyList();
    }

    @Override
    public List<PropertySpec> getOptionalProperties() {
        return Collections.emptyList();
    }
}

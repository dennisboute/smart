package com.ericsson.iec.service;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

import java.util.logging.Logger;

import com.energyict.mdc.messages.DeviceMessage;
import com.energyict.mdc.messages.DeviceMessageFactoryImpl;
import com.energyict.mdc.messages.DeviceMessageStatus;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.mdw.core.DeviceMessageFilter;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import com.ericsson.iec.model.DataConcentratorDeployment;

public class CheckB07MessageResults extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment>{

	public static final String TRANSITION_NAME = "CheckB07MessageResults";
	
	public CheckB07MessageResults(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
		DeviceMessageFilter filter = new DeviceMessageFilter();
		filter.setTrackingIdMask(dataConcentratorDeployment.getTrackingId());
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		
		for (DeviceMessage deviceMessage : new DeviceMessageFactoryImpl().findByDeviceAndFilter(device, filter)) {
			if (deviceMessage.getStatus() == DeviceMessageStatus.CONFIRMED) {
				return;
			}
		}
		dataConcentratorDeployment.setDeploymentStatus("1");
		throw new FatalExecutionException(MESSAGE_PROCESSING_FAILED, "", "");
	}
	
	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}

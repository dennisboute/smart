package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_DOES_NOT_EXISTS;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.WRONG_DEVICE_CONFIGURATION;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.core.exception.NonFatalValidationException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.GenericMeter;

import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest;
import sapmdm01.interfaces.mdm.iec.MeterSapLinkRequest.MessageContent;

public class MeterSapLinkConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<MeterSapLinkRequest> {
	
	@Override
	protected MeterSapLinkRequest unmarshal(String request) throws MarshallingException {
		return (MeterSapLinkRequest) IecMarshallerHelper.getInstance().unmarshall(request, MeterSapLinkRequest.class);
	}
	
	@Override
	protected void doProcess(MeterSapLinkRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		MessageContent requestContent = message.getMessageContent();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(processContext, logger);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class ProcessContext {
		Date endDate;
		String materialId;
		String deviceId;
		String serialId;
		String utilitiesAdvancedMeteringSystemID;
		
		GenericMeter meter;
		
		public ProcessContext(MessageContent requestContent) {
			initialize(requestContent);
		}
		
		private void initialize(MessageContent requestContent) {
			if (requestContent == null)
				return;
			
			endDate = requestContent.getEndDate().toGregorianCalendar().getTime();
			materialId = requestContent.getMaterialId();
			deviceId = requestContent.getDeviceId();
			serialId = requestContent.getSerialId();
			utilitiesAdvancedMeteringSystemID = requestContent.getUtilitiesAdvancedMeteringSystemID();

			meter = IecWarehouse.getInstance().getMeterFactory().findBySerialId(serialId);
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestValidator {
		
		public static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);

			// Validate Fields:
			CommonValidatorFactory.validateValue(pc.endDate, "31/12/9999", "EndDate");
			CommonValidatorFactory.validateValue(pc.utilitiesAdvancedMeteringSystemID, "MDMA", "UtilitiesAdvancedMeteringSystemID");
			validateMeter(pc);
			
			CommonValidatorFactory.validateLookup(pc.materialId, "MaterialId", "meterModel", pc.meter.getFolderType()); // Meter is never null here

			validateDeviceConfiguration(pc);
		}
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateMeter(ProcessContext pc) throws AbstractValidationException {
			if (pc.meter == null) 
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Meter", pc.serialId);
		}

		private static void validateDeviceConfiguration(ProcessContext pc) throws FatalValidationException {
//			DeviceConfiguration c = pc.meter.getDevice().getDevice().getConfiguration();
			String deviceConfigurationName = pc.meter.getDevice().getDevice().getConfiguration().getName();
			if (!deviceConfigurationName.startsWith(pc.materialId))
				throw new FatalValidationException(WRONG_DEVICE_CONFIGURATION, "Device", pc.serialId, "Meter Device Configuration does not match " + pc.materialId);
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		public static void processMessageContent(ProcessContext pc, Logger logger) throws IecException {
			try {
				pc.meter.setExternalName(IecWarehouse.getInstance().getMeterFactory().buildExternalName(pc.deviceId, pc.meter));
				pc.meter.setStatus(new BigDecimal(2)); // Available - SAP
			} catch (Exception e) {
				logger.severe("Failed processing the message (General Error): " + e.getMessage());
				throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", pc.serialId);
			}

			updateMeter(pc.meter);
			logger.info("Meter Saved");
			logger.info("Message Processing Completed");
		}

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void updateMeter(GenericMeter meter) throws IecException {
			try {
				meter.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Meter", meter.getExternalName());
			}
		}
	}
}

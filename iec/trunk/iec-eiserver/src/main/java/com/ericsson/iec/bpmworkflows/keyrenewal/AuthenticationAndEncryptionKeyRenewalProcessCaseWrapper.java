package com.ericsson.iec.bpmworkflows.keyrenewal;

import com.energyict.hsm.worldline.model.exception.EnumLookupValueException;
import com.energyict.hsm.worldline.model.folderwrappers.KeyType;
import com.energyict.hsm.worldline.model.numberlookupwrappers.CryptographicType;
import com.energyict.massupdateactions.model.workflow.IndividualWorkflow;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.imp.ConsumptionRequest;
import com.ericsson.iec.model.GenericMeter;

import java.math.BigDecimal;
import java.util.Date;

public interface AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper extends IndividualWorkflow {

    void setMeter(GenericMeter meter);
    GenericMeter getMeter();

    void setDevice(Device device);
    Device getDevice();

    KeyType getAkKeyType();

    void setAkKeyType(KeyType akKeyType);

    KeyType getEkKeyType();

    void setEkKeyType(KeyType ekKeyType);

    CryptographicType getCryptographicType() throws EnumLookupValueException;

    void setCryptographicType(CryptographicType var1);

    Boolean getActive();

    void setActive(Boolean var1);

    Date getTimerDate();

    void setTimerDate(Date var1);

    BigDecimal getRemainingAttempts();

    void setRemainingAttempts(BigDecimal var1);

    Boolean getAttemptSuccessful();

    void setAttemptSuccessful(Boolean var1);

    Boolean getRetry();

    void setRetry(Boolean var1);

    Boolean getAbort();

    void setAbort(Boolean var1);

    void setMassUpdateActionItem(ConsumptionRequest var1);

    Date getExpiryDate();

    void setExpiryDate(Date var1);

}
package com.ericsson.iec.prototype;

import java.sql.SQLException;
import java.util.Date;

import com.energyict.cbo.BusinessException;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;
import com.ericsson.iec.model.Warehouse;

public interface ProtoType<FW extends FolderVersionWrapper> {

	void setActiveDate(Date activeDate);
	void setWarehouse(Warehouse warehouse);
	FW save() throws BusinessException, SQLException;
}

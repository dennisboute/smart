package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.BusinessObject;
import com.energyict.mdw.core.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class PortfolioMaintainerProcessor {

    private final Logger logger;

    public PortfolioMaintainerProcessor(Logger logger) {
        this.logger = logger;
    }

    public void process(Group group, MeterPortfolio meterPortfolio) throws BusinessException, SQLException {
        List<Channel> channels = new ArrayList<>();

        List<? extends BusinessObject> members = group.getMembers();
        for (BusinessObject businessObject : members) {
            if (businessObject instanceof Device) {
                Device device = (Device) businessObject;
                channels.addAll(device.getChannels());
            }
        }
        new PortfolioSynchronizer(meterPortfolio, logger).synchronize(channels);
    }

}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.relation.RelationAdapter;
import com.energyict.projects.common.model.relation.RelationWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;

import java.math.BigDecimal;

import static com.ericsson.iec.constants.AttributeTypeConstants.NocEventDetailsAttributes.*;

public class NocEventDetailsImpl extends RelationWrapperImpl implements NocEventDetails {

    private static final long serialVersionUID = 7158722396925345126L;

    protected NocEventDetailsImpl(RelationAdapter adapter) {
        super(adapter);
    }

    @Override
    public BigDecimal getEventCode() {
        return getBigDecimalAttribute(EVENT_CODE.getName());
    }

    @Override
    public void setEventCode(BigDecimal eventCode) {
        setBigDecimalAttribute(EVENT_CODE.getName(), eventCode);
    }

    @Override
    public BigDecimal getLogicalEventGroup() {
        return getBigDecimalAttribute(LOGICAL_EVENT_GROUP.getName());
    }

    @Override
    public void setLogicalEventGroup(BigDecimal logicalEventGroup) {
        setBigDecimalAttribute(EVENT_CODE.getName(), logicalEventGroup);
    }

    @Override
    public NocEventConfiguration getNocEventConfiguration() {
        return getFolderVersionWrapperAttribute(CONFIG_FOLDER.getName(), IecWarehouse.getInstance().getNocEventConfigurationFactory());
    }

    @Override
    public void setNocEventConfiguration(NocEventConfiguration nocEventConfiguration) {
        setFolderVersionWrapperAttribute(CONFIG_FOLDER.getName(), nocEventConfiguration);
    }


}

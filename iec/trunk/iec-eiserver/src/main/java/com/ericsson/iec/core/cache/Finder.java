package com.ericsson.iec.core.cache;

public interface Finder<K, V> {

    V findValue(K key);
}
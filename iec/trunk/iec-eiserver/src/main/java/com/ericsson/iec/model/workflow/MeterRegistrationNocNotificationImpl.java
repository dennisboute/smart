package com.ericsson.iec.model.workflow;

import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.METER;
import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.REGISTRATION_STATUS;
import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.RETRY;
import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.SERVICE_REQUEST;

import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.ProcessCaseWithExceptionHandling;


public class MeterRegistrationNocNotificationImpl extends ProcessCaseWithExceptionHandling implements MeterRegistrationNocNotification {

	private static final long serialVersionUID = -2801903355948795694L;

	protected MeterRegistrationNocNotificationImpl(ProcessCaseAdapter adapter) {
		super(adapter);
	}
	
	@Override
	public GenericMeter getMeter() {
		return getFolderVersionWrapperAttribute(METER.name, IecWarehouse.getInstance().getMeterFactory());
	}
	@Override
	public void setMeter(GenericMeter meter) {
		setFolderVersionWrapperAttribute(METER.name, meter);
	}
	
	@Override
	public String getRegistrationStatus() {
		return getStringAttribute(REGISTRATION_STATUS.name);
	}
	
	@Override
	public void setRegistrationStatus(String registrationStatus) {
		setStringAttribute(REGISTRATION_STATUS.name, registrationStatus);
	}

	@Override
	public Boolean getRetry() {
		return getBooleanAttribute(RETRY.name);
	}
	@Override
	public void setRetry(Boolean retry) {
		setBooleanAttribute(RETRY.name, retry);
	}

	@Override
	public void setServiceRequest(ServiceRequest serviceRequest) {
		setServiceRequestAttribute(SERVICE_REQUEST.name, serviceRequest);
	}

	@Override
	public ServiceRequest getServiceRequest() {
		return getServiceRequestAttribute(SERVICE_REQUEST.name);
	}
}

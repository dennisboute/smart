package com.ericsson.iec.service;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceEventFilter;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.model.DataConcentratorDeployment;

import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

public class CheckS15MessageResults extends AbstractServiceTransitionHandlerWithExceptionHandling<DataConcentratorDeployment>{

	public static final String TRANSITION_NAME = "CheckS15MessageResults";

	public CheckS15MessageResults(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(DataConcentratorDeployment dataConcentratorDeployment) throws IecException {
		DeviceEventFilter filter = new DeviceEventFilter();
		filter.setDeviceCode(1);
		filter.setLogBookType(MeteringWarehouse.getCurrent().getLogBookTypeFactory().find(120));
		Device device = dataConcentratorDeployment.getDataConcentrator().getDevice().getDevice();
		if (device.getEvents(filter).isEmpty()) {
			dataConcentratorDeployment.setDeploymentStatus("1");
			throw new FatalExecutionException(MESSAGE_PROCESSING_FAILED, "", "");
		}
		dataConcentratorDeployment.setDeploymentStatus("0");
	}
	
	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}

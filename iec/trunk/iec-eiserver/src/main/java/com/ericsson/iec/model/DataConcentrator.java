package com.ericsson.iec.model;

import java.math.BigDecimal;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface DataConcentrator extends FolderVersionWrapper {

	BigDecimal getStatus();
	void setStatus(BigDecimal status);

	DataConcentratorDevice getDevice();
	void setDevice(DataConcentratorDevice device);

	String getSerialId();
	void setSerialId(String serialId);

	String getManufacturer();
	void setManufacturer(String manufacturer);

	BigDecimal getManufacturingYear();
	void setManufacturingYear(BigDecimal manufacturingYear);

	String getModelDescription();
	void setModelDescription(String modelDescription);

	String getMacPlc();
	void setMacPlc(String macPlc);

	String getFirmwareVersion();
	void setFirmwareVersion(String firmwareVersion);

	String getComments();
	void setComments(String comments);
}

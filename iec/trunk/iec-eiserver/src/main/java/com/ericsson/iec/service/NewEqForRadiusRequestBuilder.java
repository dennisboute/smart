package com.ericsson.iec.service;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.*;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.MdusWebservice;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest.Request;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.Collections;
import java.util.UUID;

public class NewEqForRadiusRequestBuilder {

    private ObjectFactory objectFactory;
    private ClassInjector classInjector;

    public SapResponseMessage build(String serialNumber) throws SystemObjectNotDefined, SystemParameterIncorrectValue, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
        ServiceRequest serviceRequest = new ServiceRequestCreator().createServiceRequest(MdusWebservice.NEW_EQUIPMENT_FOR_RADIUS, UUID.randomUUID().toString(), Collections.emptyList(), null);
        NewEqForRadiusRequest request = buildNewEqForRadiusRequest();
        Request buildNewEqForRadiusRequestRequest = buildNewEqForRadiusRequestRequest();
        request.setRequest(buildNewEqForRadiusRequestRequest);
        NewEqForRadiusRequest.Request.EquipmentList EquipmentList = buildEquipmentList(serialNumber);
        buildNewEqForRadiusRequestRequest.getEquipmentList().add(EquipmentList);
        String message = marshall(request);
        SapResponseMessage responseMessage = new SapResponseMessage(MdusWebservice.NEW_EQUIPMENT_FOR_RADIUS, message, serviceRequest.getId());
        return responseMessage;
    }

    private NewEqForRadiusRequest buildNewEqForRadiusRequest() {
        return getSEObjectFactory().createNewEqForRadiusRequest();
    }

    private Request buildNewEqForRadiusRequestRequest() {
        return getSEObjectFactory().createNewEqForRadiusRequestRequest();
    }

    private Request.EquipmentList buildEquipmentList(String serialNumber) {
        NewEqForRadiusRequest.Request.EquipmentList EquipmentList = getSEObjectFactory().createNewEqForRadiusRequestRequestEquipmentList();
//        EquipmentList.setSerialNumber(serialNumber);
        return EquipmentList;
    }

    private String marshall(NewEqForRadiusRequest NewEqForRadiusRequest) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(NewEqForRadiusRequest.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter writer = new StringWriter();

            marshaller.marshal(NewEqForRadiusRequest, writer);
            return writer.toString();

        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private ClassInjector getClassInjector() {
        if (classInjector == null) {
            classInjector = new IecClassInjector();
        }
        return classInjector;
    }

    private ObjectFactory getSEObjectFactory() {
        if (objectFactory == null) {
            objectFactory = new ObjectFactory();
        }
        return objectFactory;
    }
}

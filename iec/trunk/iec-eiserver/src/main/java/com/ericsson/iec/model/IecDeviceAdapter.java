package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.DeviceConfiguration;
import com.energyict.mdw.relation.RelationType;
import com.energyict.projects.common.model.device.DeviceAdapterImpl;

public class IecDeviceAdapter extends DeviceAdapterImpl {

	private static final long serialVersionUID = 2551237751101265217L;

	protected IecDeviceAdapter(Device device, Date date) {
		super(device, date);
	}
	
	protected IecDeviceAdapter(DeviceConfiguration deviceConfiguration, Date date) {
		super(deviceConfiguration, date);
	}
	
    protected void setDeviceAttribute(Device device) {
        RelationType defaultRelationType = device.getDeviceType().getDefaultRelationType();
        if (defaultRelationType!=null) {
        	String defaultAttributeName = device.getDeviceType().getDefaultAttributeType().getName();
        	setAttribute(defaultAttributeName, device);
        }
    }


}

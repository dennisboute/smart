package com.ericsson.iec.exporter.cleandcfiles;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.eisexport.core.AbstractExporter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CleanDcFilesExporter extends AbstractExporter {
    private static final String RETENTION_DAYS_DC_FILES = "Retention days DC files";
    private static final String RETENTION_DAYS_DC_FILES_VALUE = "50";
    public static final String FOLDER_NAME = "Folder that contains files to be cleaned";
    public static final String FOLDER_NAME_VALUE = "7103";

    @Override
    protected void export() throws IOException, BusinessException, SQLException {
        Integer retentionDaysDcFiles = parseIntProperty(getProperty(RETENTION_DAYS_DC_FILES, RETENTION_DAYS_DC_FILES_VALUE));
        Integer folderId = parseIntProperty(getProperty(FOLDER_NAME, FOLDER_NAME_VALUE));
        CleanDcFilesProcessor cleanDcFilesProcessor = new CleanDcFilesProcessor(getLogger());
        cleanDcFilesProcessor.cleanDcFiles(retentionDaysDcFiles, folderId);
    }

    private Integer parseIntProperty(String value) {
        return Integer.valueOf(value);
    }

    @Override
    public List<PropertySpec> getRequiredProperties() {
        List<PropertySpec> requiredProperties = new ArrayList<>();
        requiredProperties.add(PropertySpecFactory.stringPropertySpec(RETENTION_DAYS_DC_FILES, RETENTION_DAYS_DC_FILES_VALUE));
        requiredProperties.add(PropertySpecFactory.stringPropertySpec(FOLDER_NAME, FOLDER_NAME_VALUE));
        return requiredProperties;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getVersion() {
        return null;
    }
}

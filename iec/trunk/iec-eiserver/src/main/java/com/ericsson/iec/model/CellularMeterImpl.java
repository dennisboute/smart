package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;

public class CellularMeterImpl extends GenericMeterImpl implements CellularMeter {

	private static final long serialVersionUID = -1871659369835667823L;

	protected CellularMeterImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

}


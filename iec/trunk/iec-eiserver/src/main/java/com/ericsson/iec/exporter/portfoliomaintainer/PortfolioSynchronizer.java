package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.cpo.ShadowList;
import com.energyict.mdw.core.*;
import com.energyict.mdw.shadow.MeterPortfolioItemShadow;
import com.energyict.mdw.shadow.MeterPortfolioShadow;
import com.energyict.projects.common.TimePeriodComparator;
import com.energyict.projects.dateandtime.Clock;
import com.ericsson.iec.core.exception.IecException;
import org.apache.commons.collections15.CollectionUtils;

import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

public class PortfolioSynchronizer {

    private final PortfolioSynchronizerLogger synchronizerLogger;
    private final MeterPortfolio portfolio;
    private ShadowList<MeterPortfolioItemShadow> allPortfolioItemShadows;

    public PortfolioSynchronizer(MeterPortfolio portfolio, Logger logger) {
        this.portfolio = portfolio;
        this.synchronizerLogger = new PortfolioSynchronizerLogger(portfolio, logger);
    }

    public PortfolioSynchronizer(MeterPortfolio portfolio, PortfolioSynchronizerLogger synchronizerLogger) {
        this.portfolio = portfolio;
        this.synchronizerLogger = synchronizerLogger;
    }

    public void synchronize(List<Channel> channels) throws BusinessException, SQLException {
        MeterPortfolioShadow portfolioShadow = portfolio.getShadow();
        allPortfolioItemShadows = portfolioShadow.getItemShadows();
        Map<Integer, SortedSet<MeterPortfolioItemShadow>> deviceId2itemShadows =
                PortfolioItemShadowsManager.INSTANCE.get().groupAndSortPortfolioItemsByChannelId(allPortfolioItemShadows);

        removeNondeviceItemShadows(deviceId2itemShadows);
        removeOrphanItemShadows(channels);
        for (Channel channel : channels) {
            List<MeterPortfolioItem> portfolioItemList = MeteringWarehouse.getCurrent().getMeterPortfolioItemFactory().findByPortfolioAndChannel(portfolio, channel);
            if (portfolioItemList.isEmpty()) {
                MeterPortfolioItemShadow meterPortfolioItemShadow = getMeterPortfolioItemShadow(channel);
                portfolioShadow.add(meterPortfolioItemShadow);
                synchronizerLogger.logDevicePortfolioItemCreation(meterPortfolioItemShadow);
            }
            synchronize(channel, deviceId2itemShadows.remove(channel.getId()));
        }
        portfolio.update(portfolioShadow);
    }

    private MeterPortfolioItemShadow getMeterPortfolioItemShadow(Channel channel) {
        MeterPortfolioItemShadow meterPortfolioItemShadow = new MeterPortfolioItemShadow();
        meterPortfolioItemShadow.setChannelId(channel.getId());
        meterPortfolioItemShadow.setPortfolioId(portfolio.getId());
        meterPortfolioItemShadow.setFromDate(Clock.INSTANCE.get().now());
        return meterPortfolioItemShadow;
    }

    public void synchronizeUniqueDevice(Channel channel) throws BusinessException, SQLException {
        MeterPortfolioShadow portfolioShadow = portfolio.getShadow();
        allPortfolioItemShadows = portfolioShadow.getItemShadows();
        Map<Integer, SortedSet<MeterPortfolioItemShadow>> deviceId2itemShadows =
                PortfolioItemShadowsManager.INSTANCE.get().groupAndSortPortfolioItemsByChannelId(allPortfolioItemShadows);
        synchronize(channel, deviceId2itemShadows.remove(channel.getId()));
        portfolio.update(portfolioShadow);
    }

    private void removeNondeviceItemShadows(
            Map<Integer, SortedSet<MeterPortfolioItemShadow>> deviceId2itemShadows) throws IecException {
        SortedSet<MeterPortfolioItemShadow> channelItemShadows = deviceId2itemShadows.remove(0);
        if (channelItemShadows != null) {
            for (MeterPortfolioItemShadow channelItemShadow : channelItemShadows) {
                synchronizerLogger.logChannelPortfolioItemRemoval(channelItemShadow);
                allPortfolioItemShadows.remove(channelItemShadow);
            }
        }
    }

    private void removeOrphanItemShadows(List<Channel> channels) throws IecException {
        Collection<Integer> DeviceIds = CollectionUtils.collect(channels, input -> input.getId());
        Iterator<MeterPortfolioItemShadow> it = allPortfolioItemShadows.iterator();
        while (it.hasNext()) {
            MeterPortfolioItemShadow itemShadow = it.next();
            if (!DeviceIds.contains(itemShadow.getChannelId())) {
                synchronizerLogger.logDevicePortfolioItemRemoval(itemShadow);
                it.remove();
            }
        }
    }

    private void synchronize(Channel channel, SortedSet<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        if (deviceItemShadows == null) {
            deviceItemShadows = new TreeSet<>();
        }
        synchronize(toPeriodChannels(channel), deviceItemShadows);
    }

    private void synchronize(List<PeriodChannel> periodChannels,
                             SortedSet<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        for (PeriodChannel periodChannel : periodChannels) {
            removePrecedingItemShadows(periodChannel, deviceItemShadows);
            mergeOverlappingItemShadowsOrCreateNewItem(periodChannel, deviceItemShadows);
        }
        removeRemainingItemShadows(deviceItemShadows);
    }

    private void removePrecedingItemShadows(final PeriodChannel periodChannel,
                                            SortedSet<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        Collection<MeterPortfolioItemShadow> itemShadows =
                PortfolioItemShadowsManager.INSTANCE.get().extractPrecedingItemShadows(
                        periodChannel.getFrom(), deviceItemShadows);
        for (MeterPortfolioItemShadow itemShadow : itemShadows) {
            synchronizerLogger.logDevicePortfolioItemRemoval(itemShadow);
            deviceItemShadows.remove(itemShadow);
            allPortfolioItemShadows.remove(itemShadow);
        }
    }

    private void mergeOverlappingItemShadowsOrCreateNewItem(PeriodChannel periodChannel, SortedSet<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        List<MeterPortfolioItemShadow> itemShadows =
                PortfolioItemShadowsManager.INSTANCE.get().extractOverlappingItemShadows(
                        periodChannel.getPeriod(), deviceItemShadows);
        if (itemShadows.isEmpty()) {
            itemShadows = createItemShadows(periodChannel);
            synchronizerLogger.logDevicePortfolioItemCreation(itemShadows);
            allPortfolioItemShadows.addAll(itemShadows);
        } else {
            for (MeterPortfolioItemShadow itemShadow : itemShadows.subList(0, itemShadows.size() - 1)) {
                synchronizerLogger.logDevicePortfolioItemRemoval(itemShadow);
                deviceItemShadows.remove(itemShadow);
                allPortfolioItemShadows.remove(itemShadow);
            }
            MeterPortfolioItemShadow itemShadowToUpdate = itemShadows.get(itemShadows.size() - 1);
            deviceItemShadows.remove(itemShadowToUpdate);
            updateItemShadow(itemShadowToUpdate, periodChannel.getPeriod());
        }
    }

    private void updateItemShadow(MeterPortfolioItemShadow itemShadow, TimePeriod period) throws IecException {
        TimePeriod itemShadowPeriod = new TimePeriod(itemShadow.getFromDate(), itemShadow.getToDate());
        if (TimePeriodComparator.INSTANCE.compare(period, itemShadowPeriod) != 0) {
            synchronizerLogger.logDevicePortfolioItemUpdate(itemShadow, period);
            itemShadow.setFromDate(period.getFrom());
            itemShadow.setToDate(period.getTo());
        }
    }

    private List<MeterPortfolioItemShadow> createItemShadows(PeriodChannel periodChannel) {
        List<MeterPortfolioItemShadow> itemShadows = new ArrayList<>();
        MeterPortfolioItemShadow itemShadow = new MeterPortfolioItemShadow();
        itemShadow.setFromDate(periodChannel.getFrom());
        itemShadow.setToDate(periodChannel.getTo());
        itemShadow.setChannelId(periodChannel.getChannel().getId());
        itemShadow.setPortfolioId(portfolio.getId());
        itemShadows.add(itemShadow);
        return itemShadows;
    }

    private void removeRemainingItemShadows(SortedSet<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        Iterator<MeterPortfolioItemShadow> it = deviceItemShadows.iterator();
        while (it.hasNext()) {
            MeterPortfolioItemShadow itemShadow = it.next();
            it.remove();
            synchronizerLogger.logDevicePortfolioItemRemoval(itemShadow);
            allPortfolioItemShadows.remove(itemShadow);
        }
    }

    private List<PeriodChannel> toPeriodChannels(Channel channel) {
        List<PeriodChannel> periodChannels =
                new ArrayList<>();

        List<MeterPortfolioItem> portfolioItems = MeteringWarehouse.getCurrent().getMeterPortfolioItemFactory().findByPortfolioAndChannel(portfolio, channel);
        for (MeterPortfolioItem item : portfolioItems) {
            periodChannels.add(new PeriodChannel(channel, item.getPeriod()));
        }

        Collections.sort(periodChannels);
        return periodChannels;
    }
}

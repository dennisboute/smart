package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.CITY_CODE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.CITY_NAME;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.COUNTRY;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.DISTRICT;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.LOGICAL_INSTALLATION_POINT_ID;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.POSTAL_CODE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.REGION;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.STREET;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterLocationAttributes.STREET_NUMBER;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

public class MeterLocationImpl extends FolderVersionWrapperImpl implements MeterLocation {

	private static final long serialVersionUID = -1871659369835667823L;

	protected MeterLocationImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public String getCountry() {
		return getStringAttribute(COUNTRY.name);
	}
	@Override
	public void setCountry(String country) {
		setStringAttribute(COUNTRY.name, country);
	}

	@Override
	public String getDistrict() {
		return getStringAttribute(DISTRICT.name);
	}
	@Override
	public void setDistrict(String district) {
		setStringAttribute(DISTRICT.name, district);
	}

	@Override
	public String getRegion() {
		return getStringAttribute(REGION.name);
	}
	@Override
	public void setRegion(String region) {
		setStringAttribute(REGION.name, region);
	}

	@Override
	public String getPostalCode() {
		return getStringAttribute(POSTAL_CODE.name);
	}
	@Override
	public void setPostalCode(String postalCode) {
		setStringAttribute(POSTAL_CODE.name, postalCode);
	}

	@Override
	public String getCityCode() {
		return getStringAttribute(CITY_CODE.name);
	}
	@Override
	public void setCityCode(String cityCode) {
		setStringAttribute(CITY_CODE.name, cityCode);
	}

	@Override
	public String getCityName() {
		return getStringAttribute(CITY_NAME.name);
	}
	@Override
	public void setCityName(String cityName) {
		setStringAttribute(CITY_NAME.name, cityName);
	}

	@Override
	public String getStreet() {
		return getStringAttribute(STREET.name);
	}
	@Override
	public void setStreet(String street) {
		setStringAttribute(STREET.name, street);
	}

	@Override
	public String getStreetNumber() {
		return getStringAttribute(STREET_NUMBER.name);
	}
	@Override
	public void setStreetNumber(String streetNumber) {
		setStringAttribute(STREET_NUMBER.name, streetNumber);
	}
	
	@Override
	public String getLogicalInstallationPointID() {
		return getStringAttribute(LOGICAL_INSTALLATION_POINT_ID.name);
	}
	@Override
	public void setLogicalInstallationPointID(String logicalInstallationPointID) {
		setStringAttribute(LOGICAL_INSTALLATION_POINT_ID.name, logicalInstallationPointID);
	}
}


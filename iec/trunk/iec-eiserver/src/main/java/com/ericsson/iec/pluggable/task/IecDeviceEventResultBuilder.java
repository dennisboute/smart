package com.ericsson.iec.pluggable.task;

import com.energyict.mdw.core.DeviceEvent;
import com.energyict.mdw.core.DeviceEventResultBuilder;

import java.util.ArrayList;
import java.util.List;

public class IecDeviceEventResultBuilder implements DeviceEventResultBuilder {
    private List<DeviceEvent> listOfDeviceEvents = new ArrayList<>();

    @Override
    public void add(DeviceEvent deviceEvent) {
        listOfDeviceEvents.add(deviceEvent);
    }

    public List<DeviceEvent> getListOfDeviceEvents() {
        return listOfDeviceEvents;
    }
}

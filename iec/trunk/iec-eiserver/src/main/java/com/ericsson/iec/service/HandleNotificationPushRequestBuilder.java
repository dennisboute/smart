package com.ericsson.iec.service;

import com.ericsson.iec.mdus.MdusWebservice;

public class HandleNotificationPushRequestBuilder extends HandleNotificationRequestBuilder {

    @Override
    protected MdusWebservice getMdusWebService() {
        return MdusWebservice.HANDLE_NOTIFICATION_METER_DATA_PUSH_REQUEST;
    }
}

package com.ericsson.iec.mdus.handler.servicerequest;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.cbo.TimePeriod;
import com.energyict.mdus.core.SapRequestMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusInstantiationException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.requesthandler.RequestHandlerParameters;
import com.energyict.mdw.shadow.imp.ConsumptionRequestShadow;
import com.ericsson.iec.core.IecWarehouse;

import sapmdm01.interfaces.mdm.iec.MeterLocationRequest;

public class MeterLocationHandler extends IecSingleRequestHandler<MeterLocationRequest>{

	public MeterLocationHandler() throws MarshallingException {
		super();
	}

	@Override
	public void handle(SapRequestMessage sapRequestMessage) throws BusinessException {
		MeterLocationRequest request = unmarshall(sapRequestMessage.getMessage(), MeterLocationRequest.class);
		String uuid = request.getMessageHeader().getMessageID();
		RequestHandlerParameters<MeterLocationRequest> parameters = 
				new RequestHandlerParameters<MeterLocationRequest>(sapRequestMessage, request, uuid);
		handle(parameters);
	}
	
	protected ConsumptionRequestShadow getConsumptionRequestShadow(RequestHandlerParameters<MeterLocationRequest> parameters) throws BusinessException {
		MeterLocationRequest singleRequest = parameters.getRequest();
		try {
			return buildConsumptionRequestShadow(singleRequest);
		} catch (JAXBException e) {
			throw new BusinessException(e);
		}
	}

	private ConsumptionRequestShadow buildConsumptionRequestShadow(MeterLocationRequest singleRequest) throws SystemObjectNotDefined, MdusInstantiationException, SystemParameterIncorrectValue, JAXBException {
		ConsumptionRequestShadow shadow = new ConsumptionRequestShadow();
		TimePeriod validityPeriod = IecWarehouse.getInstance().getIecClassInjector().getIecConsumptionRequestValidityPeriodCalculator().calculateForMeterLocationRequest(singleRequest);
		
		shadow.setName(singleRequest.getMessageContent().getDeviceId().toString());
		shadow.setExternalName(singleRequest.getMessageContent().getDeviceId().toString());
		shadow.setFrom(validityPeriod.getFrom());
		shadow.setTo(validityPeriod.getTo());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(MeterLocationRequest.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(singleRequest, writer);
		shadow.setRequest(writer.toString()); 
				
		return shadow;
	}
}

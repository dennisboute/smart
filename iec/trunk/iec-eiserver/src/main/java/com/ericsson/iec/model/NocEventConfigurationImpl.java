package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

import static com.ericsson.iec.constants.AttributeTypeConstants.CellularGridAttributes.IDENTIFIER;
import static com.ericsson.iec.constants.AttributeTypeConstants.NocEventConfigurationAttributes.DESCRIPTION;

public class NocEventConfigurationImpl extends FolderVersionWrapperImpl implements NocEventConfiguration {

	private static final long serialVersionUID = -6571113779229681175L;

	protected NocEventConfigurationImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public String getDescription() {
		return getStringAttribute(DESCRIPTION.name);
	}

}

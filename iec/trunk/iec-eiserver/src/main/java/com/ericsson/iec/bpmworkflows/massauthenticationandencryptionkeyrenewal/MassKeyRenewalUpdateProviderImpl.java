package com.ericsson.iec.bpmworkflows.massauthenticationandencryptionkeyrenewal;

import com.energyict.hsm.worldline.model.exception.ServiceRequestTypeNotFoundRuntimeException;
import com.energyict.hsm.worldline.relationwrappers.masskeyrenewal.MassKeyRenewalAttributesFactory;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapper;
import com.energyict.hsm.worldline.workflows.masskeyrenewal.MassKeyRenewalProcessCaseWrapper;
import com.energyict.hsm.worldline.workflows.masskeyrenewal.MassKeyRenewalProcessCaseWrapperFactory;
import com.energyict.massupdateactions.model.MassUpdateLogic;
import com.energyict.massupdateactions.model.MassUpdateProvider;
import com.energyict.massupdateactions.model.action.MassUpdateActionFactory;
import com.energyict.massupdateactions.model.workflow.MassUpdateWorkflow;
import com.energyict.mdw.core.Group;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.service.ServiceRequestType;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;

import java.util.concurrent.atomic.AtomicReference;

public class MassKeyRenewalUpdateProviderImpl implements MassUpdateProvider<KeyRenewalProcessCaseWrapper> {
    public static final AtomicReference<MassUpdateProvider<KeyRenewalProcessCaseWrapper>> INSTANCE = new AtomicReference(new MassKeyRenewalUpdateProviderImpl());
    private static final String SERVICE_REQUEST_TYPE_NAME = "Mass key renewal upgrade";
    private static final String KEY_RENEWAL_PROCESS_CASE = "KeyRenewalProcessCase";
    private static final String MASS_UPDATE_ACTION_ITEM = "massUpdateActionItem";

    public MassKeyRenewalUpdateProviderImpl() {
    }

    public ProcessCaseWrapperFactory<MassUpdateWorkflow<KeyRenewalProcessCaseWrapper>> getMassUpdateWorkflowFactory() {
        return (ProcessCaseWrapperFactory)MassKeyRenewalProcessCaseWrapperFactory.INSTANCE.get();
    }

    public MassUpdateActionFactory<KeyRenewalProcessCaseWrapper> getMassUpdateActionFactory() {
        return (MassUpdateActionFactory)MassKeyRenewalAttributesFactory.INSTANCE.get();
    }

    public ServiceRequestType getServiceRequestType() {
        ServiceRequestType serviceRequestType = MeteringWarehouse.getCurrent().getServiceRequestTypeFactory().findByExternalName("Mass key renewal upgrade");
        if (serviceRequestType == null) {
            throw new ServiceRequestTypeNotFoundRuntimeException("Mass key renewal upgrade");
        } else {
            return serviceRequestType;
        }
    }

    public String newServiceRequestName(MassUpdateWorkflow<KeyRenewalProcessCaseWrapper> paramMassUpdateWorkflow) {
        if (paramMassUpdateWorkflow instanceof MassKeyRenewalProcessCaseWrapper) {
            Group deviceGroup = ((MassKeyRenewalProcessCaseWrapper)paramMassUpdateWorkflow).getDeviceGroup();
            return "Mass key renewal upgrade - " + deviceGroup.getExternalName();
        } else {
            return null;
        }
    }

    public String getIndividualProcessRelationName() {
        return "KeyRenewalProcessCase";
    }

    public String getIndividualProcessItemReferenceAttributeName() {
        return "massUpdateActionItem";
    }

    public MassUpdateLogic<KeyRenewalProcessCaseWrapper> getMassUpdateLogic() {
        return (MassAkAndEkUpdateLogicImpl) MassAkAndEkUpdateLogicImpl.INSTANCE.get();
    }
}

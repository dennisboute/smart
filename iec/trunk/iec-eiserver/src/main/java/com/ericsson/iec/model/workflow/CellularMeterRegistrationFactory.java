package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;

public interface CellularMeterRegistrationFactory extends ProcessCaseWrapperFactory<CellularMeterRegistration> {

}

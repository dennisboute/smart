package com.ericsson.iec.prototype;

import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypes;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public class DataConcentratorProtoTypeFactoryImpl extends AbstractProtoTypeFactoryImpl implements DataConcentratorProtoTypeFactory {

	public DataConcentratorProtoTypeFactoryImpl() {
		super();
	}
	
	@Override
	public DataConcentratorProtoType find() throws FatalConfigurationException {
		return new DataConcentratorProtoTypeImpl(getCopySpec());
	}

	@Override
	public String getCopySpecName() {
		return ProtoTypes.CIRCUTOR_DATACONCENTRATOR.getName();
	}

	@Override
	public ProtoTypeObjectType getObjectType() {
		return ProtoTypeObjectType.DC;
	}
}

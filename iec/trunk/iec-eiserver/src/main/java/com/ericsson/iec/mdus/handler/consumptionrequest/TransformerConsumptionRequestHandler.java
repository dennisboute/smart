package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_CREATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_DELETE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_THE_VALUE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_ALREADY_EXISTS;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_DOES_NOT_EXISTS;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_HAS_CHILDREN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_HAS_REFERENCES;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.WRONG_MESSAGE_SEQUENCE;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractExecutionException;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.core.exception.NonFatalExecutionException;
import com.ericsson.iec.core.exception.NonFatalValidationException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory.ActionType;
import com.ericsson.iec.model.CellularGrid;
import com.ericsson.iec.model.CtMeterGrid;
import com.ericsson.iec.model.PlcGrid;
import com.ericsson.iec.model.Transformer;
import com.ericsson.iec.model.TransformerStation;

import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerRequest.MessageContent;

public class TransformerConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<TransformerRequest> {


	@Override
	protected TransformerRequest unmarshal(String request) throws MarshallingException {
		return (TransformerRequest) IecMarshallerHelper.getInstance().unmarshall(request, TransformerRequest.class);
	}
	
	@Override
	protected void doProcess(TransformerRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		MessageContent requestContent = message.getMessageContent();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(requestContent, processContext, logger);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class ProcessContext {
		String stationNumerator;
		String transformerName;
		String transformerExternalName;
		String cellularGridExternalName;
		String plcGridExternalName;
		String ctMeterGridExternalName;
		Date date;
		ActionType action;

		TransformerStation ts;
		Transformer transformer;
		CellularGrid cellularGrid;
		PlcGrid plcGrid;
		CtMeterGrid ctMeterGrid;
		
		int transformerCountByTs;
		
		public ProcessContext(MessageContent requestContent) {
			initialize(requestContent);
		}
		
		private void initialize(MessageContent requestContent) {
			if (requestContent == null)
				return;
		
			stationNumerator = requestContent.getSTATIONNUMERATOR();
			transformerName = requestContent.getNUMTRANSINSCHEMA();
			date = requestContent.getDATE().toGregorianCalendar().getTime();
			action = ActionType.getActionByName(requestContent.getACTION());
			
			ts = CommonObjectFactory.getTransformerStation(stationNumerator);
			transformerCountByTs = CommonObjectFactory.getTransformerCountByTs(ts);
			
			transformerExternalName = CommonObjectFactory.getTransformerExternalName(transformerName, ts); // support null ts
			cellularGridExternalName = transformerExternalName;
			plcGridExternalName = transformerExternalName;
			ctMeterGridExternalName = transformerExternalName;
			
			transformer = CommonObjectFactory.getTransformer(transformerExternalName);
			cellularGrid = CommonObjectFactory.getCellularGrid(cellularGridExternalName);
			plcGrid = CommonObjectFactory.getPlcGrid(plcGridExternalName);
			ctMeterGrid = CommonObjectFactory.getCtMeterGrid(ctMeterGridExternalName);
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestValidator {
		private static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);
	
			// Validate Fields:
			CommonValidatorFactory.validateStationNumerator(pc.stationNumerator);
			validateTransformerStation(pc);
			CommonValidatorFactory.validateCoordinate(requestContent.getTRANSKORDINATEX(), "TRANS_KORDINATE_X");
			CommonValidatorFactory.validateCoordinate(requestContent.getTRANSKORDINATEY(), "TRANS_KORDINATE_Y");
			CommonValidatorFactory.validateNumTransInSchema(pc.transformerName);
			CommonValidatorFactory.validatePlcCell(requestContent.getPLCCELL());
			validateDryOil(requestContent.getDRYOIL());
			CommonValidatorFactory.validateAction(pc.action);
	
			// Validate by Context:
			if (pc.action == ActionType.INSERT) { 
				if (pc.transformer != null)
					throw new NonFatalValidationException(OBJECT_ALREADY_EXISTS, "Transformer", pc.transformerExternalName);
				validateNumberOfTransformers(pc);
			} else { // UPDATE | REMOVE
				if (pc.transformer == null)
					throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer", pc.transformerExternalName);
			}
			
			if (pc.action == ActionType.UPDATE) {
				if (pc.transformer.getLatestNisUpdateDate().compareTo(pc.date) > 0)
					throw new NonFatalValidationException(WRONG_MESSAGE_SEQUENCE, pc.date, pc.transformer.getLatestNisUpdateDate());
			}

			if (pc.action == ActionType.REMOVE) {
				if (CommonObjectFactory.getDcCountByTransformer(pc.transformer) > 0)
					throw new NonFatalValidationException(OBJECT_HAS_CHILDREN, "Transformer", pc.transformerExternalName, "Data Concentrator");
				
				if (pc.cellularGrid != null && CommonObjectFactory.getPremiseCountByFolder(pc.cellularGrid.getFolder()) > 0)
					throw new NonFatalValidationException(OBJECT_HAS_CHILDREN, "Cellular Grid", pc.cellularGridExternalName, "Premise");
				if (pc.plcGrid != null && CommonObjectFactory.getPremiseCountByFolder(pc.plcGrid.getFolder()) > 0)
					throw new NonFatalValidationException(OBJECT_HAS_CHILDREN, "Plc Grid", pc.plcGridExternalName, "Premise");
				if (pc.ctMeterGrid != null && CommonObjectFactory.getPremiseCountByFolder(pc.ctMeterGrid.getFolder()) > 0)
					throw new NonFatalValidationException(OBJECT_HAS_CHILDREN, "CT Meter Grid", pc.ctMeterGridExternalName, "Premise");
	
	//			if (!pc.transformer.getFolder().canDelete())
	//				throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "Transformer", pc.transformer.getExternalName());
				if (!pc.cellularGrid.getFolder().canDelete())
					throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "Cellular Grid", pc.cellularGrid.getExternalName());
				if (!pc.plcGrid.getFolder().canDelete())
					throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "Plc Grid", pc.plcGrid.getExternalName());
				if (!pc.ctMeterGrid.getFolder().canDelete())
					throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "CT Meter Grid", pc.ctMeterGrid.getExternalName());
			}
		}
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateTransformerStation(ProcessContext pc) throws AbstractValidationException {
			if (pc.ts == null) 
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer Station", pc.stationNumerator);
		}
		
		private static void validateNumberOfTransformers(ProcessContext pc) throws AbstractValidationException {
			int allowedTransformerCount = pc.ts.getNumberOfTransformers().intValue();
			if (allowedTransformerCount < pc.transformerCountByTs + 1)
				throw new NonFatalValidationException(FIELD_DOES_NOT_MATCH_THE_VALUE, "The number of tranformers in station is ", allowedTransformerCount, "larger by 1 than the number of transformers currently linked to this station", pc.transformerCountByTs);
		}

		private static void validateDryOil(String dryOil) throws FatalValidationException {
			if (dryOil != null && !dryOil.isEmpty()) {
				String[] validValues = new String[] { "dry", "oil" };
				CommonValidatorFactory.validateValueList(dryOil, validValues, "DRY_OIL");
			}
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		private static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
			if (pc.action == ActionType.INSERT) {
				pc.transformer = createTransformer();
				pc.cellularGrid = createCellularGrid();
				pc.plcGrid = createPlcGrid();
				pc.ctMeterGrid = createCtMeterGrid();
			}
	
			if (pc.action == ActionType.REMOVE) {
				deleteGridFolder(pc.cellularGrid, "Cellular Grid");
				deleteGridFolder(pc.plcGrid, "Plc Grid");
				deleteGridFolder(pc.ctMeterGrid, "Ct Meter Grid");
				deleteTransformer(pc.transformer);
				logger.info("Transformer Deleted");
			} else { // insert / update:
				try {
					// Set Attributes:
					pc.transformer.setDistrict(pc.ts.getDistrict());
					pc.transformer.setRegion(pc.ts.getRegion());
					pc.transformer.setCity(pc.ts.getCity());
					pc.transformer.setTransformerStation(pc.ts);
					pc.transformer.setHighVoltageLineFeeder(requestContent.getHIGHVOLTAGELINEFEEDER());
					pc.transformer.setNameHighVoltageLineFeeder(requestContent.getNAMEHVOLTLINEFEEDER());
					pc.transformer.setCoordinates(CommonObjectFactory.getCoordinates(requestContent.getTRANSKORDINATEX(), requestContent.getTRANSKORDINATEY()));
					pc.transformer.setManufacturer(requestContent.getTRANSMANUFACTURER());
					pc.transformer.setSupplyVoltageAmpereRating(new BigDecimal(requestContent.getSUPLAYKVA()));
					pc.transformer.setDryOil(requestContent.getDRYOIL());
					pc.transformer.setTransformerType(requestContent.getPLCCELL());
					pc.transformer.setLatestNisUpdateDate(pc.date);
					
					pc.transformer.setName(pc.transformerName.replaceAll("/", " "));
					pc.transformer.setExternalName(FolderTypes.TRANSFORMER.buildExternalName(pc.transformerExternalName));
					pc.transformer.setActiveDate(new Date(0));
					pc.transformer.setFrom(new Date(0));
					pc.transformer.setParent(pc.ts.getFolder()); // Transformer is places under TS
				} catch (Exception e) {
					logger.severe("Failed processing the message (General Error): " + e.getMessage());
					throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Transformer", pc.transformerName);
				}
	
				updateTransformer(pc.transformer, pc.action);
				
				if (pc.action == ActionType.INSERT) {
					createCellularGrid(pc);
					createPlcGrid(pc);
					createCtMeterGrid(pc);
				}
					
				logger.info("Transformer Saved");
			}
			logger.info("Message Processing Completed");
		}
	
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
		private static Transformer createTransformer() {
			return IecWarehouse.getInstance().getTransformerFactory().createNew();
		}
		
		private static CellularGrid createCellularGrid() {
			return IecWarehouse.getInstance().getCellularGridFactory().createNew();
		}
	
		private static PlcGrid createPlcGrid() {
			return IecWarehouse.getInstance().getPlcGridFactory().createNew();
		}
	
		private static CtMeterGrid createCtMeterGrid() {
			return IecWarehouse.getInstance().getCtMeterGridFactory().createNew();
		}
	
		private static void updateTransformer(Transformer transformer, ActionType action) throws AbstractExecutionException {
			try {
				transformer.saveChanges();
			} catch (BusinessException | SQLException e) {
				if (action == ActionType.INSERT)
					throw new NonFatalExecutionException(e, CANNOT_CREATE_OBJECT, "Transformer", transformer.getExternalName());
				else
					throw new NonFatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Transformer", transformer.getExternalName());
			}
		}
	
		private static void deleteTransformer(Transformer transformer) throws AbstractExecutionException {
			try {
				transformer.deleteFolder();
			} catch (BusinessException | SQLException e) {
				throw new NonFatalExecutionException(e, CANNOT_DELETE_OBJECT, "Transformer", transformer.getExternalName());
			}
		}
		
		private static void createCellularGrid(ProcessContext pc) throws AbstractExecutionException {
			try {
				pc.cellularGrid.setIdentifier(pc.cellularGridExternalName);
				pc.cellularGrid.setName("01 - Cellular");
				pc.cellularGrid.setExternalName(FolderTypes.CELLULAR_GRID.buildExternalName(pc.cellularGridExternalName));
				pc.cellularGrid.setActiveDate(new Date(0));
				pc.cellularGrid.setFrom(new Date(0));
				pc.cellularGrid.setParent(pc.transformer.getFolder());
					
				pc.cellularGrid.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Cellular Grid", pc.cellularGridExternalName);
			}
		}
		
		private static void createPlcGrid(ProcessContext pc) throws AbstractExecutionException {
			try {
				pc.plcGrid.setIdentifier(pc.plcGridExternalName);
				pc.plcGrid.setName("02 - PLC");
				pc.plcGrid.setExternalName(FolderTypes.PLC_GRID.buildExternalName(pc.plcGridExternalName));
				pc.plcGrid.setActiveDate(new Date(0));
				pc.plcGrid.setFrom(new Date(0));
				pc.plcGrid.setParent(pc.transformer.getFolder());
					
				pc.plcGrid.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Plc Grid", pc.plcGridExternalName);
			}
		}
		
		private static void createCtMeterGrid(ProcessContext pc) throws AbstractExecutionException {
			try {
				pc.ctMeterGrid.setIdentifier(pc.ctMeterGridExternalName);
				pc.ctMeterGrid.setName("03 - CT Meter");
				pc.ctMeterGrid.setExternalName(FolderTypes.CT_METER_GRID.buildExternalName(pc.ctMeterGridExternalName));
				pc.ctMeterGrid.setActiveDate(new Date(0));
				pc.ctMeterGrid.setFrom(new Date(0));
				pc.ctMeterGrid.setParent(pc.transformer.getFolder());
					
				pc.ctMeterGrid.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "CT Meter Grid", pc.ctMeterGridExternalName);
			}
		}
	
		private static void deleteGridFolder(FolderVersionWrapper gridFolder, String objectName) throws AbstractExecutionException {
			try {
				gridFolder.deleteFolder();
			} catch (BusinessException | SQLException e) {
				throw new NonFatalExecutionException(e, CANNOT_DELETE_OBJECT, objectName, gridFolder.getExternalName());
			}
		}
	}
}

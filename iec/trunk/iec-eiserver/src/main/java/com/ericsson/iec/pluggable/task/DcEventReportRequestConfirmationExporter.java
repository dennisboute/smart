package com.ericsson.iec.pluggable.task;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exporter.ConfirmationExporter;
import com.energyict.mdw.service.ServiceRequestType;
import com.ericsson.iec.mdus.MdusWebservice;

public class DcEventReportRequestConfirmationExporter extends ConfirmationExporter {

	@Override
	protected ServiceRequestType getServiceRequestType() throws BusinessException {
		return MdusWebservice.DC_EVENT_REPORT_REQUEST.getServiceRequestType();
	}

}

package com.ericsson.iec.bpmworkflows.massauthenticationandencryptionkeyrenewal;

import com.energyict.bpm.core.ProcessCaseState;
import com.energyict.cbo.BusinessException;
import com.energyict.hsm.worldline.LoggingMessages;
import com.energyict.hsm.worldline.model.exception.MultipleKeyRenewalRelationsException;
import com.energyict.hsm.worldline.model.exception.MultipleKeyRenewalRelationsRuntimeException;
import com.energyict.hsm.worldline.model.exception.SavingChangesRuntimeException;
import com.energyict.hsm.worldline.model.folderwrappers.KeyType;
import com.energyict.hsm.worldline.model.folderwrappers.KeyTypeFactory;
import com.energyict.hsm.worldline.model.relationwrappers.KeyRenewal;
import com.energyict.hsm.worldline.model.systemparameters.enums.PrioritySystem;
import com.energyict.hsm.worldline.relationwrappers.masskeyrenewal.MassKeyRenewalAttributes;
import com.energyict.hsm.worldline.utils.KeyRenewalRelationUtils;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapper;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapperFactory;
import com.energyict.hsm.worldline.workflows.keyrenewal.utils.SupportedDeviceConfigurationUtils;
import com.energyict.hsm.worldline.workflows.masskeyrenewal.MassKeyRenewalProcessCaseWrapper;
import com.energyict.massupdateactions.model.MassUpdateLogic;
import com.energyict.massupdateactions.model.action.MassUpdateAction;
import com.energyict.massupdateactions.model.action.MassUpdateActionItem;
import com.energyict.massupdateactions.model.workflow.MassUpdateWorkflow;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.mdw.core.SystemParameter;
import com.energyict.projects.dateandtime.Clock;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MassAkAndEkUpdateLogicImpl implements MassUpdateLogic<KeyRenewalProcessCaseWrapper> {
    public static final AtomicReference<MassUpdateLogic<KeyRenewalProcessCaseWrapper>> INSTANCE = new AtomicReference(new MassAkAndEkUpdateLogicImpl());

    public MassAkAndEkUpdateLogicImpl() {
    }

    public boolean skipItem(MassUpdateActionItem paramMassUpdateActionItem, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        if (!this.checkDeviceIsPresent(paramMassUpdateActionItem, paramMassUpdateAction).and(this.checkDeviceConfigurationIsPresent(paramMassUpdateActionItem, paramMassUpdateAction)).and(this.checkDeviceHasNoOngoingRenewalProcess(paramMassUpdateActionItem, paramMassUpdateAction)).test(paramMassUpdateActionItem.getDevice())) {
            this.logWarnMessage(paramMassUpdateAction, LoggingMessages.SKIP_STARTING_INDIVIDUAL_PROCESS.getMessage(new Object[]{paramMassUpdateActionItem.getConsumptionRequest().getName()}));
            return true;
        } else {
            return false;
        }
    }

    private Predicate<Device> checkDeviceHasNoOngoingRenewalProcess(MassUpdateActionItem paramMassUpdateActionItem, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        KeyType keyType = ((MassKeyRenewalAttributes) paramMassUpdateAction).getKeyType();
        return (device) -> {
            return !((KeyRenewalRelationUtils) KeyRenewalRelationUtils.INSTANCE.get()).findKeyRenewalRelationsListByKeyTypeAndDevice(keyType, device).stream().filter((e) -> {
                return e.getCurrentUpdate() != null && ProcessCaseState.ONGOING.equals(e.getCurrentUpdate().getState());
            }).map((c) -> {
                this.logWarnMessage(paramMassUpdateAction, LoggingMessages.ONGOING_RENEWAL_PROCESSCASE_FOUND_FOR_DEVICE.getMessage(new Object[]{device.getName(), keyType.getName()}));
                return c;
            }).findAny().isPresent();
        };
    }

    private Predicate<Device> checkDeviceConfigurationIsPresent(MassUpdateActionItem paramMassUpdateActionItem, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        KeyType keyType = ((MassKeyRenewalAttributes) paramMassUpdateAction).getKeyType();
        return (device) -> {
            return ((SupportedDeviceConfigurationUtils) SupportedDeviceConfigurationUtils.INSTANCE.get()).findSupportedDeviceConfigurations(keyType, device.getConfiguration()).stream().findAny().orElseGet(() -> {
                this.logWarnMessage(paramMassUpdateAction, LoggingMessages.KEY_TYPE_DOES_NOT_HAVE_REQUIRED_DEVICE_CONFIGURATION.getMessage(new Object[]{keyType.getName(), device.getName()}));
                return null;
            }) != null;
        };
    }

    private Predicate<Device> checkDeviceIsPresent(MassUpdateActionItem paramMassUpdateActionItem, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        return (device) -> {
            return Optional.ofNullable(device).orElseGet(() -> {
                this.logWarnMessage(paramMassUpdateAction, LoggingMessages.NO_DEVICE_SET_FOR_MASSUPDATEACTIONITEM.getMessage(new Object[]{paramMassUpdateActionItem.getConsumptionRequest().getName()}));
                return null;
            }) != null;
        };
    }

    private void logWarnMessage(MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction, String message) {
        MassKeyRenewalAttributes attributes = (MassKeyRenewalAttributes) paramMassUpdateAction;
        attributes.getLogger().ifPresent((l) -> {
            l.warning(message);
        });
    }

    public boolean readyForActivation(MassUpdateActionItem paramMassUpdateActionItem, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        Device device = paramMassUpdateActionItem.getDevice();
        if (device == null) {
            return false;
        } else {
            if (paramMassUpdateAction instanceof MassKeyRenewalAttributes) {
                KeyType keyType = ((MassKeyRenewalAttributes) paramMassUpdateAction).getKeyType();
                List<? extends KeyRenewal> keyRenewalList = (List) ((KeyRenewalRelationUtils) KeyRenewalRelationUtils.INSTANCE.get()).findKeyRenewalByDeviceNonNullCurrentUpdate(device).stream().filter((k) -> {
                    return ProcessCaseState.ONGOING.equals(k.getCurrentUpdate().getState());
                }).collect(Collectors.toList());
                if (keyRenewalList == null || keyRenewalList.isEmpty()) {
                    return true;
                }

                KeyRenewal currentKeyRenewal;
                try {
                    currentKeyRenewal = ((KeyRenewalRelationUtils) KeyRenewalRelationUtils.INSTANCE.get()).findKeyRenewalRelationByKeyTypeAndDevice(keyType, device);
                } catch (MultipleKeyRenewalRelationsException var10) {
                    throw new MultipleKeyRenewalRelationsRuntimeException();
                }

                SystemParameter systemParameter = MeteringWarehouse.getCurrent().getSystemParameterFactory().find("keyRenewalPrioritySystem");
                PrioritySystem prioritySystem = null;
                if (systemParameter != null) {
                    prioritySystem = PrioritySystem.getByDisplayValue(systemParameter.getValue());
                } else {
                    prioritySystem = PrioritySystem.NONE;
                }

                if (PrioritySystem.NONE.equals(prioritySystem) || PrioritySystem.SEQUENTIAL.equals(prioritySystem)) {
                    return true;
                }

                if (PrioritySystem.EXPIRY_DATE_FIRST.equals(prioritySystem) || PrioritySystem.SEQUENTIAL_WITH_EXPIRY_DATE_FIRST.equals(prioritySystem)) {
                    Optional nonNullExpiryDateList;
                    if (currentKeyRenewal != null && currentKeyRenewal.getExpiryDate() != null) {
                        nonNullExpiryDateList = keyRenewalList.stream().filter((e) -> {
                            return e.getExpiryDate() != null && currentKeyRenewal.getExpiryDate().after(e.getExpiryDate());
                        }).findAny();
                        if (!nonNullExpiryDateList.isPresent()) {
                            return true;
                        }
                    } else {
                        nonNullExpiryDateList = keyRenewalList.stream().filter((e) -> {
                            return e.getExpiryDate() != null;
                        }).findAny();
                        if (!nonNullExpiryDateList.isPresent()) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }

    public void copyTypeSpecificAttributes(MassUpdateWorkflow<KeyRenewalProcessCaseWrapper> paramMassUpdateWorkflow, MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction) {
        if (paramMassUpdateWorkflow instanceof MassKeyRenewalProcessCaseWrapper && paramMassUpdateAction instanceof MassKeyRenewalAttributes) {
            MassKeyRenewalProcessCaseWrapper processCaseWrapper = (MassKeyRenewalProcessCaseWrapper) paramMassUpdateWorkflow;
            MassKeyRenewalAttributes attributes = (MassKeyRenewalAttributes) paramMassUpdateAction;
            attributes.setKeyType(processCaseWrapper.getKeyType());
            attributes.setLogger(paramMassUpdateWorkflow.getProcessCase().getLogger());

            try {
                attributes.saveChanges();
            } catch (SQLException | BusinessException var6) {
                throw new SavingChangesRuntimeException(var6);
            }
        }

    }

    public KeyRenewalProcessCaseWrapper startIndividualWorkflow(MassUpdateAction<KeyRenewalProcessCaseWrapper> paramMassUpdateAction, MassUpdateActionItem paramMassUpdateActionItem) {
        if (paramMassUpdateAction instanceof MassKeyRenewalAttributes) {
            String[] keyTypeSplit = ((MassKeyRenewalAttributes) paramMassUpdateAction).getKeyType().getName().split("_");
            List<KeyType> keyTypes = KeyTypeFactory.INSTANCE.get().findAll(Clock.INSTANCE.get().now()).stream().filter(keyType -> keyType.getName().contains(keyTypeSplit[0])).collect(Collectors.toList());
            Device device = paramMassUpdateActionItem.getDevice();
            KeyRenewalProcessCaseWrapper keyRenewalProcessCaseWrapper = (KeyRenewalProcessCaseWrapper) ((KeyRenewalProcessCaseWrapperFactory) KeyRenewalProcessCaseWrapperFactory.INSTANCE.get()).createNew();
            keyRenewalProcessCaseWrapper.setDevice(device);
            keyRenewalProcessCaseWrapper.setKeyType(keyTypes.get(0));
            keyRenewalProcessCaseWrapper.setMassUpdateActionItem(paramMassUpdateActionItem.getConsumptionRequest());

            KeyRenewalProcessCaseWrapper keyRenewalProcessCaseWrapper2 = (KeyRenewalProcessCaseWrapper) ((KeyRenewalProcessCaseWrapperFactory) KeyRenewalProcessCaseWrapperFactory.INSTANCE.get()).createNew();
            keyRenewalProcessCaseWrapper2.setDevice(device);
            keyRenewalProcessCaseWrapper2.setKeyType(keyTypes.get(1));
            keyRenewalProcessCaseWrapper2.setMassUpdateActionItem(paramMassUpdateActionItem.getConsumptionRequest());
            try {
                keyRenewalProcessCaseWrapper.saveChanges();
                keyRenewalProcessCaseWrapper2.saveChanges();
                return keyRenewalProcessCaseWrapper2;
            } catch (SQLException | BusinessException var7) {
                throw new SavingChangesRuntimeException(var7);
            }
        } else {
            return null;
        }
    }

    public void finalAttemptFailed(MassUpdateActionItem paramMassUpdateActionItem, String paramString) {
    }
}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface FirmwareVersion extends FolderVersionWrapper {

	String getFirmwareVersion();
	void setFirmwareVersion(String firmwareVersion);

}

package com.ericsson.iec.exporter.meterkey;

import com.energyict.cbo.BusinessException;
import com.energyict.mdw.export.ActionHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

class MeterKeyZiverQWriter implements MeterKeyWriter {
    private Logger logger;
    private ActionHandler actionHandler;

    public MeterKeyZiverQWriter(Logger logger, ActionHandler actionHandler) {
        this.logger = logger;
        this.actionHandler = actionHandler;
    }

    public void write(MeterKeyItem meterKeyItem) throws IOException, SQLException, BusinessException {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("keys");
            doc.appendChild(rootElement);

            for (MeterKeyModel model : meterKeyItem.getMeterKeyModels()) {
                Element key = doc.createElement("Key");
                String systemTitle = getSystemTitle(model.getGenericMeter().getDevice().getDevice().getSerialNumber());
                key.setAttribute("SysTitle", systemTitle);
                key.setAttribute("AK", model.getAuthenticationKey());
                key.setAttribute("GUK", model.getEncryptionKey());
                rootElement.appendChild(key);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            actionHandler.startNewFile("Keys_");
            StreamResult result = new StreamResult(actionHandler.getPrintWriter());
            transformer.transform(source, result);
        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

    private String getSystemTitle(String serialNumberMeter) {
        int length = serialNumberMeter.length();
        String last5Numbers = serialNumberMeter.substring(length - 5, length);
        String serialZiv = "ZIV" + last5Numbers;
        return asciiToHex(serialZiv).toUpperCase();
    }

    private static String asciiToHex(String asciiStr) {
        char[] chars = asciiStr.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char ch : chars) {
            hex.append(Integer.toHexString((int) ch));
        }

        return hex.toString();
    }
}

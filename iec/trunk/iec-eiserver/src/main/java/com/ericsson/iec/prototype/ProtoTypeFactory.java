package com.ericsson.iec.prototype;

import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public interface ProtoTypeFactory<P extends ProtoType > {

	P find() throws FatalConfigurationException;
	
	ProtoTypeObjectType getObjectType();
}

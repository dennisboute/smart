package com.ericsson.iec.service;

import com.energyict.mdw.service.ServiceRequest;
import com.energyict.mdw.service.ServiceRequestState;
import com.ericsson.iec.model.DataConcentratorDeployment;

import java.util.logging.Logger;

public class CheckDcFaultNotificationRequest {

    public static final String TRANSITION_NAME = "Check Dc Fault Notification";

    private Logger logger;

    public CheckDcFaultNotificationRequest(Logger logger) {
        this.logger = logger;
    }

    public final boolean process(DataConcentratorDeployment dataConcentratorDeployment) {
        ServiceRequest serviceRequest = dataConcentratorDeployment.getServiceRequest();
        return serviceRequest.getState() == ServiceRequestState.FAILED || serviceRequest.getState() == ServiceRequestState.SUCCESS;
    }

}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.relation.RelationWrapper;

import java.math.BigDecimal;

public interface NocEventDetails extends RelationWrapper {

	BigDecimal getEventCode();
	void setEventCode(BigDecimal eventCode);

	BigDecimal getLogicalEventGroup();
	void setLogicalEventGroup(BigDecimal logicalEventGroup);

	NocEventConfiguration getNocEventConfiguration();
	void setNocEventConfiguration(NocEventConfiguration nocEventConfiguration);


}

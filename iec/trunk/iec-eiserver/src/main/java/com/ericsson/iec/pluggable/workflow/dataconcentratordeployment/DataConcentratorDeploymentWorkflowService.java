package com.ericsson.iec.pluggable.workflow.dataconcentratordeployment;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.energyict.annotations.processcaseadapter.WorkFlowService;
import com.energyict.bpm.annotations.FlowCondition;
import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.annotations.WatchCondition;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.ProcessCase;
import com.energyict.bpm.core.WorkItem;
import com.energyict.cpo.BasicPropertySpec;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.mdw.core.MeteringWarehouse;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.DataConcentratorDeployment;
import com.ericsson.iec.model.DataConcentratorDeploymentFactory;
import com.ericsson.iec.service.*;

@WorkFlowService(version = "1.0")
public class DataConcentratorDeploymentWorkflowService extends AbstractWorkflowServiceWithExceptionHandling<DataConcentratorDeployment>  {

	private static final String PROPERTY_USER_FILE_ID = "User File ID";
	
	@ServiceMethod(name=InformNoc.TRANSITION_NAME)
	public CaseAttributes informNoc(WorkItem workItem, Logger logger) {
		return process(workItem, new InformNoc(logger));
	}

	@ServiceMethod(name=BuildAndQueueDcFaultNotification.TRANSITION_NAME)
	public CaseAttributes buildAndQueueDcFaultNotification(WorkItem workItem, Logger logger) {
		return process(workItem, new BuildAndQueueDcFaultNotification(logger));
	}

	@ServiceMethod(name=CheckDcFaultNotificationSent.TRANSITION_NAME)
	public CaseAttributes checkDcFaultNotificationSent(WorkItem workItem, Logger logger) {
		return process(workItem, new CheckDcFaultNotificationSent(logger));
	}
	
	@ServiceMethod(name=CreateB07MessageAndTriggerCommunication.TRANSITION_NAME)
	public CaseAttributes createB07MessageAndTriggerCommunication(WorkItem workItem, Logger logger) {
		Integer userFileId = ((BigDecimal)getTypedProperty(PROPERTY_USER_FILE_ID)).intValue();
		return process(workItem, new CreateB07MessageAndTriggerCommunication(logger, userFileId));
	}
	
	@ServiceMethod(name=CheckS15MessageResults.TRANSITION_NAME)
	public CaseAttributes checkS15MessageResults(WorkItem workItem, Logger logger) {
		return process(workItem, new CheckS15MessageResults(logger));
	}
	
	@ServiceMethod(name=CheckS12MessageResults.TRANSITION_NAME)
	public CaseAttributes checkS12MessageResults(WorkItem workItem, Logger logger) {
		return process(workItem, new CheckS12MessageResults(logger));
	}
	
	@ServiceMethod(name=CheckB07MessageResults.TRANSITION_NAME)
	public CaseAttributes checkB07MessageResults(WorkItem workItem, Logger logger) {
		return process(workItem, new CheckB07MessageResults(logger));
	}
	
	@FlowCondition(name = "Retry")
	public Boolean retry(ProcessCase processCase, Logger logger) {
		DataConcentratorDeployment processCaseWrapper = wrap(processCase);
		return processCaseWrapper.getRetry(); //TODO create retry attribute
	}
	
	@FlowCondition(name = "No Retry")
	public Boolean noRetry(ProcessCase processCase, Logger logger) {
		return !retry(processCase, logger);
	}
	
	@WatchCondition(name = CheckForB07Message.TRANSITION_NAME)
	public boolean checkB07Message(WorkItem workItem, CaseAttributes caseAttributes, Logger logger) {
		DataConcentratorDeployment wrapper = wrap(workItem);
		return new CheckForB07Message(logger).process(wrapper);
	}
	
	@WatchCondition(name = CheckForS15Message.TRANSITION_NAME)
	public boolean checkForS15Message(WorkItem workItem, CaseAttributes caseAttributes, Logger logger) {
		DataConcentratorDeployment wrapper = wrap(workItem);
		return new CheckForS15Message(logger).process(wrapper);
	}
	
	@WatchCondition(name = CheckForS12Message.TRANSITION_NAME)
	public boolean checkForS12Message(WorkItem workItem, CaseAttributes caseAttributes, Logger logger) {
		DataConcentratorDeployment wrapper = wrap(workItem);
		return new CheckForS12Message(logger).process(wrapper);
	}

	@WatchCondition(name = CheckDcFaultNotificationRequest.TRANSITION_NAME)
	public boolean checkDcFaultNotificationRequest(WorkItem workItem, CaseAttributes caseAttributes, Logger logger) {
		DataConcentratorDeployment wrapper = wrap(workItem);
		return new CheckDcFaultNotificationRequest(logger).process(wrapper);
	}
	
	
	@Override
	public String getVersion() {
		return "Version 1.0";
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getOptionalProperties() {
		return Collections.emptyList();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<PropertySpec> getRequiredProperties() {
		return Arrays.asList(PropertySpecFactory.bigDecimalPropertySpec(PROPERTY_USER_FILE_ID));
	}

	@Override
	public DataConcentratorDeploymentFactory getProcessCaseWrapperFactory() {
		return IecWarehouse.getInstance().getDataConcentratorDeploymentFactory();
	}

}

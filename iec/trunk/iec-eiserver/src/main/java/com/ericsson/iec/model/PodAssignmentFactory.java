package com.ericsson.iec.model;

import java.util.List;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;
import com.energyict.projects.common.model.relation.RelationWrapperFactory;

public interface PodAssignmentFactory extends RelationWrapperFactory<PodAssignment> {

	List<PodAssignment> findByReferences(GenericMeter meter, FolderVersionWrapper pointOfDelivery);

	List<PodAssignment> findByReferences(GenericMeter meter);

	List<PodAssignment> findByReferences(String uniqueId);
}

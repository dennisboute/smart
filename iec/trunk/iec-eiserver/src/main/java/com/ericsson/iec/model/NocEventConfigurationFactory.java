package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface NocEventConfigurationFactory extends FolderVersionWrapperFactory<NocEventConfiguration> {

	NocEventConfiguration findByKey(String key);

}

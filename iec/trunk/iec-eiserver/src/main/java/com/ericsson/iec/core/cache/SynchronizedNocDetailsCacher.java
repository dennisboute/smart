package com.ericsson.iec.core.cache;

import com.ericsson.iec.model.NocEventDetails;
import com.ericsson.iec.model.NocEventDetailsFactory;

public class SynchronizedNocDetailsCacher extends AbstractFinderCacher<String, NocEventDetails> {

    private static final int EXPECTED_NUMBER_OF_OBJECTS = 1;

    protected final NocEventDetailsFactory factory;

    public SynchronizedNocDetailsCacher(String businessObjectName, NocEventDetailsFactory factory) {
        super(businessObjectName);
        this.factory = factory;
    }

    @Override
    public NocEventDetails findValue(String eventCodeLogicalEventGroup) {
        NocEventDetails nocEventDetails = this.factory.findByCode(eventCodeLogicalEventGroup);
        return nocEventDetails;
    }
}

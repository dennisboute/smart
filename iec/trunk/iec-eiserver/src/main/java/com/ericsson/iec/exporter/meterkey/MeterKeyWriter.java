package com.ericsson.iec.exporter.meterkey;

import com.energyict.cbo.BusinessException;

import java.io.IOException;
import java.sql.SQLException;

public interface MeterKeyWriter {

    void write(MeterKeyItem meterKeyItem) throws IOException, SQLException, BusinessException;
}

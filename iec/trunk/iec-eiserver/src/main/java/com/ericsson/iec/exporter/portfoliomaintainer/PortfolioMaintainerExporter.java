package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.eisexport.core.AbstractExporter;
import com.energyict.mdw.core.Group;
import com.energyict.mdw.core.MeterPortfolio;
import com.energyict.projects.coreextensions.ExtendedPropertySpecFactory;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;

import java.sql.SQLException;
import java.util.List;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_FIND_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_NOT_ALLOWED;

public class PortfolioMaintainerExporter extends AbstractExporter {

    public String getDescription() {
        return "Updates portfolio containing channels from a group of Meters";
    }

    public static final String GROUP_PROPERTY_NAME = "Group of meters";
    public static final String PORTFOLIO_PROPERTY_NAME = "Portfolio to update";

    @Override
    protected void export() throws BusinessException, SQLException {
        Group group = getAndValidateGroupProperty();
        MeterPortfolio portfolio = getMeterPortfolioPropertyValidatedNotNull();
        PortfolioMaintainerProcessor processor = new PortfolioMaintainerProcessor(getLogger());
        processor.process(group, portfolio);
        processor.process(group, portfolio);
    }

    @Override
    public List<PropertySpec> getRequiredProperties() {
        List<PropertySpec> keys = super.getRequiredProperties();
        keys.add(PropertySpecFactory.groupReferencePropertySpec(GROUP_PROPERTY_NAME));
        keys.add(ExtendedPropertySpecFactory.meterPortfolioReference(PORTFOLIO_PROPERTY_NAME));
        return keys;
    }

    public String getVersion() {
        return "$Date: 2018-05-31 10:56:15 +0200 (do, 31 mei 2018) $";
    }

    private Group getAndValidateGroupProperty() throws FatalExecutionException, FatalValidationException {
        Group group = (Group) getProperties().getProperty(GROUP_PROPERTY_NAME);
        if (group == null) {
            throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Group", GROUP_PROPERTY_NAME);
        }
        if (!"Standard".equals(group.getSearchFilter().getFolderType().getName())) {
            throw new FatalValidationException(OBJECT_NOT_ALLOWED, "Group", group.getSearchFilter().getFolderType().getName(), "Group", GROUP_PROPERTY_NAME, "Group of devices expected.");
        }
        return group;
    }

    private MeterPortfolio getMeterPortfolioPropertyValidatedNotNull() throws FatalExecutionException {
        MeterPortfolio meterPortfolio = (MeterPortfolio) getProperties().getProperty(PORTFOLIO_PROPERTY_NAME);
        if (meterPortfolio == null) {
            throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Meter Portfolio", PORTFOLIO_PROPERTY_NAME);
        }
        return meterPortfolio;
    }
}

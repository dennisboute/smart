package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.constants.AttributeTypeConstants.RegionAttributes;
import com.ericsson.iec.core.IecWarehouse;

public class RegionImpl extends FolderVersionWrapperImpl implements Region {

	private static final long serialVersionUID = -1871659369835667822L;

	protected RegionImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public District getDistrict() {
		return getFolderVersionWrapperAttribute(RegionAttributes.DISTRICT.name, IecWarehouse.getInstance().getDistrictFactory());
	}
	@Override
	public void setDistrict(District district) {
		setFolderVersionWrapperAttribute(RegionAttributes.DISTRICT.name, district);
	}

	@Override
	public Date getLatestNisUpdateDate() {
		return getDateAndTimeAttribute(RegionAttributes.LATEST_NIS_UPDATE_DATE.name);
	}
	@Override
	public void setLatestNisUpdateDate(Date latestNisUpdateDate) {
		setDateAndTimeAttribute(RegionAttributes.LATEST_NIS_UPDATE_DATE.name, latestNisUpdateDate);
	}
}

package com.ericsson.iec.service;

import java.io.StringWriter;
import java.util.Collections;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusResourceBusyException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.MdusWebservice;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.DCFaultNotificationRequest.Request;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;

public class DcFaultNotificationRequestBuilder {

	private ObjectFactory objectFactory;
	private ClassInjector classInjector;

	public DcFaultNotificationRequestBuilder() {
	}
	
	public SapResponseMessage build(String dcID, String activityStep, String eventCode) throws SystemObjectNotDefined, SystemParameterIncorrectValue, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
		ServiceRequest serviceRequest = new ServiceRequestCreator().createServiceRequest(MdusWebservice.DC_FAULT_NOTIFICATION, UUID.randomUUID().toString(), Collections.emptyList(), null);
		DCFaultNotificationRequest dcFaultNotificationRequest = buildDCFaultNotificationRequest();
		Request request = buildRequest(dcID, activityStep, eventCode);
		dcFaultNotificationRequest.setRequest(request);
		String message = marshall(dcFaultNotificationRequest);
		SapResponseMessage responseMessage = new SapResponseMessage(MdusWebservice.DC_FAULT_NOTIFICATION, message, serviceRequest.getId());
		return responseMessage;
	}
	
	private DCFaultNotificationRequest buildDCFaultNotificationRequest() {
		return getSEObjectFactory().createDCFaultNotificationRequest();
		 
	}
	
	private Request buildRequest(String dcID, String activityStep, String eventCode ) {
		Request request = getSEObjectFactory().createDCFaultNotificationRequestRequest();
		request.setDCID(dcID);
		request.setActivityStep(activityStep);
		request.setEventCode(eventCode);
		return request;
	}
	
	private String marshall(DCFaultNotificationRequest dcFaultNotificationRequest) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(DCFaultNotificationRequest.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();
			marshaller.marshal(dcFaultNotificationRequest, writer);
			return writer.toString();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private ClassInjector getClassInjector() {
		if (classInjector==null) {
			classInjector=new IecClassInjector();
		}
		return classInjector;
	}
	
	private ObjectFactory getSEObjectFactory() {
		if (objectFactory == null) {
			objectFactory = new ObjectFactory();
		}
		return objectFactory;
	}
	
	
	
	
}

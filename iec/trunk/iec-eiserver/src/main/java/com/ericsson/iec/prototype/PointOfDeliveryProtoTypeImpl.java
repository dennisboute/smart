package com.ericsson.iec.prototype;

import static com.ericsson.iec.constants.ProtoTypeConstants.PointOfDeliveryParameters.POD_ID;

import com.energyict.mdw.core.Folder;
import com.energyict.mdw.template.CopySpec;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.PointOfDelivery;

public class PointOfDeliveryProtoTypeImpl extends AbstractProtoTypeImpl<PointOfDelivery> implements PointOfDeliveryProtoType {
	
	protected PointOfDeliveryProtoTypeImpl(CopySpec copySpec) {
		super(copySpec);
	}
	
	@Override
	public PointOfDelivery wrapFolder(Folder folder) {
		return IecWarehouse.getInstance().getPointOfDeliveryFactory().createNewForLastVersion(folder);
	}
	
	@Override
	public void setPodId(String podId) {
		setStringParameter(POD_ID.getName(), podId);
	}
}

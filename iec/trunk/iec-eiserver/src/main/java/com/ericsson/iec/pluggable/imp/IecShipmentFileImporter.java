package com.ericsson.iec.pluggable.imp;

import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.cpo.TypedProperties;
import com.energyict.hsm.worldline.imp.shipment.ShipmentFileImporter;
import com.energyict.hsm.worldline.imp.shipment.ShipmentFileProcessor;
import com.energyict.hsm.worldline.model.exception.SignatureVerificationOfShipmentFileFailedException;
import com.energyict.projects.common.exceptions.CustomerException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecExceptionReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IecShipmentFileImporter extends ShipmentFileImporter {
	private static final String SET_RADIUS_ACTIVE = "Set radius active";
	private static final boolean SET_RADIUS_ACTIVE_VALUE = false;

	@Override
	protected ShipmentFileProcessor getProcessor() {
		return new IecShipmentFileProcessor(getLogger(),isRadiusActive());
	}

	@Override
	public List<PropertySpec> getRequiredProperties() {
		List<PropertySpec> properties = super.getRequiredProperties();
		properties.add(PropertySpecFactory.notNullableBooleanPropertySpec(SET_RADIUS_ACTIVE, SET_RADIUS_ACTIVE_VALUE));
		return properties;
	}

	public boolean isRadiusActive() {
		return (boolean) getProperties().getProperty(SET_RADIUS_ACTIVE);
	}

	@Override
	protected void doImportFile() throws CustomerException, IOException {
		try{
			super.doImportFile();
		}catch(SignatureVerificationOfShipmentFileFailedException e){
			throw new FatalValidationException(IecExceptionReference.ValidationExceptionMessage.SIGNATURE_NOT_GENERATED_BY_MANUFACTURER);
		}

	}
}

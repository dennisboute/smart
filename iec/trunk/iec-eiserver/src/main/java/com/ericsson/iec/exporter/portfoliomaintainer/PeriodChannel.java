package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.TimePeriod;
import com.energyict.mdw.core.Channel;
import com.energyict.projects.common.TimePeriodComparator;

import java.util.Date;

class PeriodChannel implements Comparable<PeriodChannel> {
    private final Channel channel;
    private final TimePeriod timePeriod;

    PeriodChannel(Channel channel, TimePeriod timePeriod) {
        this.channel = channel;
        this.timePeriod = timePeriod;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public TimePeriod getPeriod() {
        return this.timePeriod;
    }

    public Date getFrom() {
        return getPeriod().getFrom();
    }

    public Date getTo() {
        return getPeriod().getTo();
    }

    public int compareTo(PeriodChannel other) {
        if (other == null) {
            return 1;
        }
        int result = getChannel().getId() - other.getChannel().getId();
        if (result != 0) {
            return result;
        }
        return TimePeriodComparator.INSTANCE.compare(getPeriod(), other.getPeriod());
    }
}

package com.ericsson.iec.service;

import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.*;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.MdusWebservice;
import com.ericsson.iec.model.MeterEventReportModel;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.HandleNotificationMeterDataRequest;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.HandleNotificationMeterDataRequest.Request;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.HandleNotificationMeterDataRequest.Request.MeterEventReport;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public abstract class HandleNotificationRequestBuilder {

    private ObjectFactory objectFactory;
    private ClassInjector classInjector;


    public SapResponseMessage build(List<MeterEventReportModel> meterEventReportModel) throws SystemObjectNotDefined, SystemParameterIncorrectValue, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
        ServiceRequest serviceRequest = new ServiceRequestCreator().createServiceRequest(getMdusWebService(), UUID.randomUUID().toString(), Collections.emptyList(), null);
        HandleNotificationMeterDataRequest registeredNotificationRequest = buildHandleNotificationMeterDataRequest();
        Request request = buildHandleNotificationMeterDataRequestRequest();
        registeredNotificationRequest.setRequest(request);
        for (MeterEventReportModel eventReportModel : meterEventReportModel) {
            MeterEventReport meterEventReport = buildMeterEventReport(eventReportModel);
            request.getMeterEventReport().add(meterEventReport);
        }
        String message = marshall(registeredNotificationRequest);
        SapResponseMessage responseMessage = new SapResponseMessage(getMdusWebService(), message, serviceRequest.getId());
        return responseMessage;
    }

    protected abstract MdusWebservice getMdusWebService();

    private HandleNotificationMeterDataRequest buildHandleNotificationMeterDataRequest() {
        return getSEObjectFactory().createHandleNotificationMeterDataRequest();

    }

    private Request buildHandleNotificationMeterDataRequestRequest() {
        return getSEObjectFactory().createHandleNotificationMeterDataRequestRequest();

    }

    private MeterEventReport buildMeterEventReport(MeterEventReportModel meterEventReportModel) {
        MeterEventReport meterEventReport = getSEObjectFactory().createHandleNotificationMeterDataRequestRequestMeterEventReport();
        meterEventReport.setUtilitiesDeviceID(meterEventReportModel.getUtilitiesDeviceId());
        meterEventReport.setEventCode(meterEventReportModel.getEventCode());
        meterEventReport.setAdditionalInfo(meterEventReportModel.getAdditionalInfo());
        meterEventReport.setTimeStamp(meterEventReportModel.getXmlGregorianCalendar());
        meterEventReport.setLogicalEventSubGroupId(meterEventReportModel.getLogicalEventSubGroup());
        meterEventReport.setLogicalEventGroupId(meterEventReportModel.getLogicalEventGroup());
        return meterEventReport;
    }

    private String marshall(HandleNotificationMeterDataRequest handleNotificationMeterDataRequest) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(HandleNotificationMeterDataRequest.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            StringWriter writer = new StringWriter();

            marshaller.marshal(handleNotificationMeterDataRequest, writer);
            return writer.toString();

        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private ClassInjector getClassInjector() {
        if (classInjector == null) {
            classInjector = new IecClassInjector();
        }
        return classInjector;
    }

    private ObjectFactory getSEObjectFactory() {
        if (objectFactory == null) {
            objectFactory = new ObjectFactory();
        }
        return objectFactory;
    }
}

package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.TimePeriod;
import com.energyict.mdw.core.*;
import com.energyict.mdw.shadow.MeterPortfolioItemShadow;
import com.energyict.projects.exception.DefaultObjectName;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_FIND_OBJECT;

public class PortfolioSynchronizerLogger {

    private final MeterPortfolio portfolio;
    private final Logger logger;

    public PortfolioSynchronizerLogger(MeterPortfolio portfolio, Logger logger) {
        this.logger = logger;
        this.portfolio = portfolio;
    }

    public void logChannelPortfolioItemRemoval(MeterPortfolioItemShadow itemShadow) throws IecException {
        if (logger == null) {
            return;
        }

        ChannelFactory channelFactory = MeteringWarehouse.getCurrent().getChannelFactory();
        int channelId = itemShadow.getChannelId();
        Channel channel = channelFactory.find(channelId);
        if (channel == null) {
            throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Channel", channelId);
        }
        logger.info(MessageFormat.format("Removing {0} ''{1}'' (id: {2}) from portfolio ''{3}'' over period {4}.",
                DefaultObjectName.CHANNEL.getObjectName(),
                channel.getName(),
                channel.getId(),
                portfolio.getName(),
                new TimePeriod(itemShadow.getFromDate(), itemShadow.getToDate())
        ));
    }

    public void logDevicePortfolioItemRemoval(MeterPortfolioItemShadow itemShadow) throws IecException {
        if (logger == null) {
            return;
        }

        Device Device = findDevice(itemShadow);
        logger.info(MessageFormat.format("Removing {0} ''{1}'' (id: {2}) from portfolio ''{3}'' over period {4}.",
                DefaultObjectName.CHANNEL.getObjectName(),
                Device.getName(),
                Device.getId(),
                portfolio.getName(),
                new TimePeriod(itemShadow.getFromDate(), itemShadow.getToDate())
        ));
    }

    public void logDevicePortfolioItemCreation(List<MeterPortfolioItemShadow> deviceItemShadows) throws IecException {
        if (logger == null) {
            return;
        }
        for (MeterPortfolioItemShadow meterPortfolioItemShadow : deviceItemShadows) {
            Device Device = findDevice(meterPortfolioItemShadow);
            logger.info(MessageFormat.format("Adding {0} ''{1}'' (id: {2}) to portfolio ''{3}'' over period {4}.",
                    DefaultObjectName.CHANNEL.getObjectName(),
                    Device.getName(),
                    Device.getId(),
                    portfolio.getName(),
                    new TimePeriod(meterPortfolioItemShadow.getFromDate(), meterPortfolioItemShadow.getToDate())
            ));
        }
    }

    public void logDevicePortfolioItemCreation(MeterPortfolioItemShadow meterPortfolioItemShadow) throws IecException {
        if (logger == null) {
            return;
        }
        Device Device = findDevice(meterPortfolioItemShadow);
        logger.info(MessageFormat.format("Adding {0} ''{1}'' (id: {2}) to portfolio ''{3}'' over period {4}.",
                DefaultObjectName.CHANNEL.getObjectName(),
                Device.getName(),
                Device.getId(),
                portfolio.getName(),
                new TimePeriod(meterPortfolioItemShadow.getFromDate(), meterPortfolioItemShadow.getToDate())
        ));
    }

    public void logDevicePortfolioItemUpdate(MeterPortfolioItemShadow deviceItemShadow, TimePeriod newPeriod) throws IecException {
        if (logger == null) {
            return;
        }

        Device device = findDevice(deviceItemShadow);
        logger.info(MessageFormat.format("Resizing {0} ''{1}'' (id: {2}) of portfolio ''{3}'' from period {4} to period {5}.",
                DefaultObjectName.CHANNEL.getObjectName(),
                device.getName(),
                device.getId(),
                portfolio.getName(),
                new TimePeriod(deviceItemShadow.getFromDate(), deviceItemShadow.getToDate()),
                newPeriod)
        );
    }

    private Device findDevice(MeterPortfolioItemShadow itemShadow) throws IecException {
        int channelId = itemShadow.getChannelId();
        Channel channel = MeteringWarehouse.getCurrent().getChannelFactory().find(channelId);
        Device device = channel.getDevice();
        if (device == null) {
            throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Channel", channel);
        }
        return device;
    }

}

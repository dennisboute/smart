package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.COMMENTS;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.DEVICE;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.FIRMWARE_VERSION;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.MAC_PLC;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.MANUFACTURER;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.MANUFACTURING_YEAR;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.MODEL_DESCRIPTION;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.SERIAL_ID;
import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorAttributes.STATUS;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.core.IecWarehouse;

public class DataConcentratorImpl extends FolderVersionWrapperImpl implements DataConcentrator {

	private static final long serialVersionUID = -1871659369835667822L;

	protected DataConcentratorImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public BigDecimal getStatus() {
		return getBigDecimalAttribute(STATUS.name);
	}
	@Override
	public void setStatus(BigDecimal status) {
		setBigDecimalAttribute(STATUS.name, status);
	}

	@Override
	public DataConcentratorDevice getDevice() {
		DataConcentratorDeviceFactory d = IecWarehouse.getInstance().getDataConcentratorDeviceFactory(); 
		return d.createNew(getDeviceAttribute(DEVICE.name), new Date(0)); // TBD: set correct conversion
	}
	@Override
	public void setDevice(DataConcentratorDevice device) {
		setDeviceAttribute(DEVICE.name, device.getDevice()); // TBD: make sure correct conversion
	}

	@Override
	public String getSerialId() {
		return getStringAttribute(SERIAL_ID.name);
	}
	@Override
	public void setSerialId(String serialId) {
		setStringAttribute(SERIAL_ID.name, serialId);
	}

	@Override
	public String getManufacturer() {
		return getStringAttribute(MANUFACTURER.name);
	}
	@Override
	public void setManufacturer(String manufacturer) {
		setStringAttribute(MANUFACTURER.name, manufacturer);
	}

	@Override
	public BigDecimal getManufacturingYear() {
		return getBigDecimalAttribute(MANUFACTURING_YEAR.name);
	}
	@Override
	public void setManufacturingYear(BigDecimal manufacturingYear) {
		setBigDecimalAttribute(MANUFACTURING_YEAR.name, manufacturingYear);
	}

	@Override
	public String getModelDescription() {
		return getStringAttribute(MODEL_DESCRIPTION.name);
	}
	@Override
	public void setModelDescription(String modelDescription) {
		setStringAttribute(MODEL_DESCRIPTION.name, modelDescription);
	}

	@Override
	public String getMacPlc() {
		return getStringAttribute(MAC_PLC.name);
	}
	@Override
	public void setMacPlc(String macPlc) {
		setStringAttribute(MAC_PLC.name, macPlc);
	}

	@Override
	public String getFirmwareVersion() {
		return getStringAttribute(FIRMWARE_VERSION.name);
	}
	
	@Override
	public void setFirmwareVersion(String firmwareVersion) {
		setStringAttribute(FIRMWARE_VERSION.name, firmwareVersion);
	}

	@Override
	public String getComments() {
		return getStringAttribute(COMMENTS.name);
	}
	@Override
	public void setComments(String comments) {
		setStringAttribute(COMMENTS.name, comments);
	}
}

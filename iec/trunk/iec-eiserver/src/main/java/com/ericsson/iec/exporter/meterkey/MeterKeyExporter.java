package com.ericsson.iec.exporter.meterkey;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.PropertySpec;
import com.energyict.cpo.PropertySpecFactory;
import com.energyict.eisexport.core.AbstractExporter;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.exception.FatalExecutionException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.NO_PERMISSION;

public class MeterKeyExporter extends AbstractExporter {
    public static final String METER_GROUP_NAME = "Group of meters";
    private MeterKeyProcessor processor;

    @Override
    protected void export() throws IOException, BusinessException, SQLException {
    	validateExecutionPermissions(getLogger());
        processor = new MeterKeyProcessor();
        for (MeterKeyWriter writer : getMeterKeyWriters()) {
            List<MeterKeyItem> meterKeyItems = processor.process(getProperties().getTypedProperty(METER_GROUP_NAME));
            if (!meterKeyItems.isEmpty()) {
                for (MeterKeyItem meterKeyItem : meterKeyItems) {
                    writer.write(meterKeyItem);
                }
            }
        }
    }

    @Override
    public List<PropertySpec> getRequiredProperties() {
        List<PropertySpec> requiredProperties = new ArrayList<>();
        requiredProperties.add(PropertySpecFactory.groupReferencePropertySpec(METER_GROUP_NAME));
        return requiredProperties;
    }

    @Override
    public String getDescription() {
        return "Meter Key Exporter to export meter keys";
    }

    @Override
    public String getVersion() {
        return "v0.1";
    }

    public List<MeterKeyWriter> getMeterKeyWriters() {
        List<MeterKeyWriter> meterKeyWriters = new ArrayList<>();
        meterKeyWriters.add(new MeterKeyB31Writer(getLogger(), getHandler()));
        meterKeyWriters.add(new MeterKeyZiverQWriter(getLogger(), getHandler()));
        meterKeyWriters.add(new MeterKeyShipmentWriter(getLogger(), getHandler()));
        return meterKeyWriters;
    }
    
    private void validateExecutionPermissions(Logger logger) throws BusinessException {
        try {
        	InetAddress host = InetAddress.getLocalHost();
			String hostIp = host.getHostAddress();
	        String allowedHostList = IecSystemParameters.METER_KEY_EXPORTER_ALLOWED_SERVERS.getStringValue();
	        if (allowedHostList.contains(hostIp))
	        	logger.info("Validated server for execution: " + hostIp);
	        else
	            throw new FatalExecutionException(NO_PERMISSION, "This server is not allowed for meter key exporting: " + hostIp);
		} catch (UnknownHostException e) {
			e.printStackTrace();
            throw new FatalExecutionException(e, NO_PERMISSION, "Failed locating host address");
		}
    }
}

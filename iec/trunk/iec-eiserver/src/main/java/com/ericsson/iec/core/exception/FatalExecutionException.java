package com.ericsson.iec.core.exception;

import com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage;

public class FatalExecutionException extends AbstractExecutionException {

	private static final long serialVersionUID = -4161286101151051217L;

	public FatalExecutionException(ExecutionExceptionMessage exceptionCode, Object... messageParameters) {
		super(exceptionCode, true, messageParameters);
	}

	public FatalExecutionException(Throwable cause, ExecutionExceptionMessage exceptionCode, Object... messageParameters) {
		super(cause, exceptionCode, true, messageParameters);
	}

}

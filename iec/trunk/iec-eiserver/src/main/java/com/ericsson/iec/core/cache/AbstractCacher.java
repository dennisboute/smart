package com.ericsson.iec.core.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractCacher<K, V> implements Cacher<K, V> {

    /**Cache Location**/
    protected Map<K,V> cache;
    protected String businessObjectName;

    public AbstractCacher() {
        super();
        cache = new ConcurrentHashMap<>();
    }

    public AbstractCacher(String businessObjectName) {
        this();
        this.businessObjectName = businessObjectName;
    }
}

package com.ericsson.iec.service;

import com.ericsson.iec.mdus.MdusWebservice;

public class HandleNotificationPullRequestBuilder extends HandleNotificationRequestBuilder {
    @Override
    protected MdusWebservice getMdusWebService() {
        return MdusWebservice.HANDLE_NOTIFICATION_METER_DATA_PULL_REQUEST;
    }
}

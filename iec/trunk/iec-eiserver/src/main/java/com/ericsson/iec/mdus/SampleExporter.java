package com.ericsson.iec.mdus;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.energyict.cbo.BusinessException;
import com.energyict.eisexport.core.AbstractExporter;
import com.energyict.mdus.core.ClassInjector;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.SapWebService;
import com.energyict.mdus.core.ServiceRequestCreator;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusResourceBusyException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.mdus.options.NewEquipmentForRadiusOptions;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest.Request;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.NewEqForRadiusRequest.Request.EquipmentList;
import iec.tibco.mdm.ws_mdm_rad_01.resources.xsd.abs.ws_mdm_rad_01.ObjectFactory;

public class SampleExporter extends AbstractExporter {

	private ClassInjector classInjector;
	private ObjectFactory objectFactory;

	@Override
	protected void export() throws IOException, BusinessException, SQLException {

		String serial = "SERIALID001_"+new Date().getTime();
		ServiceRequest serviceRequest = createServiceRequest(MdusWebservice.NEW_EQUIPMENT_FOR_RADIUS, serial);
		
		Date from = getPeriod().getFrom();
		Date to = getPeriod().getTo();

		NewEquipmentForRadiusOptions options = new NewEquipmentForRadiusOptions(serial, from, to);

		ObjectFactory objectFactory = getSEObjectFactory();
		NewEqForRadiusRequest newEqRadiusRequest = objectFactory.createNewEqForRadiusRequest();
		
		Request request1 = objectFactory.createNewEqForRadiusRequestRequest();
		request1.setEquipmentType("M2M");
		
		EquipmentList equipmentList = new EquipmentList();
//		equipmentList.setSerialNumber(options.getSerialId());
		request1.getEquipmentList().add(equipmentList);
		
		newEqRadiusRequest.setRequest(request1);
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(NewEqForRadiusRequest.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();

			marshaller.marshal(newEqRadiusRequest, writer);
			SapResponseMessage message = new SapResponseMessage(MdusWebservice.NEW_EQUIPMENT_FOR_RADIUS, writer.toString(), serviceRequest.getId());
			getClassInjector().getMessageSender().sendMessage(message);
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	private ObjectFactory getSEObjectFactory() {
		if (objectFactory == null) {
			objectFactory = new ObjectFactory();
		}
		return objectFactory;
	}
	
	protected final ServiceRequest createServiceRequest(SapWebService sapWebService, String externalName) throws MdusBusinessException, MdusSqlException, SystemObjectNotDefined, SystemParameterIncorrectValue, MdusResourceBusyException {
		return new ServiceRequestCreator().createServiceRequest(sapWebService, externalName, Collections.emptyList(), null);
	}

	private ClassInjector getClassInjector() {
		if (classInjector==null) {
			classInjector=new IecClassInjector();
		}
		return classInjector;
	}

	public String getDescription() {
		return "SAMPLE DESCRIPTION";
	}

	public String getVersion() {
		return "$Date: 2013-10-02 16:25:01 +0200 (mer., 02 oct. 2013) $";
	}

}

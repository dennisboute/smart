package com.ericsson.iec.bpmworkflows.keyrenewal;

import com.energyict.hsm.worldline.model.exception.EnumLookupValueException;
import com.energyict.hsm.worldline.model.folderwrappers.KeyType;
import com.energyict.hsm.worldline.model.folderwrappers.KeyTypeFactory;
import com.energyict.hsm.worldline.model.numberlookupwrappers.CryptographicType;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.imp.ConsumptionRequest;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;
import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.ProcessCaseWithExceptionHandling;

import java.math.BigDecimal;
import java.util.Date;

import static com.ericsson.iec.constants.AttributeTypeConstants.AuthenticationAndEncryptionKeyRenewalAttributes.*;
import static com.ericsson.iec.constants.AttributeTypeConstants.PlcMeterRegistrationAttributes.METER;

public class AuthenticationAndEncryptionKeyRenewalProcessCaseWrapperImpl extends ProcessCaseWithExceptionHandling implements AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper {

    private static final long serialVersionUID = -8201418965748795694L;

    protected AuthenticationAndEncryptionKeyRenewalProcessCaseWrapperImpl(ProcessCaseAdapter adapter) {
        super(adapter);
    }

    @Override
    public GenericMeter getMeter() {
        return getFolderVersionWrapperAttribute(METER.name, IecWarehouse.getInstance().getPlcMeterFactory());
    }

    @Override
    public void setMeter(GenericMeter meter) {
        setFolderVersionWrapperAttribute(METER.name, meter);
    }

    @Override
    public Device getDevice() {
        return this.getDeviceAttribute(DEVICE.getName());
    }

    @Override
    public void setDevice(Device device) {
        this.setDeviceAttribute(DEVICE.getName(), device);
    }

    @Override
    public KeyType getAkKeyType() {
        return (KeyType) this.getFolderVersionWrapperAttribute(AK_KEY_TYPE.getName(), (FolderVersionWrapperFactory) KeyTypeFactory.INSTANCE.get());
    }

    @Override
    public void setAkKeyType(KeyType akKeyType) {
        this.setFolderVersionWrapperAttribute(AK_KEY_TYPE.getName(), akKeyType);
    }

    @Override
    public KeyType getEkKeyType() {
        return (KeyType) this.getFolderVersionWrapperAttribute(EK_KEY_TYPE.getName(), (FolderVersionWrapperFactory) KeyTypeFactory.INSTANCE.get());
    }

    @Override
    public void setEkKeyType(KeyType ekKeyType) {
        this.setFolderVersionWrapperAttribute(EK_KEY_TYPE.getName(), ekKeyType);
    }

    @Override
    public CryptographicType getCryptographicType() throws EnumLookupValueException {
        return null;
    }

    @Override
    public void setCryptographicType(CryptographicType var1) {

    }

    @Override
    public Boolean getActive() {
        return null;
    }

    @Override
    public void setActive(Boolean var1) {

    }

    @Override
    public Date getTimerDate() {
        return null;
    }

    @Override
    public void setTimerDate(Date var1) {

    }

    @Override
    public BigDecimal getRemainingAttempts() {
        return null;
    }

    @Override
    public void setRemainingAttempts(BigDecimal var1) {

    }

    @Override
    public Boolean getAttemptSuccessful() {
        return null;
    }

    @Override
    public void setAttemptSuccessful(Boolean var1) {

    }

    @Override
    public Boolean getRetry() {
        return null;
    }

    @Override
    public void setRetry(Boolean var1) {

    }

    @Override
    public Boolean getAbort() {
        return null;
    }

    @Override
    public void setAbort(Boolean var1) {

    }

    @Override
    public void setMassUpdateActionItem(ConsumptionRequest var1) {

    }

    @Override
    public Date getExpiryDate() {
        return null;
    }

    @Override
    public void setExpiryDate(Date var1) {

    }

    @Override
    public ConsumptionRequest getMassUpdateActionItem() {
        return null;
    }
}


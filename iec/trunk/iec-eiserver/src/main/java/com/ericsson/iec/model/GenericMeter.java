package com.ericsson.iec.model;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.mdw.core.VirtualMeter;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface GenericMeter extends FolderVersionWrapper {

	Premise getPremise();
	void setPremise(Premise premise);

	BigDecimal getStatus();
	void setStatus(BigDecimal status);

	String getMeterModel();
	void setMeterModel(String meterModel);

	GenericMeterDevice getDevice();
	void setDevice(GenericMeterDevice device);

	String getSerialId();
	void setSerialId(String serialId);

	String getManufacturer();
	void setManufacturer(String manufacturer);

	String getBatch();
	void setBatch(String batch);

	public FirmwareVersion getFirmwareVersion();
	void setFirmwareVersion(FirmwareVersion firmwareVersion);

	Date getDeliveryDate();
	void setDeliveryDate(Date deliveryDate);

	Date getConfigurationDate();
	void setConfigurationDate(Date configurationDate);

	MeterLocation getMeterLocation();
	void setMeterLocation(MeterLocation meterLocation);

	VirtualMeter getVirtualMeter();
	void setVirtualMeter(VirtualMeter virtualMeter);

	String getComments();
	void setComments(String comments);

	String getMacAddress();
	void setMacAddress(String macAddress);
	
	String getSapId();
}

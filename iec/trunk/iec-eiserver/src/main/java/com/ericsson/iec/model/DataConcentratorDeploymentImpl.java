package com.ericsson.iec.model;

import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.ericsson.iec.core.IecWarehouse;

import static com.ericsson.iec.constants.AttributeTypeConstants.DataConcentratorDeploymentAttributes.*;


public class DataConcentratorDeploymentImpl extends ProcessCaseWithExceptionHandling implements DataConcentratorDeployment {

	private static final long serialVersionUID = -2801903355948795694L;

	protected DataConcentratorDeploymentImpl(ProcessCaseAdapter adapter) {
		super(adapter);
	}
	
	@Override
	public DataConcentrator getDataConcentrator() {
		return getFolderVersionWrapperAttribute(DATA_CONCENTRATOR.name, IecWarehouse.getInstance().getDataConcentratorFactory());
	}

	@Override
	public void setDataConcentrator(DataConcentrator dataConcentrator) {
		setFolderVersionWrapperAttribute(DATA_CONCENTRATOR.name, dataConcentrator);
	}
	
	@Override
	public String getDeploymentStatus() {
		return getStringAttribute(DEPLOYMENT_STATUS.name);
	}
	
	@Override
	public void setDeploymentStatus(String deploymentStatus) {
		setStringAttribute(DEPLOYMENT_STATUS.name, deploymentStatus);
	}
	
	@Override
	public Boolean getRetry() {
		return getBooleanAttribute(RETRY.name);
	}
	
	@Override
	public void setRetry(Boolean retry) {
		setBooleanAttribute(RETRY.name, retry);
	}

	@Override
	public String getTrackingId() {
		return getStringAttribute(TRACKING_ID.name);
	}

	@Override
	public void setTrackingId(String trackingId) {
		setStringAttribute(TRACKING_ID.name, trackingId);
		
	}

	@Override
	public ServiceRequest getServiceRequest() {
		return getServiceRequestAttribute(SERVICE_REQUEST.name);
	}

	@Override
	public void setServiceRequest(ServiceRequest serviceRequest) {
		setServiceRequestAttribute(SERVICE_REQUEST.name,serviceRequest);
	}


}

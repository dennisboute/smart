package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.TimePeriod;
import com.energyict.cpo.ShadowList;
import com.energyict.mdw.shadow.MeterPortfolioItemShadow;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;

import java.util.*;

public class PortfolioItemShadowsManagerImpl implements PortfolioItemShadowsManager {
    public Map<Integer, SortedSet<MeterPortfolioItemShadow>> groupAndSortPortfolioItemsByChannelId(ShadowList<MeterPortfolioItemShadow> itemShadows) {
        Map<Integer, SortedSet<MeterPortfolioItemShadow>> channelId2itemShadows =
                new HashMap<>();

        for (MeterPortfolioItemShadow itemShadow : itemShadows) {
            Integer channelId = itemShadow.getChannelId();
            SortedSet<MeterPortfolioItemShadow> channelItems = channelId2itemShadows.get(channelId);
            if (channelItems == null) {
                channelItems = new TreeSet<>(new MeterPortfolioItemComparator());
                channelId2itemShadows.put(channelId, channelItems);
            }
            channelItems.add(itemShadow);
        }

        return channelId2itemShadows;
    }

    public SortedSet<MeterPortfolioItemShadow> extractAndSortPortfolioItems(int channelId, ShadowList<MeterPortfolioItemShadow> itemShadows) {
        SortedSet<MeterPortfolioItemShadow> channelItems =
                new TreeSet<>(new MeterPortfolioItemComparator());
        return CollectionUtils.select(itemShadows, buildChannelItemPredicate(channelId), channelItems);
    }

    private Predicate<? super MeterPortfolioItemShadow> buildChannelItemPredicate(final int channelId) {
        return (Predicate<MeterPortfolioItemShadow>) itemShadow -> channelId == itemShadow.getChannelId();
    }

    public List<MeterPortfolioItemShadow> extractPrecedingItemShadows(
            final Date test, final Collection<MeterPortfolioItemShadow> itemShadows) {
        List<MeterPortfolioItemShadow> result = new ArrayList<>(itemShadows.size());
        return CollectionUtils.select(itemShadows, buildPrecedingPredicate(test), result);
    }

    private Predicate<MeterPortfolioItemShadow> buildPrecedingPredicate(final Date test) {
        return itemShadow -> {
            Date toDate = itemShadow.getToDate();
            return (test != null) && (toDate != null) && !toDate.after(test);
        };
    }

    public List<MeterPortfolioItemShadow> extractOverlappingItemShadows(
            final TimePeriod period, final Collection<MeterPortfolioItemShadow> itemShadows) {
        List<MeterPortfolioItemShadow> result = new ArrayList<>(itemShadows.size());
        return CollectionUtils.select(itemShadows, buildOverlappingPredicate(period), result);
    }

    private Predicate<MeterPortfolioItemShadow> buildOverlappingPredicate(final TimePeriod period) {
        return itemShadow -> new TimePeriod(itemShadow.getFromDate(), itemShadow.getToDate()).overlaps(period);
    }
}

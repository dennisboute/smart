package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.TimePeriod;
import com.energyict.mdw.shadow.MeterPortfolioItemShadow;
import com.energyict.projects.common.TimePeriodComparator;

import java.util.Comparator;

public class MeterPortfolioItemComparator implements Comparator<MeterPortfolioItemShadow> {
    public int compare(MeterPortfolioItemShadow itemShadow1, MeterPortfolioItemShadow itemShadow2) {
        TimePeriod period1 = new TimePeriod(itemShadow1.getFromDate(), itemShadow1.getToDate());
        TimePeriod period2 = new TimePeriod(itemShadow2.getFromDate(), itemShadow2.getToDate());
        return TimePeriodComparator.INSTANCE.compare(period1, period2);
    }
}

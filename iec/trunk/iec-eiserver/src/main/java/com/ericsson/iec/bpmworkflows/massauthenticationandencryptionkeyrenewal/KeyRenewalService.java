package com.ericsson.iec.bpmworkflows.massauthenticationandencryptionkeyrenewal;

import com.energyict.bpm.annotations.ServiceMethod;
import com.energyict.bpm.core.CaseAttributes;
import com.energyict.bpm.core.WorkItem;
import com.energyict.cpo.PropertySpec;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapper;
import com.energyict.hsm.worldline.workflows.keyrenewal.KeyRenewalProcessCaseWrapperFactory;
import com.energyict.massupdateactions.model.MassUpdateProvider;
import com.energyict.massupdateactions.transitionhandlers.individual.CompleteMassUpdateActionItem;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractWorkflowServiceWithExceptionHandling;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class KeyRenewalService extends AbstractWorkflowServiceWithExceptionHandling<KeyRenewalProcessCaseWrapper> {

    public KeyRenewalService() {
    }

    public String getVersion() {
        return "$Date: 2017-05-03 09:01:42 +0200 (Wed, 03 May 2017) $";
    }

    public List<PropertySpec> getOptionalProperties() {
        return new ArrayList();
    }

    public List<PropertySpec> getRequiredProperties() {
        return new ArrayList();
    }

    public ProcessCaseWrapperFactory<KeyRenewalProcessCaseWrapper> getProcessCaseWrapperFactory() {
        return (ProcessCaseWrapperFactory) KeyRenewalProcessCaseWrapperFactory.INSTANCE.get();
    }

    @ServiceMethod(
            name = "Complete mass update action item"
    )
    public CaseAttributes completeMassUpdateActionItem(WorkItem workItem, Logger logger) {
        return this.process(workItem, new CompleteMassUpdateActionItem(logger, (MassUpdateProvider) MassKeyRenewalUpdateProviderImpl.INSTANCE.get()));
    }
}

package com.ericsson.iec.model;

import com.energyict.mdw.core.VirtualMeter;
import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface PointOfDelivery extends FolderVersionWrapper {

	VirtualMeter getVirtualMeter();
	void setVirtualMeter(VirtualMeter virtualMeter);
}

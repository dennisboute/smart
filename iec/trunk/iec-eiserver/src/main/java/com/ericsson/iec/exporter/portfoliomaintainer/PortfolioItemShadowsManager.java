package com.ericsson.iec.exporter.portfoliomaintainer;

import com.energyict.cbo.TimePeriod;
import com.energyict.cpo.ShadowList;
import com.energyict.mdw.shadow.MeterPortfolioItemShadow;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface PortfolioItemShadowsManager {

    AtomicReference<PortfolioItemShadowsManager> INSTANCE = new AtomicReference<>(new PortfolioItemShadowsManagerImpl());

    Map<Integer, SortedSet<MeterPortfolioItemShadow>> groupAndSortPortfolioItemsByChannelId(
            final ShadowList<MeterPortfolioItemShadow> itemShadows);

    SortedSet<MeterPortfolioItemShadow> extractAndSortPortfolioItems(
            int virtualMeterId, ShadowList<MeterPortfolioItemShadow> itemShadows);

    List<MeterPortfolioItemShadow> extractPrecedingItemShadows(
            final Date test, final Collection<MeterPortfolioItemShadow> itemShadows);

    List<MeterPortfolioItemShadow> extractOverlappingItemShadows(
            final TimePeriod period, final Collection<MeterPortfolioItemShadow> itemShadows);

}

package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.common.model.folder.FolderVersionWrapperFactoryImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;

public class DistrictFactoryImpl extends FolderVersionWrapperFactoryImpl<District> implements DistrictFactory {

	public DistrictFactoryImpl() {
		super();
	}
	
	public DistrictFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	@Override
	public District createNew(FolderVersionAdapter arg0) {
		return new DistrictImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return FolderTypes.DISTRICT.name;
	}
	
	@Override
	public District findByKey(String key) {
		return findByExternalName(FolderTypes.DISTRICT.buildExternalName(key), new Date());
	}
}

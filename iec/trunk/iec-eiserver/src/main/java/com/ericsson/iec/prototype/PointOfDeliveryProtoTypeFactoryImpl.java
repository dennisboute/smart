package com.ericsson.iec.prototype;

import static com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypes.POINT_OF_DELIVERY;

import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.core.exception.FatalConfigurationException;

public class PointOfDeliveryProtoTypeFactoryImpl extends AbstractProtoTypeFactoryImpl implements PointOfDeliveryProtoTypeFactory {

	public PointOfDeliveryProtoTypeFactoryImpl() {
		super();
	}
	
	@Override
	public PointOfDeliveryProtoType find() throws FatalConfigurationException {
		return new PointOfDeliveryProtoTypeImpl(getCopySpec());
	}

	@Override
	public String getCopySpecName() {
		return POINT_OF_DELIVERY.getName();
	}

	@Override
	public ProtoTypeObjectType getObjectType() {
		return ProtoTypeObjectType.POD;
	}
}

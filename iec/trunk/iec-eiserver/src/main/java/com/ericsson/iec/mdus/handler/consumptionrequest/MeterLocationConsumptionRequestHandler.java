package com.ericsson.iec.mdus.handler.consumptionrequest;

import com.energyict.cbo.BusinessException;
import com.energyict.mdc.tasks.overview.CommunicationTask;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.mdw.core.Device;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.*;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.MeterLocation;
import com.ericsson.iec.model.Premise;
import com.ericsson.iec.model.Warehouse;
import sapmdm01.interfaces.mdm.iec.MeterLocationRequest;
import sapmdm01.interfaces.mdm.iec.MeterLocationRequest.MessageContent;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.*;

public class MeterLocationConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<MeterLocationRequest> {

    @Override
    protected MeterLocationRequest unmarshal(String request) throws MarshallingException {
        return (MeterLocationRequest) IecMarshallerHelper.getInstance().unmarshall(request, MeterLocationRequest.class);
    }

    @Override
    protected void doProcess(MeterLocationRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
        MessageContent requestContent = message.getMessageContent();
        ProcessContext processContext = new ProcessContext(requestContent);
        RequestValidator.validateMessageContent(requestContent, processContext);
        RequestProcessor.processMessageContent(requestContent, processContext, logger);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class ProcessContext {
        Date locationEndDate;
        String sapRegionCode;
        String sapRegionId;
        String cityCode;
        String cityName;
        String transformerId;

        BigDecimal meterCoordinateX;
        BigDecimal meterCoordinateY;
        String meterUsage;
        String utilitiesAdvancedMeteringSystemID;
        String deviceId;
        String premiseCode;
        String locationId;
        String logicalInstallationPointID;

        String districtId;
        String regionId;

        GenericMeter meter;
        Premise premise;
        MeterLocation meterLocation;
        Warehouse meterLocationFolder;

        public ProcessContext(MessageContent requestContent) {
            initialize(requestContent);
        }

        private void initialize(MessageContent requestContent) {
            if (requestContent == null)
                return;

            XMLGregorianCalendar xmlEndDate = requestContent.getLocationEndDate();
            locationEndDate = (xmlEndDate != null) ? xmlEndDate.toGregorianCalendar().getTime() : null;
            sapRegionCode = requestContent.getRegionCode();
            sapRegionId = requestContent.getRegionId();
            cityCode = requestContent.getCityId();
            cityName = requestContent.getCityName();
            transformerId = requestContent.getTransformerID();
            meterCoordinateX = requestContent.getMETERKORDINATEX();
            meterCoordinateY = requestContent.getMETERKORDINATEY();
            meterUsage = requestContent.getMeterUsage();
            deviceId = requestContent.getDeviceId();
            premiseCode = requestContent.getPREMISECODE();
            locationId = requestContent.getInstallationPointId();
            logicalInstallationPointID = requestContent.getLogicalInstallationPointID();

            meter = CommonObjectFactory.getMeter(deviceId);
            premise = CommonObjectFactory.getPremise(premiseCode);
            meterLocation = CommonObjectFactory.getMeterLocation(locationId);
            utilitiesAdvancedMeteringSystemID = requestContent.getUtilitiesAdvancedMeteringSystemID();
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestValidator {
        public static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException, AbstractConfigurationException {
            if (requestContent == null)
                throw new FatalValidationException(MESSAGE_IS_EMPTY);

            // Validate Fields:
            CommonValidatorFactory.validateValue(pc.locationEndDate, "31/12/9999", "LocationEndDate");
            validateSapRegionCode(pc.sapRegionCode);
            pc.districtId = pc.sapRegionCode.substring(1);
            CommonValidatorFactory.validateLookup(pc.districtId, "districtId (input field RegionCode)", "district", CommonObjectFactory.getMeterLocationFolderType());
            validateSapRegionId(pc.sapRegionId);
            pc.regionId = pc.sapRegionId.substring(1);
            CommonValidatorFactory.validateLookup(pc.regionId, "RegionId", "region", CommonObjectFactory.getMeterLocationFolderType());
            CommonValidatorFactory.validateMandatoryValue(pc.cityCode, "CityCode");
            CommonValidatorFactory.validateMandatoryValue(pc.cityName, "CityName");
            CommonValidatorFactory.validateCoordinate(pc.meterCoordinateX, "METER_KORDINATE_X");
            CommonValidatorFactory.validateCoordinate(pc.meterCoordinateY, "METER_KORDINATE_Y");
            validateMeterUsage(pc.meterUsage);
            CommonValidatorFactory.validateMandatoryValue(pc.logicalInstallationPointID, "LogicalInstallationPointID");
            CommonValidatorFactory.validateValue(pc.utilitiesAdvancedMeteringSystemID, "MDMA", "UtilitiesAdvancedMeteringSystemID");

            validateMeter(pc);
            validatePremise(pc);

            pc.meterLocationFolder = IecWarehouse.getInstance().getWarehouseFactory().find(Warehouses.METER_LOCATION_WAREHOUSE); // contains validation for null
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        private static void validateSapRegionCode(String sapRegionCode) throws FatalValidationException {
            CommonValidatorFactory.validateMandatoryValue(sapRegionCode, "RegionCode");
            if (sapRegionCode == null || !sapRegionCode.startsWith("Z"))
                throw new FatalValidationException(FIELD_DOES_NOT_MATCH_PATTERN, "RegionCode", sapRegionCode, "Starts with Z");
        }

        private static void validateSapRegionId(String sapRegionId) throws FatalValidationException {
            CommonValidatorFactory.validateMandatoryValue(sapRegionId, "RegionId");
            if (sapRegionId == null || !sapRegionId.startsWith("Z"))
                throw new FatalValidationException(FIELD_DOES_NOT_MATCH_PATTERN, "RegionId", sapRegionId, "Starts with Z");
        }

        private static void validateMeterUsage(String meterUsage) throws FatalValidationException {
            String[] validValues = new String[]{"1", "3", "4"};
            CommonValidatorFactory.validateValueList(meterUsage, validValues, "MeterUsage");
        }

        private static void validateMeter(ProcessContext pc) throws NonFatalValidationException {
            if (pc.meter == null)
                throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Meter", pc.deviceId);
        }

        private static void validatePremise(ProcessContext pc) throws AbstractValidationException {
            if (pc.premise == null)
                throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Premise", pc.premiseCode);
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestProcessor {
        public static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
            try {
                processMeterLocation(requestContent, pc);

                pc.meter.setParent(pc.premise.getFolder());
                pc.meter.setPremise(pc.premise);
                pc.meter.setMeterLocation(pc.meterLocation);
                pc.meter.setStatus(new BigDecimal(3)); // Installed – Unsealed
            } catch (Exception e) {
                logger.severe("Failed processing the message (General Error): " + e.getMessage());
                throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", pc.deviceId);
            }

            updatePremiseType(pc, logger); // Only if 1'st meter under this premise

            checkIfAmplifier(pc);

            updateMeter(pc.meter);
            logger.info("Meter Saved");
            logger.info("Message Processing Completed");
        }

        private static void checkIfAmplifier(ProcessContext pc) throws FatalExecutionException {
            try {
                if (pc.meterUsage.equals(4)) {
                    Device device = pc.meter.getDevice().getDevice();
                    for (CommunicationTask communicationTask : device.getCommunicationOverview().getCommunicationTasks()) {
                        communicationTask.getComTaskExecution().delete();
                    }
                }
            } catch (BusinessException | SQLException e) {
                throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", pc.deviceId);
            }
        }
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static void processMeterLocation(MessageContent requestContent, ProcessContext pc) throws IecException {
        boolean isNewLocation = (pc.meterLocation == null);
        try {
            if (pc.meterLocation == null)
                pc.meterLocation = IecWarehouse.getInstance().getMeterLocationFactory().createNew();

            pc.meterLocation.setFrom(requestContent.getLocationStartDate().toGregorianCalendar().getTime());
            pc.meterLocation.setCountry("IL");
            pc.meterLocation.setDistrict(pc.districtId);
            pc.meterLocation.setRegion(pc.regionId);
            pc.meterLocation.setPostalCode(requestContent.getStreetPostalCode());
            pc.meterLocation.setCityCode(pc.cityCode);
            pc.meterLocation.setCityName(pc.cityName);
            pc.meterLocation.setStreet(requestContent.getStreetName());
            pc.meterLocation.setStreetNumber(requestContent.getHouseId());
            pc.meterLocation.setLogicalInstallationPointID(pc.logicalInstallationPointID);

            pc.meterLocation.setName(pc.locationId);
            pc.meterLocation.setExternalName(FolderTypes.METER_LOCATION.buildExternalName(pc.locationId));
            pc.meterLocation.setActiveDate(new Date(0));
            pc.meterLocation.setFrom(new Date(0));
            pc.meterLocation.setParent(pc.meterLocationFolder.getFolder());
        } catch (Exception e) {
            throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter Location", pc.locationId);
        }

        try {
            pc.meterLocation.saveChanges();
        } catch (BusinessException | SQLException e) {
            if (isNewLocation)
                throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Meter Location", pc.meterLocation.getExternalName());
            else
                throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Meter Location", pc.meterLocation.getExternalName());
        }
    }

    private static void updateMeter(GenericMeter meter) throws IecException {
        try {
            meter.saveChanges();
        } catch (BusinessException | SQLException e) {
            throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Meter", meter.getExternalName());
        }
    }

    private static void updatePremiseType(ProcessContext pc, Logger logger) throws IecException {
        // Required only for 1'st meter:
        int meterCount = CommonObjectFactory.getMeterCountByFolder(pc.premise.getFolder());
        if (meterCount > 0)
            return;

        // Check if current premiseType is OK:
        String plcMeterFolderTypeName = CommonObjectFactory.getPlcMeterFolderType().getDisplayName();
        String requiredPremiseType = (pc.meter.getFolderType().getDisplayName().equals(plcMeterFolderTypeName)) ? "P" : "C";
        if (pc.premise.getPremiseType() == requiredPremiseType)
            return;

        // Change premiseType & parent:
        pc.premise.setPremiseType(requiredPremiseType);
        pc.premise.setParent(CommonObjectFactory.calcPremiseParentFolder(pc.premise));

        try {
            pc.premise.saveChanges();
        } catch (BusinessException | SQLException e) {
            logger.severe("Failed updating premise " + pc.premise.getExternalName() + " type & parent");
            throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Premise", pc.premise.getExternalName());
        }
        logger.info("Premise " + pc.premise.getExternalName() + " premiseType updated to: '" + pc.premise.getPremiseType() + "'");
    }
}

package com.ericsson.iec.bpmworkflows.keyrenewal;

import com.energyict.projects.common.model.processcase.ProcessCaseAdapter;
import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactoryImpl;
import com.ericsson.iec.constants.ProcessConstants;

public class AuthenticationAndEncryptionKeyRenewalFactoryImpl extends ProcessCaseWrapperFactoryImpl<AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper> implements AuthenticationAndEncryptionKeyRenewalFactory {

    @Override
    public String getProcessName() {
        return ProcessConstants.Processes.AUTHENTICATION_AND_ENCRYPTION_KEY_RENEWAL.name;
    }

    @Override
    public AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper createNew(ProcessCaseAdapter adapter) {
        return new AuthenticationAndEncryptionKeyRenewalProcessCaseWrapperImpl(adapter);
    }
}

package com.ericsson.iec.model;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.CELLULAR_METER;

import com.energyict.mdw.core.Folder;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionAdapterFactory;
import com.energyict.projects.coreextensions.FolderTypeSearchFilter;
import com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes;
import com.ericsson.iec.core.IecWarehouse;

public class CellularMeterFactoryImpl extends GenericMeterFactoryImpl implements CellularMeterFactory {

	public CellularMeterFactoryImpl(FolderVersionAdapterFactory factory) {
		super(factory);
	}

	public CellularMeterFactoryImpl() {
		super();
	}

	@Override
	public CellularMeter createNew(FolderVersionAdapter arg0) {
		return new CellularMeterImpl(arg0);
	}

	@Override
	protected String getFolderTypeName() {
		return CELLULAR_METER.name;
	}

	@Override
	public String buildExternalName(String key) {
		return CELLULAR_METER.buildExternalName(key);
	}

	@Override
	public GenericMeter findBySerialId(String serialId) {
		FolderTypeSearchFilter filter = new FolderTypeSearchFilter(getFolderType(), IecWarehouse.getInstance().getMeteringWarehouse().getRootFolder());
        filter.addCriterium(MeterAttributes.SERIAL_ID.name, serialId);

        for (Folder folder : filter.findMatchingFolders()) {
            return super.createNewForLastVersion(folder);
        }
        return null;
    }

	public GenericMeter findByGenericMeterDevice(GenericMeterDevice genericMeterDevice){
		FolderTypeSearchFilter filter = new FolderTypeSearchFilter(getFolderType(), IecWarehouse.getInstance().getMeteringWarehouse().getRootFolder());
		filter.addCriterium(MeterAttributes.DEVICE.name, genericMeterDevice.getDevice());

		for (Folder folder : filter.findMatchingFolders()) {
			return createNewForLastVersion(folder);
		}
		return null;
	}
}

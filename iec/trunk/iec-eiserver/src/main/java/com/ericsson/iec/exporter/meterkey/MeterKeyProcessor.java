package com.ericsson.iec.exporter.meterkey;

import com.energyict.mdc.protocol.security.SecurityPropertySet;
import com.energyict.mdc.shadow.protocol.security.SecurityProperties;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.core.Group;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.GenericMeterDevice;

import java.util.*;

class MeterKeyProcessor {

    public List<MeterKeyItem> process(Group meterKeysGroup) {
        try {
            return createMeterKeyItems(meterKeysGroup);
        } catch (Exception ex) {
            return null;
        }
    }

    private HashMap<String, List<Device>> groupDevicesByConcentrator(Group meterKeysGroup) {
        HashMap<String, List<Device>> devicesByConcentrator = new HashMap<>();
        for (Object object : meterKeysGroup.getMembers()) {
            Device device = (Device) object;
            String concentratorSerialId = device.getGateway() == null ? "CIRCUTOR" : IecWarehouse.getInstance().getDataConcentratorFactory().createNewForLastVersion(device.getGateway().getFolder()).getSerialId();
            if (!devicesByConcentrator.containsKey(concentratorSerialId)) {
                List<Device> deviceList = new ArrayList<>();
                deviceList.add(device);
                devicesByConcentrator.put(concentratorSerialId, deviceList);
            } else {
                devicesByConcentrator.get(concentratorSerialId).add(device);
            }
        }
        return devicesByConcentrator;
    }

    private List<MeterKeyItem> createMeterKeyItems(Group meterKeysGroup) {
        HashMap<String, List<Device>> devicesByConcentrator = groupDevicesByConcentrator(meterKeysGroup);
        List<MeterKeyItem> meterKeyItems = new ArrayList<>();
        for (Map.Entry<String, List<Device>> entry : devicesByConcentrator.entrySet()) {
            String concentratorSerialId = entry.getKey();
            List<Device> deviceList = entry.getValue();
            Collections.sort(deviceList, Comparator.comparing(Device::getSerialNumber));
            MeterKeyItem meterKeyItem = buildMeterKeyItem(concentratorSerialId, deviceList);
            meterKeyItems.add(meterKeyItem);
        }
        return meterKeyItems;
    }

    private MeterKeyItem buildMeterKeyItem(String concentratorSerialId, List<Device> devices) {
        return new MeterKeyItem(concentratorSerialId, buildModels(devices));
    }

    private List<MeterKeyModel> buildModels(List<Device> devices) {
        List<MeterKeyModel> models = new ArrayList<>();
        for (Device meter : devices) {
            models.add(buildModel(meter));
        }
        return models;
    }

    private MeterKeyModel buildModel(Device device) {
        MeterKeyModel model = new MeterKeyModel();
        model.setGenericMeter(getGenericMeter(device));
        for (SecurityProperties securityProperties : device.getShadow().getSecurityProperties()) {
            if (securityProperties.getProperties().size() > 0) {
                for (SecurityPropertySet securityPropertySet : device.getConfiguration().getCommunicationConfiguration().getSecurityPropertySets()) {
                    if (securityProperties.getSecurityPropertySetId() == securityPropertySet.getId()) {
                        model.setAuthenticationKey((String) securityProperties.get("AuthenticationKey"));
                        model.setEncryptionKey((String) securityProperties.get("EncryptionKey"));
                    }
                }
            }
        }

        return model;
    }

    public GenericMeter getGenericMeter(Device device) {
        GenericMeterDevice genericMeterDevice = IecWarehouse.getInstance().getGenericMeterDeviceFactory().findByDevice(device);
        GenericMeter genericMeter = IecWarehouse.getInstance().getMeterFactory().findByGenericMeterDevice(genericMeterDevice);
        return genericMeter;
    }
}

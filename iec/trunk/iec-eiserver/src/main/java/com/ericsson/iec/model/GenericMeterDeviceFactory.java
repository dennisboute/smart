package com.ericsson.iec.model;

import com.energyict.mdw.core.Device;
import com.energyict.projects.common.model.device.DeviceWrapperFactory;

public interface GenericMeterDeviceFactory extends DeviceWrapperFactory<GenericMeterDevice> {
    GenericMeterDevice findByDevice(Device device);
}

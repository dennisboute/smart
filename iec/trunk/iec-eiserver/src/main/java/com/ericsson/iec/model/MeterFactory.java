package com.ericsson.iec.model;

import java.util.Date;

import com.energyict.mdw.core.Folder;
import com.energyict.projects.common.model.folder.ConfigurableFolderVersionWrapperFactory;
import com.energyict.projects.coreextensions.FolderTypeSearchFilter;
import com.ericsson.iec.constants.AttributeTypeConstants;
import com.ericsson.iec.core.IecWarehouse;

public class MeterFactory extends ConfigurableFolderVersionWrapperFactory<GenericMeter, GenericMeterFactory> {

	public MeterFactory() {
		map(new CellularMeterFactoryImpl());
		map(new PlcMeterFactoryImpl());
	}
	
	public String buildExternalName(String key, GenericMeter meter) {
		String meterFolderType = meter.getFolderType().getName();
		for (GenericMeterFactory f : super.getFactories()) {
			if (f.getFolderType().getName().equals(meterFolderType))
				return f.buildExternalName(key);
		}
		return null;
	}
	
	public GenericMeter findByKey(String key) {
		for (GenericMeterFactory f : super.getFactories()) {
			GenericMeter meter = super.findByExternalName(f.buildExternalName(key), new Date());
			if (meter != null)
				return meter;
		}
		return null;
	}

	public GenericMeter findBySerialId(String serialId) {
		for (GenericMeterFactory f : super.getFactories()) {
			GenericMeter meter = f.findBySerialId(serialId);
			if (meter != null)
				return meter;
		}
		return null;
    }

	public GenericMeter findByGenericMeterDevice(GenericMeterDevice genericMeterDevice){
		for (GenericMeterFactory f : super.getFactories()) {
			GenericMeter meter = f.findByGenericMeterDevice(genericMeterDevice);
			if (meter != null)
				return meter;
		}
		return null;
	}
}

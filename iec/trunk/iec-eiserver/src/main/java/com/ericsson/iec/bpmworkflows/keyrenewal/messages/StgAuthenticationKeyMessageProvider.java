package com.ericsson.iec.bpmworkflows.keyrenewal.messages;

import com.energyict.cbo.HexString;
import com.energyict.cpo.TypedProperties;
import com.energyict.hsm.worldline.model.RenewKey;
import com.energyict.hsm.worldline.model.messages.MessageWithProperties;
import com.energyict.hsm.worldline.model.messages.impl.MessageWithPropertiesImpl;
import com.energyict.hsm.worldline.model.messages.outboundrenewal.OutboundRenewalMessageProvider;
import com.energyict.projects.dateandtime.Clock;
import com.energyict.protocolimplv2.messages.DeviceMessageConstants;
import com.energyict.protocolimplv2.messages.STGReportsMessages;
import com.energyict.protocolimplv2.messages.enums.STGOrderRequestPriority;

public class StgAuthenticationKeyMessageProvider implements OutboundRenewalMessageProvider {
    @Override
    public MessageWithProperties getRenewalMessage(RenewKey<?> renewKey) {
        TypedProperties properties = TypedProperties.empty();
        properties.setProperty(DeviceMessageConstants.ORDER_REQUEST_PRIORITY, STGOrderRequestPriority.VERY_HIGH.getName());
        properties.setProperty(DeviceMessageConstants.AUTHORIZATION_KEY, new HexString(renewKey.getDatabaseKey().getDbValue()));
        properties.setProperty(DeviceMessageConstants.MAXIMUM_EXECUTION_DATE, Clock.INSTANCE.get().now());
        properties.setProperty(DeviceMessageConstants.EXECUTION_DATE, Clock.INSTANCE.get().now());
        return new MessageWithPropertiesImpl(STGReportsMessages.B32_AK, properties);
    }
}

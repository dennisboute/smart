package com.ericsson.iec.mdus.responder;

import java.sql.SQLException;

import javax.xml.ws.Service;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.SapEndpoint;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.exception.MdusBusinessException;
import com.energyict.mdus.core.exception.MdusResourceBusyException;
import com.energyict.mdus.core.exception.MdusSqlException;
import com.energyict.mdus.core.exception.NoServiceRequestForIdException;
import com.energyict.mdus.core.exception.UnexpectedObjectCount;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.model.relation.SapMdusRequest;
import com.energyict.mdus.core.responder.AbstractSapResponder;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.mdw.service.ServiceRequestState;
import com.ericsson.iec.mdus.IecMarshallerHelper;

public abstract class IecMdusSapResponder<S extends Service, T> extends AbstractSapResponder<S, MarshallingException, T> {

	public IecMdusSapResponder(SapEndpoint endpoint, Class<S> serviceClass, Class<T> portClass) throws MarshallingException {
		super(endpoint, serviceClass, portClass, IecMarshallerHelper.getInstance());
	}

	protected void markServiceRequestSuccess(ResponseMessage message) throws NoServiceRequestForIdException, MdusBusinessException, MdusSqlException {
		markServiceRequest(message, ServiceRequestState.SUCCESS);
	}
	
	protected void markServiceRequestFailed(ResponseMessage message) throws NoServiceRequestForIdException, UnexpectedObjectCount, MdusBusinessException, MdusSqlException, MdusResourceBusyException {
		markServiceRequest(message, ServiceRequestState.FAILED);
	}
	
	protected void logError(ResponseMessage message, String errorMessage) throws BusinessException, NoServiceRequestForIdException, MdusBusinessException, MdusSqlException {
		if ( message instanceof SapResponseMessage )
		{
			ServiceRequest serviceRequest = ((SapResponseMessage)message).getServiceRequest();
			if ( serviceRequest != null ){
				SapMdusRequest request = MdusWarehouse.getCurrent().getSapMdusRequestFactory().findByServiceRequest(serviceRequest);
				request.setErrorMessage(errorMessage);
				try {
					request.saveChanges();
				}
				catch (BusinessException e) {
					throw new MdusBusinessException(e);
				}
				catch (SQLException e) {
					throw new MdusSqlException(e);
				}
			}
		}
	}

	private void markServiceRequest(ResponseMessage message, ServiceRequestState state) throws NoServiceRequestForIdException, MdusBusinessException, MdusSqlException {
		if ( message instanceof SapResponseMessage )
		{
			ServiceRequest serviceRequest = ((SapResponseMessage)message).getServiceRequest();
			if ( serviceRequest != null ){
				try {
					serviceRequest.setState(state);
				}
				catch (BusinessException e) {
					throw new MdusBusinessException(e);
				}
				catch (SQLException e) {
					throw new MdusSqlException(e);
				}
				
			}
		}		
	}
	
}

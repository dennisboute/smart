package com.ericsson.iec.mdus;

import java.util.ArrayList;
import java.util.List;

import com.energyict.mdus.core.SapMessagePrioritySystemParameterSpec;
import com.energyict.mdus.core.SapMessageValidityPeriodSystemParameterSpec;
import com.energyict.mdus.core.SapServiceRequestTypeSystemParameterSpecImpl;
import com.energyict.mdus.core.SapSystemParameterSpec;
import com.energyict.mdus.core.SapWebService;
import com.energyict.mdus.core.SapWebServiceCache;
import com.energyict.mdus.core.SapWebServiceGroup;
import com.energyict.mdus.core.SapWebServiceProperties;
import com.energyict.mdus.core.exception.SystemObjectNotDefined;
import com.energyict.mdus.core.exception.SystemParameterIncorrectValue;
import com.energyict.mdus.core.temporarycache.systemparameter.SystemParameterCache;
import com.energyict.mdus.core.temporarycache.systemparameter.transformer.IntegerTransformer;
import com.energyict.mdus.core.temporarycache.systemparameter.transformer.PositiveIntegerTransformer;
import com.energyict.mdus.core.temporarycache.systemparameter.transformer.ServiceRequestTypeTransformer;
import com.energyict.mdw.service.ServiceRequestType;

public enum MdusWebservice implements SapWebService {
	
	TRANSFORMER_STATION_REQUEST("Transformer Station Request", MdusWebserviceGroup.NIS),
	TRANSFORMER_REQUEST("Transformer Request", MdusWebserviceGroup.NIS),
	PREMISE_REQUEST("Premise Request", MdusWebserviceGroup.NIS),
	DC_ASSET_REQUEST("Dc Asset Request", MdusWebserviceGroup.NOC),
	DC_EVENT_REPORT_REQUEST("Dc Event Report Request", MdusWebserviceGroup.NOC),
	FIELD_ACTIVITY_NOTIFICATION_REQUEST("Field Activity Notification Request", MdusWebserviceGroup.WFM),
	METER_SAP_LINK_REQUEST("Meter Sap Link Request", MdusWebserviceGroup.SAP),
	METER_LOCATION_REQUEST("Meter Location Request", MdusWebserviceGroup.SAP),
	METER_POD_REQUEST("Meter Pod Request", MdusWebserviceGroup.SAP),
	NEW_EQUIPMENT_FOR_RADIUS("New Equipment For Radius Request", MdusWebserviceGroup.RADIUS),
	REGISTERED_NOTIFICATION_REQUEST("Registered Notification Request", MdusWebserviceGroup.NOC),
	DC_FAULT_NOTIFICATION("DC Fault Notification", MdusWebserviceGroup.NOC),
	HANDLE_NOTIFICATION_METER_DATA_PULL_REQUEST("Handle Notification Meter Data Pull Request", MdusWebserviceGroup.NOC),
	HANDLE_NOTIFICATION_METER_DATA_PUSH_REQUEST("Handle Notification Meter Data Push Request", MdusWebserviceGroup.NOC),
	CELLULAR_COMMUNICATION_STATISTICS_REQUEST("Cellular Communication Statistics Request", MdusWebserviceGroup.NOC),
	;
		
	private SapWebServiceProperties sapWebServiceProperties;
	
	private static final String IEC_PREFIX = "iec.mdus";
	
	private MdusWebservice(String name, SapWebServiceGroup group) {
		this.sapWebServiceProperties = new SapWebServiceProperties(name, group);
	}
	
	@Override
	public int getMessagePriority() throws SystemObjectNotDefined, SystemParameterIncorrectValue {
		String name = getSapMessagePrioritySystemParameterSpec().getKey();
		IntegerTransformer transformer = new PositiveIntegerTransformer();
		return SystemParameterCache.getInstance().getObjectFromSystemParameter(name, transformer);
	}
	
	@Override
	public ServiceRequestType getServiceRequestType() throws SystemObjectNotDefined, SystemParameterIncorrectValue {
		String name = getSapServiceRequestTypeSystemParameterSpec().getKey();
		ServiceRequestTypeTransformer transformer = new ServiceRequestTypeTransformer();
		return SystemParameterCache.getInstance().getObjectFromSystemParameter(name, transformer);
	}

	@Override
	public SapSystemParameterSpec getSapMessagePrioritySystemParameterSpec() {
		return new SapMessagePrioritySystemParameterSpec(this, IEC_PREFIX);
	}

	@Override
	public SapSystemParameterSpec getSapMessageValidityPeriodSystemParameterSpec() {
		return new SapMessageValidityPeriodSystemParameterSpec(this, IEC_PREFIX);
	}

	@Override
	public SapSystemParameterSpec getSapServiceRequestTypeSystemParameterSpec() {
		return new SapServiceRequestTypeSystemParameterSpecImpl(this, IEC_PREFIX);
	}

	@Override
	public SapWebServiceProperties getSapWebServiceProperties() {
		return sapWebServiceProperties;
	}
	
	public static List<SapWebService> getAllValues() {
		List<SapWebService> sapWebServices = new ArrayList<SapWebService>();
		for(SapWebService sapWebservice : values()){
			sapWebServices.add(sapWebservice);
		}

		return sapWebServices;
	}

	static{
		try {
			SapWebServiceCache.getInstance().addAll(getAllValues());
		} catch (SystemObjectNotDefined | SystemParameterIncorrectValue e) {
			e.printStackTrace(); //TODO
		}
	}

}

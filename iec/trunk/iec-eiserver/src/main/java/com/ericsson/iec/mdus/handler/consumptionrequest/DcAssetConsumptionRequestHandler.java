package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_CREATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_PATTERN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_MUST_BE_FILLED_IN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_NOT_FOUND_IN_LOOKUP_TABLE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_ALREADY_EXISTS;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.DataConcentrator;
import com.ericsson.iec.model.Warehouse;
import com.ericsson.iec.prototype.DataConcentratorProtoType;

import nocmdm01.interfaces.mdm.iec.DCAssetRequest;
import nocmdm01.interfaces.mdm.iec.DCAssetRequest.MessageContent;

public class DcAssetConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<DCAssetRequest>{

	@Override
	protected DCAssetRequest unmarshal(String request) throws MarshallingException {
		return (DCAssetRequest) IecMarshallerHelper.getInstance().unmarshall(request, DCAssetRequest.class);
	}
	
	@Override
	protected void doProcess(DCAssetRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		MessageContent requestContent = message.getMessageContent();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(requestContent, processContext, logger);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class ProcessContext {
		String dcId;
		String firmwareVersion;
		
		DataConcentrator dc;
		Warehouse warehouse;
		DataConcentratorProtoType protoType;
//		FirmwareVersion fwv; // phase B

		public ProcessContext(MessageContent requestContent) throws IecException {
			initialize(requestContent);
		}
		
		private void initialize(MessageContent requestContent) throws IecException {
			if (requestContent == null)
				return;

			dcId = requestContent.getDCID();
			firmwareVersion = requestContent.getFWVersion();

			dc = CommonObjectFactory.getDataConcentrator(dcId);
			warehouse = IecWarehouse.getInstance().getWarehouseFactory().find(Warehouses.DATA_CONCENTRATOR_WAREHOUSE); // contains validation for null
			protoType = IecWarehouse.getInstance().getDataConcentratorProtoTypeFactory().find(); // contains validation for null
//			fwv = IecWarehouse.getInstance().getFirmwareVersionFactory().findByKey(firmwareVersion); // phase B
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestValidator {
		private static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);
	
			// Validate Fields:
			validateDcId(requestContent.getDCID(), requestContent.getSerialNumber());
			validateManufacturer(requestContent.getManufacturer());
			CommonValidatorFactory.validateYear(requestContent.getManufacturingYear(), "MANUFACTURING_YEAR");
			validateModelDescription(requestContent.getModelDescription());
			
			// Validate by Context:
			validateDc(pc);
			validateFirmwareVersion(pc);
		}
	
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateDcId(String dcId, String serialNumber) throws FatalValidationException {
			if (dcId == null || dcId.isEmpty())
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "DC_ID");
			if (serialNumber == null || serialNumber.isEmpty())
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "SERIAL_NUMBER");
			if (!dcId.equals("CIR" + serialNumber))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_PATTERN, "DC_ID", dcId, "CIR<serial_number>");
		}
		
		private static void validateManufacturer(String manufacturer) throws FatalValidationException {
			if (manufacturer == null || manufacturer.isEmpty())
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "Manufacturer");
			if (!manufacturer.equals("CIRCUTOR"))
				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "Manufacturer", manufacturer);
		}
		
		private static void validateModelDescription(String modelDescription) throws FatalValidationException {
			if (modelDescription == null || modelDescription.isEmpty())
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "Model_Description");
			CommonValidatorFactory.validateLookup(modelDescription, "Model Description", "modelDescription", CommonObjectFactory.getDataConcentratorFolderType());
//			if (!modelDescription.equals("Compact DC")) // Replaced by lookup validation
//				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "Model_Description", modelDescription);
		}
	
		private static void validateDc(ProcessContext pc) throws AbstractValidationException {
			if (pc.dc != null)
				throw new FatalValidationException(OBJECT_ALREADY_EXISTS, "Data Concentrator", pc.dcId);
		}
		
		private static void validateFirmwareVersion(ProcessContext pc) throws AbstractValidationException {
	//		if (pc.fwv == null) // Phase B
	//			throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "Firmware Version", pc.firmwareVersion);
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		private static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
			try {
				pc.protoType.setDataConcentratorId(requestContent.getDCID());
				pc.protoType.setSerialNumber(requestContent.getSerialNumber());
				pc.protoType.setMacAddressPLC(requestContent.getMACPLC());
				pc.protoType.setManufacturer(requestContent.getManufacturer());
				pc.protoType.setManufacturingYear(new BigDecimal(requestContent.getManufacturingYear()));
				pc.protoType.setModelDescription(requestContent.getModelDescription());
				pc.protoType.setActiveDate(new Date());
				pc.protoType.setWarehouse(pc.warehouse);
				pc.protoType.setFirmwareVersion(pc.firmwareVersion);
	//			pc.protoType.setFirmwareVersion(pc.fwv); // phase B
			} catch (Exception e) {
				logger.severe("Failed preparing the message (General Error): " + e.getMessage());
				throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Data Concentrator", pc.dcId);
			}
			
			try {
				pc.dc = pc.protoType.save();
				logger.info("DC Saved");
			} catch (Exception e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Data Concentrator", pc.dcId);
			}
			logger.info("Message Processing Completed");
		}
	}
}

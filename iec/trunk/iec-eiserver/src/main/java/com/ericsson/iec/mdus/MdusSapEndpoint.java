package com.ericsson.iec.mdus;

import java.util.ArrayList;
import java.util.List;

import com.energyict.mdus.core.SapEndpoint;
import com.energyict.mdus.core.SapEndpointCache;
import com.energyict.mdus.core.SapWebService;

public enum MdusSapEndpoint implements SapEndpoint {

	NEW_EQUIPMENT_FOR_RADIUS(MdusWebservice.NEW_EQUIPMENT_FOR_RADIUS," WSMDMRAD01ServicesServiceagent"),
	REGISTERED_NOTIFICATION_REQUEST(MdusWebservice.REGISTERED_NOTIFICATION_REQUEST , "WSMDMNOC01ServicesServiceagent"),
	DC_FAULT_NOTIFICATION(MdusWebservice.DC_FAULT_NOTIFICATION , "WSMDMNOC01ServicesServiceagent"),
	HANDLE_NOTIFICATION_METER_DATA_PULL_REQUEST(MdusWebservice.HANDLE_NOTIFICATION_METER_DATA_PULL_REQUEST , "WSMDMNOC01ServicesServiceagent"),
	HANDLE_NOTIFICATION_METER_DATA_PUSH_REQUEST(MdusWebservice.HANDLE_NOTIFICATION_METER_DATA_PUSH_REQUEST , "WSMDMNOC01ServicesServiceagent"),
	CELLULAR_COMMUNICATION_STATISTICS_REQUEST(MdusWebservice.CELLULAR_COMMUNICATION_STATISTICS_REQUEST , "WSMDMNOC01ServicesServiceagent"),
	;

	
	private SapWebService sapWebService;
	private String serviceClass;

	private MdusSapEndpoint(SapWebService sapWebService, String serviceClass) {
		this.sapWebService = sapWebService;
		this.serviceClass = serviceClass;
	}

	@Override
	public SapWebService getSapWebService() {
		return sapWebService;
	}

	@Override
	public String getServiceClassName() {
		return serviceClass;
	}

	public static List<SapEndpoint> getAllValues() {
		List<SapEndpoint> sapEndpoints = new ArrayList<SapEndpoint>();
		for(SapEndpoint sapEndpoint : values()){
			sapEndpoints.add(sapEndpoint);
		}

		return sapEndpoints;
	}

	static{
		SapEndpointCache.getInstance().addAll(getAllValues());
	}

}

package com.ericsson.iec.model.workflow;

import com.energyict.projects.common.model.processcase.ProcessCaseWrapperFactory;

public interface MeterRegistrationNocNotificationFactory extends ProcessCaseWrapperFactory<MeterRegistrationNocNotification> {

}

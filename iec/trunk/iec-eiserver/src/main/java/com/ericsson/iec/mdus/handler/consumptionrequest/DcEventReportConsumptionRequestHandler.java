package com.ericsson.iec.mdus.handler.consumptionrequest;

import com.energyict.cbo.BusinessException;
import com.energyict.cim.*;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.mdw.core.LogBook;
import com.energyict.mdw.shadow.DeviceEventShadow;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.DataConcentrator;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest.MessageContent.DCEventReport;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_FIND_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_MUST_BE_FILLED_IN;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;

public class DcEventReportConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<DCEventReport> {

    @Override
    protected DCEventReportRequest.MessageContent.DCEventReport unmarshal(String request) throws MarshallingException {
        return IecMarshallerHelper.getInstance().unmarshall(request, DCEventReport.class);
    }

    @Override
    protected void doProcess(DCEventReport message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
        DCEventReport requestContent = message;
        DcEventReportConsumptionRequestHandler.ProcessContext processContext = new DcEventReportConsumptionRequestHandler.ProcessContext(requestContent);
        DcEventReportConsumptionRequestHandler.RequestValidator.validateMessageContent(message, processContext);
        DcEventReportConsumptionRequestHandler.RequestProcessor.processMessageContent(message, processContext, logger);
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class ProcessContext {
        String dcId;
        Date timeStamp;
        String eventGroup;
        String eventCode;

        public ProcessContext(DCEventReport requestContent) {
            initialize(requestContent);
        }

        private void initialize(DCEventReport requestContent) {
            if (requestContent == null)
                return;

            timeStamp = requestContent.getTimeStamp().toGregorianCalendar().getTime();
            dcId = requestContent.getDCID();
            eventGroup = requestContent.getEventGroup();
            eventCode = requestContent.getEventCode();
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestValidator {
        private static void validateMessageContent(DCEventReport dcEventReport, DcEventReportConsumptionRequestHandler.ProcessContext pc) throws AbstractValidationException {
            if (dcEventReport == null)
                throw new FatalValidationException(MESSAGE_IS_EMPTY);
            validateDcId(dcEventReport.getDCID());
            validateEventCode(dcEventReport.getEventCode());
            validateEventGroup(dcEventReport.getEventGroup());

            CommonValidatorFactory.validateMandatoryValue(pc.timeStamp, "timeStamp");
            CommonValidatorFactory.validateValueList(pc.eventGroup, new String[]{"1", "2", "3", "4", "5"}, "eventGroup");

        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        private static void validateDcId(String dcId) throws FatalValidationException {
            if (dcId == null || dcId.isEmpty())
                throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "DC_ID");
        }

        private static void validateEventCode(String eventCode) throws FatalValidationException {
            if (eventCode == null || eventCode.isEmpty())
                throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "EventCode");
        }

        private static void validateEventGroup(String eventGroup) throws FatalValidationException {
            if (eventGroup == null || eventGroup.isEmpty())
                throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "EventGroupMapping");
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestProcessor {
        private static void processMessageContent(DCEventReport dcEventReport, DcEventReportConsumptionRequestHandler.ProcessContext pc, Logger logger) throws IecException {
            DataConcentrator dataConcentrator = IecWarehouse.getInstance().getDataConcentratorFactory().findByKey(dcEventReport.getDCID());
            if (dataConcentrator == null) {
                logger.severe("DC (External Name: ‘" + pc.dcId + "’) is not found");
                throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Data Concentrator", pc.dcId);
            }
            String logbookName = IecWarehouse.getInstance().getEventGroupMappingFactory().findByDcEventGroup(new BigDecimal(dcEventReport.getEventGroup())).getName();
            List<LogBook> logbooks = dataConcentrator.getDevice().getDevice().getLogBooks().stream().filter(logBook -> logBook.getLogBookType().getName().equals(logbookName)).collect(Collectors.toList());
            if (logbooks.size() != 1) {
                throw new FatalExecutionException(CANNOT_FIND_OBJECT, "Logbook", dcEventReport.getEventGroup());
            }
            try {
                DeviceEventShadow deviceEventShadow = new DeviceEventShadow();
                deviceEventShadow.setDeviceCode((Integer.valueOf(dcEventReport.getEventCode())));
                deviceEventShadow.setDate(pc.timeStamp);
                deviceEventShadow.setMessage(dcEventReport.getD1AdditionalInfo() + " " + dcEventReport.getD2AdditionalInfo());
                deviceEventShadow.setFlags(4);
                deviceEventShadow.setEventType(getEndDeviceEventType());
                logbooks.get(0).addEvent(deviceEventShadow);
            } catch (BusinessException | SQLException e) {
                logger.severe("Failed preparing the message (General Error): " + e.getMessage());
                throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Data Concentrator", pc.dcId);
            }
            logger.info("Message Processing Completed");
        }

        private static EndDeviceEventType getEndDeviceEventType() {
            EndDeviceEventType deviceEventType = new EndDeviceEventType(EndDeviceType.NOT_APPLICABLE, EndDeviceDomain.NOT_APPLICABLE, EndDeviceSubdomain.NOT_APPLICABLE, EndDeviceEventOrAction.NOT_APPLICABLE);
            return deviceEventType;
        }
    }
}


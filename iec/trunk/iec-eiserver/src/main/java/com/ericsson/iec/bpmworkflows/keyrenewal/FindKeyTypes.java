package com.ericsson.iec.bpmworkflows.keyrenewal;

import com.energyict.hsm.worldline.model.folderwrappers.KeyType;
import com.energyict.hsm.worldline.model.folderwrappers.KeyTypeFactory;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.energyict.projects.dateandtime.Clock;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;


public class FindKeyTypes extends AbstractServiceTransitionHandlerWithExceptionHandling<AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper>{

	public static final String TRANSITION_NAME = "Find Key Types";

	public FindKeyTypes(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(AuthenticationAndEncryptionKeyRenewalProcessCaseWrapper authenticationAndEncryptionKeyRenewal) throws IecException {
		try {
			authenticationAndEncryptionKeyRenewal.setDevice(authenticationAndEncryptionKeyRenewal.getMeter().getDevice().getDevice());
			if(authenticationAndEncryptionKeyRenewal.getDevice().getDeviceType().getDeviceProtocolPluggableClass().getJavaClassName().contains("stg")){
				List<KeyType> keyTypes = KeyTypeFactory.INSTANCE.get().findAll(Clock.INSTANCE.get().now()).stream().filter(keyType -> keyType.getName().contains("STG")).collect(Collectors.toList());
				for (KeyType keyType : keyTypes) {
					if(keyType.getName().contains("GAK")){
						authenticationAndEncryptionKeyRenewal.setAkKeyType(keyType);
					}
					else if (keyType.getName().contains("GUEK")){
						authenticationAndEncryptionKeyRenewal.setEkKeyType(keyType);
					}
				}
			}


		} catch(Exception e) {
			e.printStackTrace();
			throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
		}
	}

	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}

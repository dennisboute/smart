package com.ericsson.iec.core.exception;

import static com.energyict.projects.issues.StandardIssueType.ERROR;
import static com.energyict.projects.issues.StandardIssueType.VALIDATION_FAILED;

import com.energyict.cbo.Utils;
import com.energyict.projects.common.exceptions.ExceptionCode;
import com.energyict.projects.issues.Issue;
import com.ericsson.iec.core.IecCustomerModule;
import com.ericsson.iec.core.issues.ExecutionIssue;
import com.ericsson.iec.core.issues.ValidationIssue;

public class IecExceptionReference {
	
	public enum ConfigurationExceptionMessage implements ExceptionCode {
		WAREHOUSE_FOLDER_NOT_CONFIGURED(100, 2, "Warehouse '{0}' not found based on Externalname '{1}'"),
		PROTOTYPE_NOT_FOUND(101, 1, "ProtoType with name: '{0}' not found"),
		TOO_MANY_PROTOTYPES_FOUND(102, 1, "More than one ProtoType with name: '{0}' found"),
		NO_OUTBOUND_TCP_IP_CONNECTION_TASK_CONFIGUTED_FOR_DEVICE(103, 2, "Device with name: '{0}', external name: '{1}' has no outbound tcp ip connection"),
		ENDPOINT_NOT_CONFIGUTED(104, 1, "Endpoint '{0}' not configuted"),
		OBJECT_DOES_NOT_HAVE_VIRTUAL_METER(105, 2, "Object '{0}', external name '{1}' does not have a virtual meter"),
		OBJECT_CONFIGURATION_ERROR(106, 3, "Object '{0}', name '{1}' configuration error: '{2}'"),
		;
		
		private IecExceptionReference reference;
		
		ConfigurationExceptionMessage(long id, int expectedNumberOfMessageParameters, String defaultMessage) {
			reference = new IecExceptionReference(id, expectedNumberOfMessageParameters, defaultMessage);
		}
	
		@Override
		public String getDefaultMessage() {
			return reference.getDefaultMessage();
		}
	
		@Override
		public int getExpectedNumberOfMessageParameters() {
			return reference.getExpectedNumberOfMessageParameters();
		}
		
		public long getId() {
			return reference.getId();
		}
	
		@Override
		public String toMessageResourceKey() {
			return reference.toMessageResourceKey();
		}
		
		public String getMessage(Object... parameters) {
			return reference.getMessage(parameters);
		}
	
		public Issue getIssue(Object... parameters) {
			return new ExecutionIssue(getMessage(parameters), ERROR, reference.getModuleName(), getId());
		}
		
		public Issue getIssue(Throwable source, Object... parameters) {
			return new ExecutionIssue(getMessage(parameters), source, ERROR, reference.getModuleName(), getId());
		}
	
	}
	
	public enum ValidationExceptionMessage implements ExceptionCode {
		FIELD_MUST_BE_FILLED_IN(1001, 1, "Field '{0}' must be filled in."),
		FIELD_DOES_NOT_MATCH_PATTERN(1002, 3, "Field '{0}': value '{1}' does not match the pattern '{2}'."),
		FIELD_LENGTH_NOT_CORRECT(1003, 4, "Field '{0}': value '{1}' does not match the length of greater than '{2}' and less than '{3}' character(s)."),
		FIELD_LENGTH_NOT_EXACT(1004, 3, "Field '{0}': value '{1}' does not match the length it must be exact {2}."),
		FIELD_CONTAINS_NON_NUMERIC_VALUE(1005, 2, "Field '{0}': value '{1}' is not a numeric value."),
		FIELD_CONTAINS_NON_POSITIVE_NUMERIC_VALUE(1006, 2, "Field '{0}': value '{1}' is not a positive numeric value."),
		FIELD_DOES_NOT_MATCH_MDUS_END_DATE(1007, 2, "Field '{0}': value '{1}' does not match the value '9999-12-31'."),
		FIELD_DOES_NOT_MATCH_THE_VALUE(1008, 4, "Field '{0}': value '{1}' must be {2} the value '{3}'."),
		FIELD_CONTAINS_AN_INVALID_PERIOD(1009, 4, "Field '{0}': value '{1}' must lie before the '{2}': value '{3}'."),
		FIELD_DOES_NOT_MATCH_ANY_VALUE(1010, 3, "Field '{0}': value '{1}' should match one of the following values '{2}'."),
		MULTIPLE_TAGS_WITH_SAME_VALUE_IN_PERIOD(1011, 4, "Multiple '{0}' tags found with {1} '{2}' on '{3}'."),
		INCORRECT_SAP_MDUS_TIME_PERIOD(1012, 3, "Sap Mdus Time Period is incorrect: '{0}'. The From Date must be before or equal to the To Date. Fields From '{1}' and To '{2}'."),
		INCORRECT_SAP_MDUS_TIME_PERIOD_FROM_AND_TO_BOTH_MAXIMUM(1013, 2, "Sap Mdus Time Period is incorrect: From Date and To Date are both maximum dates '31/12/9999 00:00:00'. Fields From '{0}' and To '{1}'."),
		FIELD_SHOULD_NOT_BE_FILLED_IN(1014,1,"Field '{0}' should not be filled in."),
		FIELD_MUST_BE_EMPTY_FOR_SLAVE_DEVICE(1015, 1, "'{0}' field must be empty in case of a slave device."),
		FIELD_AND_ITS_RELATED_FIELDS_MUST_BE_EMPTY(1016, 2, "'{0}' field and its related fields ({1}) must be empty in case of a slave device."),
		FIELD_MUST_BE_EMPTY(1017, 1, "'{0}' field must be empty."),
		FIELD_DOES_NOT_MATCH_FORMAT(1018, 3, "Field '{0}': value '{1}' should match the format '{2}'."),
		FIELD_NOT_FOUND_IN_LOOKUP_TABLE(1019, 2, "Field '{0}': value '{1}' not found in lookup table."),
		OBJECT_ALREADY_EXISTS(1020, 2, "Object '{0}': value '{1}' already exists."),
		OBJECT_DOES_NOT_EXISTS(1021, 2, "Object '{0}': value '{1}' does not exists."),
		WRONG_OBJECT(1022, 3, "Object '{0}': value '{1}' '{2}'."),
		MESSAGE_IS_EMPTY(1023, 0, "Message content is empty."),
		OBJECT_HAS_CHILDREN(1024, 3, "Object '{0}': value '{1}' has sub-objects of type '{2}'."),
		WRONG_OBJECT_HIERARCHY(1025, 5, "Object '{0}': value '{1}' under '{2}' value '{3}' does not match input '{4}'."),
		WRONG_MESSAGE_SEQUENCE(1026, 2, "Message is earlier '{0}' than last object timestamp '{1}'."),
		OBJECT_HAS_REFERENCES(1027, 2, "Object '{0}': value '{1}' is referenced by other objects, and cannot be deleted."),
		MESSAGE_VALIDATION_FAILED(1028, 2, "Failed validating field '{0}': value '{1}'."),
		WRONG_DEVICE_CONFIGURATION(1029, 3, "Device Configuration validation failed '{0}': value '{1}''{2}'."),
		OVERLAPPING_POD(1030, 5, "At least one overlapping POD assignment is found for POD (Name: '{0}') and meter (Name: '{1}', External Name: '{2}') for period (Start Date: '{3}', End Date: '{4}')."),
		SIGNATURE_NOT_GENERATED_BY_MANUFACTURER(1031, 0, "The digital signature of the shipment file is not generated by the manufacturer."),
		OBJECT_NOT_ALLOWED(1032, 5, "Object '{0}': value '{1}' is not allowed under object '{2}' value '{3}', reason: '{4}'."),
		;

					
		private IecExceptionReference reference;
		
		ValidationExceptionMessage(long id, int expectedNumberOfMessageParameters, String defaultMessage) {
			reference = new IecExceptionReference(id, expectedNumberOfMessageParameters, defaultMessage);
		}

		@Override
		public String getDefaultMessage() {
			return reference.getDefaultMessage();
		}
	
		@Override
		public int getExpectedNumberOfMessageParameters() {
			return reference.getExpectedNumberOfMessageParameters();
		}
		
		public long getId() {
			return reference.getId();
		}
	
		@Override
		public String toMessageResourceKey() {
			return reference.toMessageResourceKey();
		}
		
		public String getMessage(Object... parameters) {
			return reference.getMessage(parameters);
		}

		public Issue getIssue(Object... parameters) {
			return new ValidationIssue(getMessage(parameters), VALIDATION_FAILED, reference.getModuleName(), getId());
		}
		
		public Issue getIssue(Throwable source, Object... parameters) {
			return new ValidationIssue(getMessage(parameters), source, VALIDATION_FAILED, reference.getModuleName(), getId());
		}
	}
	
	public enum ExecutionExceptionMessage implements ExceptionCode {
		CANNOT_CREATE_OBJECT(2001, 2, "Failed creating object '{0}': value '{1}'."),
		CANNOT_UPDATE_OBJECT(2002, 2, "Failed updating object '{0}': value '{1}'."),
		CANNOT_DELETE_OBJECT(2003, 2, "Failed deleting object '{0}': value '{1}'."),
		MESSAGE_PROCESSING_FAILED(2004, 2, "Failed processing object '{0}': value '{1}'."),
		UNABLE_TO_CREATE_SYSTEM_PARAMTER(2005, 2, "Unable to create System Parameter name: '{0}', display name: '{1}'"),
		ENDPOINT_DOWN(2006, 1, "Endpoint '{0}' is down"),
		CANNOT_FIND_OBJECT(2007, 2, "Failed finding object '{0}': value '{1}'."),
		NO_PERMISSION(2008, 1, "No permission for this operation: '{0}'."),
		;
					
		private IecExceptionReference reference;
		
		ExecutionExceptionMessage(long id, int expectedNumberOfMessageParameters, String defaultMessage) {
			reference = new IecExceptionReference(id, expectedNumberOfMessageParameters, defaultMessage);
		}

		@Override
		public String getDefaultMessage() {
			return reference.getDefaultMessage();
		}
	
		@Override
		public int getExpectedNumberOfMessageParameters() {
			return reference.getExpectedNumberOfMessageParameters();
		}
		
		public long getId() {
			return reference.getId();
		}
	
		@Override
		public String toMessageResourceKey() {
			return reference.toMessageResourceKey();
		}
		
		public String getMessage(Object... parameters) {
			return reference.getMessage(parameters);
		}
		public Issue getIssue(Object... parameters) {
			return new ExecutionIssue(getMessage(parameters), ERROR, reference.getModuleName(), getId());
		}
		
		public Issue getIssue(Throwable source, Object... parameters) {
			return new ExecutionIssue(getMessage(parameters), source, ERROR, reference.getModuleName(), getId());
		}
	}
	
	private long id;
	private int expectedNumberOfMessageParameters;
	private String defaultMessage;
	
	public IecExceptionReference(long id, int expectedNumberOfMessageParameters, String defaultMessage) {
		this.id = id;
		this.expectedNumberOfMessageParameters = expectedNumberOfMessageParameters;
		this.defaultMessage = defaultMessage;
	}

	public String getMessage(Object[] parameters) {
		return Utils.format(getDefaultMessage(), parameters);
	}

	public String getDefaultMessage() {
		return defaultMessage;
	}

	public String toMessageResourceKey() {
		return getModuleName() + "-" + getId();
	}

	public long getId() {
		return id;
	}

	public int getExpectedNumberOfMessageParameters() {
		return expectedNumberOfMessageParameters;
	}
	
	public String getModuleName() {
		return IecCustomerModule.MODULE_NAME;
	}
	
	
}

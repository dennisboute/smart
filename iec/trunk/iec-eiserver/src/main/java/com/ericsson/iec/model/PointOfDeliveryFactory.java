package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface PointOfDeliveryFactory extends FolderVersionWrapperFactory<PointOfDelivery> {

	PointOfDelivery findByKey(String key);
}

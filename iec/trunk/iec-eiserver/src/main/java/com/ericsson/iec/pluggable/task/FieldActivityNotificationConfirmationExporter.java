package com.ericsson.iec.pluggable.task;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exporter.ConfirmationExporter;
import com.energyict.mdw.service.ServiceRequestType;
import com.ericsson.iec.mdus.MdusWebservice;

public class FieldActivityNotificationConfirmationExporter extends ConfirmationExporter {

	@Override
	protected ServiceRequestType getServiceRequestType() throws BusinessException {
		return MdusWebservice.FIELD_ACTIVITY_NOTIFICATION_REQUEST.getServiceRequestType();
	}

}

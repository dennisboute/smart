package com.ericsson.iec.model;

import com.energyict.mdw.relation.*;
import com.energyict.projects.common.model.relation.RelationAdapter;
import com.energyict.projects.common.model.relation.RelationWrapperFactoryImpl;
import com.energyict.projects.dateandtime.Clock;

import java.math.BigDecimal;

import static com.ericsson.iec.constants.FolderTypeConstants.FolderTypes.NOC_EVENT_DETAILS;

public class NocEventDetailsFactoryImpl extends RelationWrapperFactoryImpl<NocEventDetails> implements NocEventDetailsFactory {

    @Override
    public NocEventDetails createNew(RelationAdapter adapter) {
        return new NocEventDetailsImpl(adapter);
    }

    @Override
    protected String getRelationTypeName() {
        return NOC_EVENT_DETAILS.name;
    }

    @Override
    public NocEventDetails findByCode(String eventCodeLogicalEventGroup) {
        if (eventCodeLogicalEventGroup != null) {
            FilterAspect aspectUnique = new RelationDynamicAspect(getRelationType().getAttributeType("eventCode"));
            // meerdere criterie
            String eventCode = eventCodeLogicalEventGroup.split("_")[0];
            String logicalEventGroup = eventCodeLogicalEventGroup.split("_")[1];
            CompositeFilterCriterium compositeFilterCriterium = new CompositeFilterCriterium(CompositeFilterCriterium.AND_OPERATOR);
            SimpleFilterCriterium aspectEventCodeCriterium = new SimpleFilterCriterium(aspectUnique, SimpleFilterCriterium.OPERATOR_EQUALS, new BigDecimal(eventCode));
            SimpleFilterCriterium aspectLogicalEventGroupCriterium = new SimpleFilterCriterium(aspectUnique, SimpleFilterCriterium.OPERATOR_EQUALS, new BigDecimal(logicalEventGroup));
            compositeFilterCriterium.add(aspectEventCodeCriterium);
            compositeFilterCriterium.add(aspectLogicalEventGroupCriterium);

            RelationSearchFilter filter = new RelationSearchFilter(aspectEventCodeCriterium);

            for (Relation relation : getRelationType().findByFilter(filter)) {
                if (relation.getPeriod().includes(Clock.INSTANCE.get().now())) {
                    return createNew(relation, relation.getFrom());
                }
            }
        }
        return null;
    }
}

package com.ericsson.iec.mdus.handler.consumptionrequest;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.*;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory.ActionType;
import com.ericsson.iec.model.City;
import com.ericsson.iec.model.District;
import com.ericsson.iec.model.Region;
import com.ericsson.iec.model.TransformerStation;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest.MessageContent;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.*;

public class TransformerStationConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<TransformerStationRequest> {


	@Override
	protected TransformerStationRequest unmarshal(String request) throws MarshallingException {
		return (TransformerStationRequest) IecMarshallerHelper.getInstance().unmarshall(request, TransformerStationRequest.class);
	}
	
	@Override
	protected void doProcess(TransformerStationRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		MessageContent requestContent = message.getMessageContent();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(requestContent, processContext, logger);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class ProcessContext {
		String stationNumerator;
		String districtCode;
		String regionCode;
		String cityCode;
		String cityName;
		Date date;
		int numberOfTransformers;
		int numberOfPlcTransformers;
		ActionType action;

		District district;
		Region region;
		City city;
		TransformerStation ts;
		
		int maxNumberOfTransformers;
		int transformerCountByTs;
		
		public ProcessContext(MessageContent requestContent) {
			initialize(requestContent);
		}
		
		private void initialize(MessageContent requestContent) {
			if (requestContent == null)
				return;
			
			stationNumerator = requestContent.getSTATIONNUMERATOR();
			districtCode = requestContent.getMACHOZKOD();
			regionCode = requestContent.getEZORKOD();
			cityCode = requestContent.getCITYKOD();
			cityName = requestContent.getCITYNAME();
			date = requestContent.getDATE().toGregorianCalendar().getTime();
			numberOfTransformers = requestContent.getNUMTRANSINSTATION();
			numberOfPlcTransformers = requestContent.getNUMPLCTRANSINSTATION();
			action = ActionType.getActionByName(requestContent.getACTION());
			
			district = CommonObjectFactory.getDistrict(districtCode);
			region = CommonObjectFactory.getRegion(regionCode);
			city = CommonObjectFactory.getCity(cityCode);
			ts = CommonObjectFactory.getTransformerStation(stationNumerator);
			
			transformerCountByTs = CommonObjectFactory.getTransformerCountByTs(ts);
			maxNumberOfTransformers = IecSystemParameters.NIS_MAX_NUMBER_OF_TRANSFORMERS.getIntValue();
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class RequestValidator {
		private static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);
	
			// Validate Fields:
			CommonValidatorFactory.validateStationNumerator(pc.stationNumerator);
			validateDistrict(pc);
			validateRegion(pc);
			validateCity(pc);
			CommonValidatorFactory.validateCoordinate(requestContent.getSTATIONKORDINATEX(), "STATION_KORDINATE_X");
			CommonValidatorFactory.validateCoordinate(requestContent.getSTATIONKORDINATEY(), "STATION_KORDINATE_Y");
			validateStationType(requestContent.getSTATIONTYPE());
			validateNumberOfTransformers(pc);
			validateNumberOfPlcTransformers(pc);
			CommonValidatorFactory.validateAction(pc.action);
	
			// Validate by Context:
			if (pc.action == ActionType.INSERT) {
				if (pc.ts != null) 
					throw new NonFatalValidationException(OBJECT_ALREADY_EXISTS, "Transformer Station", pc.stationNumerator);
			} else { // UPDATE | REMOVE
				if (pc.ts == null)
					throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer Station", pc.stationNumerator);
			}
	
			if (pc.action == ActionType.INSERT || pc.action == ActionType.UPDATE) {
				if (!pc.region.getDistrict().getExternalName().equals(pc.district.getExternalName()))
					throw new FatalValidationException(WRONG_OBJECT_HIERARCHY, "Region", pc.region.getExternalName(), "District", pc.region.getDistrict().getExternalName(), pc.district.getExternalName());
				if (pc.city != null) {
					if (!pc.city.getDistrict().getExternalName().equals(pc.district.getExternalName()))
						throw new FatalValidationException(WRONG_OBJECT_HIERARCHY, "City", pc.city.getExternalName(), "District", pc.city.getDistrict().getExternalName(), pc.district.getExternalName());
					if (!pc.city.getRegion().getExternalName().equals(pc.region.getExternalName()))
						throw new FatalValidationException(WRONG_OBJECT_HIERARCHY, "City", pc.city.getExternalName(), "Region", pc.city.getRegion().getExternalName(), pc.region.getExternalName());
				}
			}
	
			if (pc.action == ActionType.UPDATE) {
				if (pc.ts.getLatestNisUpdateDate().compareTo(pc.date) > 0)
					throw new FatalValidationException(WRONG_MESSAGE_SEQUENCE, pc.date, pc.ts.getLatestNisUpdateDate());
			}
	
			if (pc.action == ActionType.REMOVE) {
				if (pc.transformerCountByTs > 0)
					throw new NonFatalValidationException(OBJECT_HAS_CHILDREN, "Transformer Station", pc.stationNumerator, "Transformer");
				if (!pc.ts.getFolder().canDelete())
					throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "Transformer Station", pc.ts.getExternalName());
			}
		}
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateDistrict(ProcessContext pc) throws FatalValidationException {
			if (pc.district == null)
				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "MACHOZ_KOD", pc.districtCode);
		}
	
		private static void validateRegion(ProcessContext pc) throws FatalValidationException {
			if (pc.region == null)
				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "EZOR_KOD", pc.regionCode);
		}
	
		private static void validateCity(ProcessContext pc) throws FatalValidationException {
			if (pc.action == ActionType.UPDATE && pc.city == null)
				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "CITY_KOD", pc.cityCode);
			if (pc.action == ActionType.INSERT && pc.city == null && (pc.cityCode == null || pc.cityCode.isEmpty()))
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "CITY_KOD");
			if (pc.action == ActionType.INSERT && pc.city == null && (pc.cityName == null || pc.cityName.isEmpty()))
				throw new FatalValidationException(FIELD_MUST_BE_FILLED_IN, "CITY_NAME");
		}
		
		private static void validateStationType(Integer stationType) throws FatalValidationException {
			if (stationType != null && !(stationType >= 1 && stationType <= 3))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "STATION_TYPE", stationType, "[1,2,3]");
		}
	
		private static void validateNumberOfTransformers(ProcessContext pc) throws AbstractValidationException {
			if (!(pc.numberOfTransformers >= 0 && pc.numberOfTransformers <= pc.maxNumberOfTransformers))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "NUM_TRANS_IN_STATION", pc.numberOfTransformers, "[0,1,2,...," + pc.maxNumberOfTransformers + "]");
			
			if (pc.action == ActionType.UPDATE)
				if (pc.ts != null && pc.transformerCountByTs >= pc.numberOfTransformers)
					throw new NonFatalValidationException(FIELD_DOES_NOT_MATCH_THE_VALUE, "NUM_TRANS_IN_STATION", pc.numberOfTransformers, "transformers under this station", pc.transformerCountByTs);
		}
	
		private static void validateNumberOfPlcTransformers(ProcessContext pc) throws FatalValidationException {
			if (!(pc.numberOfPlcTransformers >= 0 && pc.numberOfPlcTransformers <= pc.maxNumberOfTransformers))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "NUM_PLC_TRANS_IN_STATION", pc.numberOfPlcTransformers, "[0,1,2,...," + pc.maxNumberOfTransformers + "]");
			if  (pc.numberOfPlcTransformers > pc.numberOfTransformers)
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_ANY_VALUE, "NUM_PLC_TRANS_IN_STATION", pc.numberOfPlcTransformers, "Must be lower than NUM_TRANS_IN_STATION");
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		private static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
			if (pc.action == ActionType.INSERT) {
				pc.ts = createTransformerStation();
	
				if (pc.city == null) {
					pc.city = createCity(requestContent, pc);
					logger.info("Created City " + pc.city.getName() + " (" + pc.city.getExternalName() + ")");
				}
			}
	
			if (pc.action == ActionType.REMOVE) {
				deleteTransformerStation(pc.ts);
				logger.info("Transformer Station Deleted");
			} else { // INSERT | UPDATE:
				try {
					pc.ts.setDistrict(pc.district);
					pc.ts.setRegion(pc.region);
					pc.ts.setCity(pc.city);
					pc.ts.setStationId(requestContent.getSTATIONID());
					pc.ts.setStationName(requestContent.getSTATIONNAME());
					pc.ts.setStationLocation(requestContent.getSTATIONLOCATION());
					pc.ts.setCoordinates(CommonObjectFactory.getCoordinates(requestContent.getSTATIONKORDINATEX(), requestContent.getSTATIONKORDINATEY()));
					pc.ts.setStationType(getStationType(requestContent.getSTATIONTYPE()));
					pc.ts.setNumberOfTransformers(getNumberOfTransformers(requestContent));
					pc.ts.setNumberOfPlcTransformers(getNumberOfPlcTransformers(requestContent));
					pc.ts.setRemarks(requestContent.getREMARKSFORSTATION());
					pc.ts.setLatestNisUpdateDate(pc.date);
		
					pc.ts.setName(pc.stationNumerator.toString());
					pc.ts.setExternalName(FolderTypes.TRANSFORMER_STATION.buildExternalName(pc.stationNumerator.toString()));
					pc.ts.setActiveDate(new Date(0));
					pc.ts.setFrom(new Date(0));
					pc.ts.setParent(pc.city.getFolder()); // TS is placed  under city
				} catch (Exception e) {
					logger.severe("Failed processing the message (General Error): " + e.getMessage());
					throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Transformer Station", pc.stationNumerator);
				}
	
				updateTransformerStation(pc.ts, pc.action);
				logger.info("Transformer Station Saved");
			}
			logger.info("Message Processing Completed");
		}
	
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
		private static City createCity(MessageContent requestContent, ProcessContext pc) throws IecException {
			try {
				City city = IecWarehouse.getInstance().getCityFactory().createNew();
				
				city.setDistrict(pc.district);
				city.setRegion(pc.region);
				city.setLatestNisUpdateDate(requestContent.getDATE().toGregorianCalendar().getTime());
				
				city.setName(pc.cityName);
				city.setExternalName(FolderTypes.CITY.buildExternalName(pc.cityCode));
				city.setActiveDate(new Date(0));
				city.setFrom(new Date(0));
				city.setParent(pc.region.getFolder());
		
				city.saveChanges();
	
				return city;
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "City", pc.cityCode);
			} 
		}
	
		private static BigDecimal getStationType(Integer stationType) {
			if (stationType != null)
				return new BigDecimal(stationType);
			else
				return null;
		}
		
		private static TransformerStation createTransformerStation() {
			return IecWarehouse.getInstance().getTransformerStationFactory().createNew(); 
		}
		
		private static void updateTransformerStation(TransformerStation ts, ActionType action) throws IecException {
			try {
				ts.saveChanges();
			} catch (BusinessException | SQLException e) {
				if (action == ActionType.INSERT)
					throw new NonFatalExecutionException(e, CANNOT_CREATE_OBJECT, "Transformer Station", ts.getExternalName());
				else
					throw new NonFatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Transformer Station", ts.getExternalName());
			}
		}
	
		private static void deleteTransformerStation(TransformerStation ts) throws IecException {
			try {
	//			ts.delete();
				ts.deleteFolder();
			} catch (BusinessException | SQLException e) {
				throw new NonFatalExecutionException(e, CANNOT_DELETE_OBJECT, "Transformer Station", ts.getExternalName());
			}
		}
	
		private static BigDecimal getNumberOfTransformers(MessageContent requestContent) {
			int numberOfTransformersInt = requestContent.getNUMTRANSINSTATION();
			return new BigDecimal(numberOfTransformersInt);
		}
	
		private static BigDecimal getNumberOfPlcTransformers(MessageContent requestContent) {
			int numberOfPlcTransformersInt = requestContent.getNUMPLCTRANSINSTATION();
			return new BigDecimal(numberOfPlcTransformersInt);
		}
	}
}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapper;

public interface MeterLocation extends FolderVersionWrapper {

	String getCountry();
	void setCountry(String country);

	String getDistrict();
	void setDistrict(String district);

	String getRegion();
	void setRegion(String region);

	String getPostalCode();
	void setPostalCode(String postalCode);

	String getCityCode();
	void setCityCode(String cityCode);

	String getCityName();
	void setCityName(String cityName);

	String getStreet();
	void setStreet(String street);

	String getStreetNumber();
	void setStreetNumber(String streetNumber);
	
	String getLogicalInstallationPointID();
	void setLogicalInstallationPointID(String logicalInstallationPointID);
}

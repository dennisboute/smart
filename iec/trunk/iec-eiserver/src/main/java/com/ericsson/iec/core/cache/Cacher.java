package com.ericsson.iec.core.cache;

import com.ericsson.iec.core.exception.IecException;

public interface Cacher<K, V> {

    V getValue(K key) throws IecException;

}

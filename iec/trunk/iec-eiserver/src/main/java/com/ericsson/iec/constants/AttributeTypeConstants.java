package com.ericsson.iec.constants;

public class AttributeTypeConstants {

    public enum DistrictAttributes {
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        DistrictAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum RegionAttributes {
        DISTRICT("district"),
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        RegionAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum CityAttributes {
        DISTRICT("district"),
        REGION("region"),
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        CityAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum TransformerStationAttributes {
        DISTRICT("district"),
        REGION("region"),
        CITY("city"),
        STATION_ID("stationId"),
        STATION_NAME("stationName"),
        STATION_LOCATION("stationLocation"),
        COORDINATES("coordinates"),
        STATION_TYPE("stationType"),
        NUMBER_OF_TRANSFORMERS("numberOfTransformers"),
        NUMBER_OF_PLC_TRANSFORMERS("numberOfPlcTransformers"),
        REMARKS("remarks"),
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        TransformerStationAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum TransformerAttributes {
        DISTRICT("district"),
        REGION("region"),
        CITY("city"),
        TRANSFORMER_STATION("transformerStation"),
        HIGH_VOLTAGE_LINE_FEEDER("highVoltageLineFeeder"),
        NAME_HIGH_VOLTAGE_LINE_FEEDER("nameHighVoltageLineFeeder"),
        COORDINATES("coordinates"),
        MANUFACTURER("manufacturer"),
        SUPPLY_VOLTAGE_AMPERE_RATING("supplyVoltageAmpereRating"),
        DRY_OIL("dryOil"),
        TRANSFORMER_TYPE("transformerType"),
        REMARKS("remarks"),
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        TransformerAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PremiseAttributes {
        TRANSFORMER_STATION("transformerStation"),
        TRANSFORMER("transformer"),
        HIGH_VOLTAGE_LINE_FEEDER("highVoltageLineFeeder"),
        HIGH_VOLTAGE_LINE_FEEDER_NAME("highVoltageLineFeederName"),
        LOW_VOLTAGE_LINE_FEEDER_ID("lowVoltageLineFeederId"),
        PREMISE_TYPE("premiseType"),
        METER_USAGE_TYPE("meterUsageType"),
        ELECTRIC_NET_TYPE("electricNetType"),
        DISTRICT("district"),
        REGION("region"),
        CITY_CODE("cityCode"),
        CITY_NAME("cityName"),
        STREET("street"),
        STREET_NUMBER("streetNumber"),
        ENTRANCE("entrance"),
        FLOOR("floor"),
        APARTMENT("apartment"),
        ADDRESS("address"),
        COORDINATES("coordinates"),
        LATEST_NIS_UPDATE_DATE("latestNisUpdateDate"),;
        public String name;

        PremiseAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum DataConcentratorAttributes {
        STATUS("status"),
        DEVICE("device"),
        SERIAL_ID("serialId"),
        MANUFACTURER("manufacturer"),
        MANUFACTURING_YEAR("manufacturingYear"),
        MODEL_DESCRIPTION("modelDescription"),
        MAC_PLC("macPlc"),
        FIRMWARE_VERSION("firmwareVersion"),
        COMMENTS("comments"),;
        public String name;

        DataConcentratorAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum FirmwareVersionAttributes {
        FIRMWARE_VERSION("firmwareVersion"),;
        public String name;

        FirmwareVersionAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum ProcessCaseWithErrorHandlingAttributes {
        TRANSITION_SUCCESS("transitionSuccess"),
        ERROR_MESSAGE("errorMessage"),
        TRANSITION_NAME("transitionName");
        public String name;

        ProcessCaseWithErrorHandlingAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum DataConcentratorDeploymentAttributes {
        DATA_CONCENTRATOR("dataconcentrator"),
        DEPLOYMENT_STATUS("deploymentStatus"),
        TRACKING_ID("trackingId"),
        RETRY("retry"),
        SERVICE_REQUEST("serviceRequest");
        public String name;

        DataConcentratorDeploymentAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PlcMeterRegistrationAttributes {
        METER("meter"),
        REGISTRATION_STATUS("registrationStatus"),
        RETRY("retry"),
        SERVICE_REQUEST("serviceRequest");
        public String name;

        PlcMeterRegistrationAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum CellularGridAttributes {
        IDENTIFIER("identifier"),;
        public String name;

        CellularGridAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum CtMeterGridAttributes {
        IDENTIFIER("identifier"),;
        public String name;

        CtMeterGridAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PlcGridAttributes {
        IDENTIFIER("identifier"),;
        public String name;

        PlcGridAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum MeterLocationAttributes {
        COUNTRY("country"),
        DISTRICT("district"),
        REGION("region"),
        POSTAL_CODE("postalCode"),
        CITY_CODE("cityCode"),
        CITY_NAME("cityName"),
        STREET("street"),
        STREET_NUMBER("streetNumber"),
        LOGICAL_INSTALLATION_POINT_ID("logicalInstallationPointID"),
        ;
        public String name;

        MeterLocationAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum MeterAttributes {
        PREMISE("premise"),
        STATUS("status"),
        METER_MODEL("meterModel"),
        DEVICE("device"),
        SERIAL_ID("serialId"),
        MANUFACTURER("manufacturer"),
        BATCH("batch"),
        FIRMWARE_VERSION("firmwareVersion"),
        DELIVERY_DATE("deliveryDate"),
        CONFIGURATION_DATE("configurationDate"),
        METER_LOCATION("meterLocation"),
        VIRTUAL_METER("virtualMeter"),
        COMMENTS("comments"),
        MAC_ADDRESS("macAddress"),;
        public String name;

        MeterAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PointOfDeliveryAttributes {
        VIRTUAL_METER("virtualMeter"),;
        public String name;

        PointOfDeliveryAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PodAssignmentAttributes {
        POINT_OF_DELIVERY("pointOfDelivery"),
        METER("meter"),
        UNIQUE_IDENTIFIER("uniqueIdentifier"),;
        public String name;

        PodAssignmentAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum NocEventConfigurationAttributes {
        DESCRIPTION("description");

        public String name;

        NocEventConfigurationAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum NocEventDetailsAttributes {
        CONFIG_FOLDER("configFolder"),
        EVENT_CODE("eventCode"),
        LOGICAL_EVENT_GROUP("logicalEventGroup");

        public String name;

        NocEventDetailsAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum PlcEventGroupAttributes {
        EVENT_GROUP_MAPPING("eventGroup");

        public String name;

        PlcEventGroupAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

    public enum AuthenticationAndEncryptionKeyRenewalAttributes {
        DEVICE("device"),
        METER("meter"),
        AK_KEY_TYPE("authenticationKeyType"),
        EK_KEY_TYPE("encryptionKeyType");

        public String name;

        AuthenticationAndEncryptionKeyRenewalAttributes(String attributeName) {
            name = attributeName;
        }

        public String getName() {
            return name;
        }
    }

}

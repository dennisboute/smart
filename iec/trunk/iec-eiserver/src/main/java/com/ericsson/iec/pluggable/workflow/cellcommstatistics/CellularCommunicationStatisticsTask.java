package com.ericsson.iec.pluggable.workflow.cellcommstatistics;

import static com.ericsson.iec.core.exception.IecExceptionReference.ConfigurationExceptionMessage.OBJECT_CONFIGURATION_ERROR;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.energyict.cbo.BusinessException;
import com.energyict.eisexport.core.AbstractExporter;
import com.energyict.mdc.journal.ComSession;
import com.energyict.mdc.journal.TaskExecutionSummary;
import com.energyict.mdc.tasks.ConnectionTask;
import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdus.core.SapResponseMessage;
import com.energyict.mdw.core.Device;
import com.energyict.mdw.service.ServiceRequest;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalConfigurationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.IecClassInjector;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.service.CellularCommunicationStatisticsRequestBuilder;

import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.CellularCommunicationStatisticsRequest.Request;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.CellularCommunicationStatisticsRequest.Request.CellularMeterStatisticsPK;
import iec.tibco.mdm.ws_mdm_noc_01.resources.xsd.abs.ws_mdm_noc_01.ObjectFactory;

public class CellularCommunicationStatisticsTask extends AbstractExporter {

	@Override
	protected void export() throws IOException, BusinessException, SQLException {

		final int MAX_NOC_MESSAGE_COUNT = IecSystemParameters.MAX_CELL_COMM_STATISTICS_MSG_TO_NOC.getIntValue();

		ObjectFactory objectFactory = new ObjectFactory();
		Request nocRequest = objectFactory.createCellularCommunicationStatisticsRequestRequest();

		List<GenericMeter> cellularMeterList = IecWarehouse.getInstance().getCellularMeterFactory().findAll(new Date());

		// Tester:
//		accumulateNocRequest(nocRequest, "meter1", new Date(), new Date());
//		accumulateNocRequest(nocRequest, "meter2", new Date(), new Date());
//		sendNocRequest(nocRequest);

		for (GenericMeter meter : cellularMeterList) {
			String meterExternalName  = FolderTypes.extractFolderKey(meter.getExternalName()); // support null external name
			if (meterExternalName == null || meterExternalName.isEmpty())
				continue;
			BigDecimal meterStatus = meter.getStatus();
			if (meterStatus == null || !meterStatus.equals(new BigDecimal(3))) // 3 = "Installed - Unsealed"
				continue;
						
			Device meterDevice = meter.getDevice().getDevice();
			
			Date lastSuccessfulTime = null;
			Date lastExecutionTime = null;
			List<ConnectionTask> connectionTasksList = meterDevice.getAllConnectionTasks(); 

			// find max dates (loop over all completed tasks):
			for (ConnectionTask task : connectionTasksList) {
				TaskExecutionSummary sessionSummary = task.getLastTaskExecutionSummary();
				if (!(sessionSummary instanceof ComSession))
					throw new FatalConfigurationException(OBJECT_CONFIGURATION_ERROR, "Meter", meterExternalName, "Execution Summary of Meter Connection Task is not of type ComSession");
				ComSession session = (ComSession)sessionSummary;

				Date taskExecutionTime = session.getStopDate();
				Date taskSuccessfulTime = task.getLastSuccessfulCommunicationEnd();
				if (lastExecutionTime == null || lastExecutionTime.compareTo(taskExecutionTime) < 0)
					lastExecutionTime = taskExecutionTime;
				if (lastSuccessfulTime == null || lastSuccessfulTime.compareTo(taskSuccessfulTime) < 0)
					lastSuccessfulTime = taskSuccessfulTime;
			}

			if (lastExecutionTime != null && lastSuccessfulTime != null) {
				accumulateNocRequest(nocRequest, meterExternalName, lastSuccessfulTime, lastExecutionTime);
				if (nocRequest.getCellularMeterStatisticsPK().size() >= MAX_NOC_MESSAGE_COUNT)
					sendNocRequest(nocRequest);
			}
		}
		sendNocRequest(nocRequest);
	}
	
	private void accumulateNocRequest(Request nocRequest, String meterSapId, Date lastSuccessfulTime, Date lastExecutionTime) throws AbstractValidationException {
		CellularMeterStatisticsPK s = new CellularMeterStatisticsPK();
		s.setUtilitiesDeviceID(meterSapId);
		s.setDateOfLastSuccefullyComunication(CommonObjectFactory.renderXMLGregorianCalendar(lastSuccessfulTime, "lastSuccessfulTime"));
		s.setResultOfLastComunicationAttemps(CommonObjectFactory.renderXMLGregorianCalendar(lastExecutionTime, "lastExecutionTime"));
		nocRequest.getCellularMeterStatisticsPK().add(s);
	}
	
	private void sendNocRequest(Request nocRequest) throws IecException {
		if (nocRequest.getCellularMeterStatisticsPK().size() < 1)
			return;

        try {
            CellularCommunicationStatisticsRequestBuilder builder = new CellularCommunicationStatisticsRequestBuilder();
            ResponseMessage responseMessage = builder.build(nocRequest);
            new IecClassInjector().getMessageSender().sendMessage(responseMessage);

            ServiceRequest serviceRequest = ((SapResponseMessage) responseMessage).getServiceRequest();
            getLogger().info("ServiceRequest: '" + serviceRequest.getExternalName() + "' has been created.");
        } catch (IecException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
        }
		nocRequest.getCellularMeterStatisticsPK().clear();
	}
	
	
	public String getDescription() {
		return "Send cellular communication statistics to NOC";
	}

	public String getVersion() {
		return "1.0";
	}
}

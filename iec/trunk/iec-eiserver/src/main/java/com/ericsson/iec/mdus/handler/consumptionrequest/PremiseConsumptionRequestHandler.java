package com.ericsson.iec.mdus.handler.consumptionrequest;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.energyict.mdw.core.Folder;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.*;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory.ActionType;
import com.ericsson.iec.model.Premise;
import com.ericsson.iec.model.Transformer;
import com.ericsson.iec.model.TransformerStation;
import nismdm01.interfaces.mdm.iec.PremiseRequest;
import nismdm01.interfaces.mdm.iec.PremiseRequest.MessageContent;

import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Logger;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.*;

public class PremiseConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<PremiseRequest> {

    private static class ProcessContext {
        String stationNumerator;
        String districtCode;
        String regionCode;
        String cityCode;
        String cityName;
        String premiseCode;
        String premiseType;
        String meterUsageType;
        String electricNetType;
        String lowVoltageFeederId;
        Date date;

        String transformerName;
        String transformerExternalName;
        ActionType action;

        TransformerStation ts;
        Transformer transformer;
        Premise premise;

        public ProcessContext(MessageContent requestContent) {
            initialize(requestContent);
        }

        private void initialize(MessageContent requestContent) {
            districtCode = requestContent.getMACHOZKOD();
            regionCode = requestContent.getEZORKOD();
            cityCode = requestContent.getCITYKOD();
            cityName = requestContent.getCITYNAME();
            stationNumerator = requestContent.getSTATIONNUMERATOR();
            transformerName = requestContent.getNUMTRANSINSCHEMA();
            premiseCode = requestContent.getPREMISECODE();
            premiseType = requestContent.getPLCCELL();
            meterUsageType = requestContent.getMETERUSAGETYPE();
            electricNetType = requestContent.getELECTRICNETTYPE();
            lowVoltageFeederId = requestContent.getLOWVOLTAGEFEEDERID();
            date = requestContent.getDATE().toGregorianCalendar().getTime();
            action = ActionType.getActionByName(requestContent.getACTION());

            premise = CommonObjectFactory.getPremise(premiseCode);
            ts = CommonObjectFactory.getTransformerStation(stationNumerator);

            transformerExternalName = CommonObjectFactory.getTransformerExternalName(transformerName, ts); // support null ts
            transformer = CommonObjectFactory.getTransformer(transformerExternalName);

            if (premise != null)
                premiseType = premise.getPremiseType();
            else if (premiseType == null || premiseType.isEmpty())
                premiseType = "P"; // This default may be changed by setMeterLocation according to 1'st meter
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestValidator {
        private static void validateMessageContent(MessageContent requestContent, ProcessContext pc) throws AbstractValidationException {
            if (requestContent == null)
                throw new FatalValidationException(MESSAGE_IS_EMPTY);

            // Validate Fields:
            CommonValidatorFactory.validateNumTransInSchema(pc.transformerName);
            CommonValidatorFactory.validatePlcCell(pc.premiseType);
            validateMeterUsageType(pc.meterUsageType);
            validateLowVoltageFeederId(pc);
            CommonValidatorFactory.validateLookup(pc.districtCode, "MACHOZ_KOD", "district", CommonObjectFactory.getPremiseFolderType());
            CommonValidatorFactory.validateLookup(pc.regionCode, "EZOR_KOD", "region", CommonObjectFactory.getPremiseFolderType());
            validateCity(pc);
            CommonValidatorFactory.validateCoordinate(requestContent.getMETERKORDINATEX(), "METER_KORDINATE_X");
            CommonValidatorFactory.validateCoordinate(requestContent.getMETERKORDINATEY(), "METER_KORDINATE_Y");
            validateElectricNetType(pc.electricNetType);
            CommonValidatorFactory.validateAction(pc.action);

            CommonValidatorFactory.validateStationNumerator(pc.stationNumerator);
            validateTransformerStation(pc);
            validateTransformer(pc);
            validateOneCtPerTransformer(pc);

            // Validate by Context:
            if (pc.action == ActionType.INSERT) {
                if (pc.premise != null)
                    throw new NonFatalValidationException(OBJECT_ALREADY_EXISTS, "Premise", pc.premiseCode);
                validateTransformerType(pc);
            } else { // UPDATE | REMOVE
                if (pc.premise == null)
                    throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Premise", pc.premiseCode);
            }

            if (pc.action == ActionType.UPDATE) {
                if (pc.premise.getLatestNisUpdateDate().compareTo(pc.date) > 0)
                    throw new NonFatalValidationException(WRONG_MESSAGE_SEQUENCE, pc.date, pc.premise.getLatestNisUpdateDate());
//				validatePremiseTransformerStation(pc); // Removed on 04/04/2018
            }

            if (pc.action == ActionType.REMOVE) {
                if (!pc.premise.getFolder().canDelete())
                    throw new NonFatalValidationException(OBJECT_HAS_REFERENCES, "Premise", pc.premise.getExternalName());
            }
        }

        private static void validateOneCtPerTransformer(ProcessContext pc) {
            //Ge ga moeten kijken of die premise CT is meterusagetype
            //Ja => Ge ga moeten kijken onder welke premise die transformer ga belanden (insert en update e)
            // ja => Ge ga moeten kijken of er al een onder zit en als er al een in zit non fatal exception => zie LLD
            if (pc.meterUsageType.equals("CT")) {
                for (Folder folder : pc.transformer.getFolder().getChildren()) {

                }
            }
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        private static void validateMeterUsageType(String meterUsageType) throws FatalValidationException {
            //Vervangen door MeterUsageType lookup!
            String[] validValues = new String[]{"CT", "NU"};
            CommonValidatorFactory.validateValueList(meterUsageType, validValues, "METER_USAGE_TYPE");
        }

        private static void validateLowVoltageFeederId(ProcessContext pc) throws FatalValidationException {
            if (!pc.meterUsageType.equals("CT"))
                CommonValidatorFactory.validateMandatoryValue(pc.lowVoltageFeederId, "LOW_VOLTAGE_FEEDER_ID");
        }

        private static void validateCity(ProcessContext pc) throws FatalValidationException {
            CommonValidatorFactory.validateMandatoryValue(pc.cityCode, "CITY_KOD");
            CommonValidatorFactory.validateMandatoryValue(pc.cityName, "CITY_NAME");
//			String lookupCityName = CommonValidatorFactory.validateLookup(pc.cityCode, "CITY_KOD", "city", CommonObjectFactory.getPremiseFolderType());
//			CommonValidatorFactory.validateValue(pc.cityName, lookupCityName, "CITY_NAME");
        }

        private static void validateElectricNetType(String electricNetType) throws FatalValidationException {
            CommonValidatorFactory.validateLookup(electricNetType, "ELECTRIC_NET_TYPE", "electricNetType", CommonObjectFactory.getPremiseFolderType());
        }

        private static void validateTransformerStation(ProcessContext pc) throws AbstractValidationException {
            if (pc.ts == null)
                throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer Station", pc.stationNumerator);
        }

        private static void validateTransformer(ProcessContext pc) throws AbstractValidationException {
            if (pc.transformer == null)
                throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer", pc.transformerExternalName);
            if (pc.transformer.getTransformerStation().getId() != pc.ts.getId())
                throw new FatalValidationException(WRONG_OBJECT_HIERARCHY, "Transformer", pc.transformer.getExternalName(), "Transformer Station", pc.transformer.getTransformerStation().getExternalName(), pc.ts.getExternalName());
        }

        private static void validateTransformerType(ProcessContext pc) throws AbstractValidationException {
            if (pc.transformer.getTransformerType().equals("C") && pc.premiseType.equals("P"))
                throw new NonFatalValidationException(OBJECT_NOT_ALLOWED, "Premise", pc.premiseCode, "Transformer", pc.transformerExternalName, "PLC Premise is not allowed under Cellular Transformer");
        }

//		private static void validatePremiseTransformerStation(ProcessContext pc) throws AbstractValidationException {
//			if  (pc.premise != null && pc.premise.getTransformerStation().getId() != pc.ts.getId())
//				throw new FatalValidationException(WRONG_OBJECT_HIERARCHY, "Premise", pc.premise.getExternalName(), "Transformer Station", pc.premise.getTransformerStation().getExternalName(), pc.ts.getExternalName());
//		}
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    private static class RequestProcessor {
        private static void processMessageContent(MessageContent requestContent, ProcessContext pc, Logger logger) throws IecException {
            if (pc.action == ActionType.INSERT)
                pc.premise = createPremise();

            if (pc.action == ActionType.REMOVE) {
                deletePremise(pc.premise);
                logger.info("Premise Deleted");
            } else { // insert / update:
                try {
                    if (pc.action == ActionType.UPDATE && pc.premise.getTransformer().getId() != pc.transformer.getId())
                        logger.info("Moving premise to transformer " + pc.transformer.getExternalName());

                    // Set Attributes:
                    pc.premise.setTransformerStation(pc.ts);
                    pc.premise.setTransformer(pc.transformer);
                    pc.premise.setHighVoltageLineFeeder(requestContent.getHIGHVOLTAGELINEFEEDER());
                    pc.premise.setNameHighVoltageLineFeeder(requestContent.getNAMEHVOLTLINEFEEDER());
                    pc.premise.setLowVoltageFeederId(pc.lowVoltageFeederId);
                    pc.premise.setPremiseType(pc.premiseType);
                    pc.premise.setMeterUsageType(pc.meterUsageType);
                    pc.premise.setElectricNetType(pc.electricNetType);
                    pc.premise.setDistrict(pc.districtCode);
                    pc.premise.setRegion(pc.regionCode);
                    pc.premise.setCityCode(pc.cityCode);
                    pc.premise.setCityName(pc.cityName);
                    pc.premise.setStreet(requestContent.getSTREETNAME());
                    pc.premise.setStreetNumber(requestContent.getHOUSENUM());
                    pc.premise.setEntrance(requestContent.getENTRANCE());
                    pc.premise.setFloor(requestContent.getFLOOR());
                    pc.premise.setApartment(requestContent.getAPARTMENT());
                    pc.premise.setAddress(requestContent.getADDRESS());
                    pc.premise.setCoordinates(CommonObjectFactory.getCoordinates(requestContent.getMETERKORDINATEX(), requestContent.getMETERKORDINATEY()));
                    pc.premise.setLatestNisUpdateDate(pc.date);

                    pc.premise.setName(pc.premiseCode);
                    pc.premise.setExternalName(FolderTypes.PREMISE.buildExternalName(pc.premiseCode));
                    pc.premise.setActiveDate(new Date(0));
                    pc.premise.setFrom(new Date(0));
                    pc.premise.setParent(CommonObjectFactory.calcPremiseParentFolder(pc.premise));
                } catch (Exception e) {
                    logger.severe("Failed processing the message (General Error): " + e.getMessage());
                    throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Premise", pc.premiseCode);
                }

                updatePremise(pc.premise, pc.action);
                logger.info("Premise Saved");
            }
            logger.info("Message Processing Completed");
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        private static Premise createPremise() {
            return IecWarehouse.getInstance().getPremiseFactory().createNew();
        }

        private static void updatePremise(Premise premise, ActionType action) throws IecException {
            try {
                if (premise.getMeterUsageType().equals("CT")) {
                    if (premise.getParent().getChildren().size() > 0) {
                        throw new FatalValidationException(IecExceptionReference.ValidationExceptionMessage.OBJECT_ALREADY_EXISTS, "CT", premise.getParent().getChildren().get(0).getName());
                    }
                }
                premise.saveChanges();
            } catch (BusinessException | SQLException e) {
                if (action == ActionType.INSERT)
                    throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Premise", premise.getExternalName());
                else
                    throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Premise", premise.getExternalName());
            }
        }

        private static void deletePremise(Premise premise) throws IecException {
            try {
                premise.deleteFolder();
            } catch (BusinessException | SQLException e) {
                throw new FatalExecutionException(e, CANNOT_DELETE_OBJECT, "Premise", premise.getExternalName());
            }
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    @Override
    protected PremiseRequest unmarshal(String request) throws MarshallingException {
        return (PremiseRequest) IecMarshallerHelper.getInstance().unmarshall(request, PremiseRequest.class);
    }

    @Override
    protected void doProcess(PremiseRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
        MessageContent requestContent = message.getMessageContent();
        ProcessContext processContext = new ProcessContext(requestContent);

        RequestValidator.validateMessageContent(requestContent, processContext);
        RequestProcessor.processMessageContent(requestContent, processContext, logger);
    }
}

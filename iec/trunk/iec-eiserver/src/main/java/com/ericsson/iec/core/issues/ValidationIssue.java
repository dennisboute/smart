package com.ericsson.iec.core.issues;

import com.energyict.projects.issues.BasicIssue;
import com.energyict.projects.issues.IssueType;

public class ValidationIssue extends BasicIssue {

	private String module;
	private long issueNumber;

	public ValidationIssue(String description, IssueType issueType, String module, long issueNumber) {
		super(description, issueType);
		this.module = module;
		this.issueNumber = issueNumber;
	}
	
	public ValidationIssue(String description, Throwable source, IssueType issueType, String module, long issueNumber) {
		super(description, source, issueType);
		this.module = module;
		this.issueNumber = issueNumber;
	}
	
	public String getFullDescription() {
		StringBuilder builder = new StringBuilder();
		builder.append(module);
		builder.append("-");
		builder.append(issueNumber);
		builder.append(": ");
		builder.append(getDescription());
		return builder.toString();
	}
}

package com.ericsson.iec.mdus.handler.consumptionrequest;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.MESSAGE_IS_EMPTY;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_DOES_NOT_EXISTS;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.energyict.cbo.BusinessException;
import com.energyict.mdus.core.exception.MarshallingException;
import com.energyict.mdus.core.model.relation.ConsumptionRequestSpecificAttributes;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.core.exception.NonFatalValidationException;
import com.ericsson.iec.mdus.IecMarshallerHelper;
import com.ericsson.iec.model.DataConcentrator;
import com.ericsson.iec.model.DataConcentratorDeployment;
import com.ericsson.iec.model.Transformer;
import com.ericsson.iec.model.TransformerStation;
import com.ericsson.iec.service.DataConcentratorDeviceUpdateService;

import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest;
import wfmmdm01.interfaces.mdm.iec.FieldActivityNotificationRequest.Request;

public class FieldActivityNotificationConsumptionRequestHandler extends AbstractIecConsumptionRequestHandler<FieldActivityNotificationRequest>{

	@Override
	protected FieldActivityNotificationRequest unmarshal(String request) throws MarshallingException {
		return (FieldActivityNotificationRequest) IecMarshallerHelper.getInstance().unmarshall(request, FieldActivityNotificationRequest.class);
	}
	
	@Override
	protected void doProcess(FieldActivityNotificationRequest message, Logger logger, ConsumptionRequestSpecificAttributes attributes) throws IecException {
		Request requestContent = message.getRequest();
		ProcessContext processContext = new ProcessContext(requestContent);
		RequestValidator.validateMessageContent(requestContent, processContext);		
		RequestProcessor.processMessageContent(requestContent, processContext, logger);
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class ProcessContext {
		String dcId;
		String stationNumerator;
		String transformerName;
		String transformerExternalName;
		
		DataConcentrator dc;
		TransformerStation ts;
		Transformer transformer;
		DataConcentratorDeviceUpdateService service;

		public ProcessContext(Request requestContent) {
			initialize(requestContent);
		}
		
		private void initialize(Request requestContent) {
			if (requestContent == null)
				return;

			dcId = "CIR" + requestContent.getSerialNumber();
			stationNumerator = requestContent.getSTATIONNUMERATOR();

			dc = CommonObjectFactory.getDataConcentrator(dcId);
			ts = CommonObjectFactory.getTransformerStation(stationNumerator);

			transformerName = requestContent.getNUMTRANSINSCHEMA();
			transformerExternalName = CommonObjectFactory.getTransformerExternalName(transformerName, ts); // support null ts
			transformer = CommonObjectFactory.getTransformer(transformerExternalName);

			service = CommonObjectFactory.getDataConcentratorDeviceUpdateService();
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	private static class RequestValidator {
		private static void validateMessageContent(Request requestContent, ProcessContext pc) throws AbstractValidationException {
			if (requestContent == null) 
				throw new FatalValidationException(MESSAGE_IS_EMPTY);
	
			// Validate Fields:
			validateOperationType(requestContent.getOperationType());
			validateOperationResult(requestContent.getOperationResult());
			validateEquipmentType(requestContent.getEquipmentType());
			validateConnectivityCheckResult(requestContent.getConnectivityCheckResult());
			CommonValidatorFactory.validateCoordinate(requestContent.getEquipmentCoordinateX(), "EQUIPMENT_COORDINATE_X");
			CommonValidatorFactory.validateCoordinate(requestContent.getEquipmentCoordinateX(), "EQUIPMENT_COORDINATE_Y");
			
			if (pc.dc == null)
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Data Concentrator", pc.dcId);
			if (pc.ts == null)
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer Station", pc.stationNumerator);
			if (pc.transformer == null)
				throw new NonFatalValidationException(OBJECT_DOES_NOT_EXISTS, "Transformer", pc.transformerExternalName);
		}
	
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void validateOperationType(String operationType) throws FatalValidationException {
			String[] validValues = new String[] { "Install", "Maintenance", "Replacement" };
			CommonValidatorFactory.validateValueList(operationType, validValues, "OPERATION_TYPE");
		}
	
		private static void validateOperationResult(String operationResult) throws FatalValidationException {
			String[] validValues = new String[] { "OK", "ERROR" };
			CommonValidatorFactory.validateValueList(operationResult, validValues, "OPERATION_RESULT");
		}
	
		private static void validateEquipmentType(String equipmentType) throws FatalValidationException {
			String[] validValues = new String[] { "DC", "FW" };
			CommonValidatorFactory.validateValueList(equipmentType, validValues, "EQUIPMENT_TYPE");
		}
	
		private static void validateConnectivityCheckResult(String connectivityCheckResult) throws FatalValidationException {
			String[] validValues = new String[] { "OK", "KO" };
			CommonValidatorFactory.validateValueList(connectivityCheckResult, validValues, "CONNECTIVITY_CHECK_RESULT");
		}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class RequestProcessor {
		private static void processMessageContent(Request requestContent, ProcessContext pc, Logger logger) throws IecException {
			if (!requestContent.getEquipmentType().equals("DC"))
				return;
			if (!requestContent.getOperationType().equals("Install"))
				return;
	
			// Update:
			try {
				if (requestContent.getOperationResult().equals("OK")) {
					pc.service.updateIpAddress(pc.dc.getDevice(), requestContent.getIPAddress()); // May throw exception if no TCP Outbound Connection is set for this device
					pc.dc.setParent(pc.transformer.getFolder());
					pc.dc.setStatus(new BigDecimal(2)); // Installed
				} else { // "ERROR"
					pc.dc.setStatus(new BigDecimal(3)); // Installed Failed
					logger.severe("DC Installaion failed: " + pc.dc.getExternalName());
				}
				
				pc.dc.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Data Concentrator", pc.dc.getExternalName());
			}
			logger.info("Field Activity Completed");

			createDataConcentratorDeployment(pc.dc);
			logger.info("Message Processing Completed");
		}
		
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		private static void createDataConcentratorDeployment(DataConcentrator dataConcentrator) throws IecException {
			try {
				DataConcentratorDeployment dataConcentratorDeployment = IecWarehouse.getInstance().getDataConcentratorDeploymentFactory().createNew();
				dataConcentratorDeployment.setDataConcentrator(dataConcentrator);
				dataConcentratorDeployment.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Data Concentrator", dataConcentrator.getExternalName());
			}
		}
	}
}

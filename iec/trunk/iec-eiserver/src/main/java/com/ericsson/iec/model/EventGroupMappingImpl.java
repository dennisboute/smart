package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;

import java.math.BigDecimal;

import static com.ericsson.iec.constants.AttributeTypeConstants.PlcEventGroupAttributes.EVENT_GROUP_MAPPING;

public class EventGroupMappingImpl extends FolderVersionWrapperImpl implements EventGroupMapping {

	private static final long serialVersionUID = -6571113779229681175L;

	protected EventGroupMappingImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public BigDecimal getEventGroup() {
		return getBigDecimalAttribute(EVENT_GROUP_MAPPING.name);
	}

}

package com.ericsson.iec.model;

import com.energyict.projects.common.model.folder.FolderVersionWrapperFactory;

public interface TransformerStationFactory extends FolderVersionWrapperFactory<TransformerStation> {

	TransformerStation findByKey(String key);
	
}

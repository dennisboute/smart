package com.ericsson.iec.model;

import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.BATCH;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.COMMENTS;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.CONFIGURATION_DATE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.DELIVERY_DATE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.DEVICE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.FIRMWARE_VERSION;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.MAC_ADDRESS;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.MANUFACTURER;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.METER_LOCATION;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.METER_MODEL;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.PREMISE;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.SERIAL_ID;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.STATUS;
import static com.ericsson.iec.constants.AttributeTypeConstants.MeterAttributes.VIRTUAL_METER;

import java.math.BigDecimal;
import java.util.Date;

import com.energyict.mdw.core.VirtualMeter;
import com.energyict.projects.common.model.folder.FolderVersionAdapter;
import com.energyict.projects.common.model.folder.FolderVersionWrapperImpl;
import com.ericsson.iec.constants.FolderTypeConstants.FolderTypes;
import com.ericsson.iec.core.IecWarehouse;

public class GenericMeterImpl extends FolderVersionWrapperImpl implements GenericMeter {

	private static final long serialVersionUID = -1871659369835667823L;

	protected GenericMeterImpl(FolderVersionAdapter folderVersionAdapter) {
		super(folderVersionAdapter);
	}

	@Override
	public Premise getPremise() {
		return getFolderVersionWrapperAttribute(PREMISE.name, IecWarehouse.getInstance().getPremiseFactory());
	}
	@Override
	public void setPremise(Premise premise) {
		setFolderVersionWrapperAttribute(PREMISE.name, premise);		
	}

	@Override
	public BigDecimal getStatus() {
		return getBigDecimalAttribute(STATUS.name);
	}
	@Override
	public void setStatus(BigDecimal status) {
		setBigDecimalAttribute(STATUS.name, status);
	}

	@Override
	public String getMeterModel() {
		return getStringAttribute(METER_MODEL.name);
	}
	@Override
	public void setMeterModel(String meterModel) {
		setStringAttribute(METER_MODEL.name, meterModel);
	}

	@Override
	public GenericMeterDevice getDevice() {
		GenericMeterDeviceFactory d = IecWarehouse.getInstance().getGenericMeterDeviceFactory(); 
		return d.createNew(getDeviceAttribute(DEVICE.name), new Date(0));
	}
	@Override
	public void setDevice(GenericMeterDevice device) {
		setDeviceAttribute(DEVICE.name, device.getDevice());
	}

	@Override
	public String getSerialId() {
		return getStringAttribute(SERIAL_ID.name);
	}
	@Override
	public void setSerialId(String serialId) {
		setStringAttribute(SERIAL_ID.name, serialId);
	}

	@Override
	public String getManufacturer() {
		return getStringAttribute(MANUFACTURER.name);
	}
	@Override
	public void setManufacturer(String manufacturer) {
		setStringAttribute(MANUFACTURER.name, manufacturer);
	}

	@Override
	public String getBatch() {
		return getStringAttribute(BATCH.name);
	}
	@Override
	public void setBatch(String batch) {
		setStringAttribute(BATCH.name, batch);
	}

	@Override
	public FirmwareVersion getFirmwareVersion() {
		return getFolderVersionWrapperAttribute(FIRMWARE_VERSION.name, IecWarehouse.getInstance().getFirmwareVersionFactory());
	}
	@Override
	public void setFirmwareVersion(FirmwareVersion firmwareVersion) {
		setFolderVersionWrapperAttribute(FIRMWARE_VERSION.name, firmwareVersion);		
	}

	@Override
	public Date getDeliveryDate() {
		return getDateAttribute(DELIVERY_DATE.name);
	}
	@Override
	public void setDeliveryDate(Date deliveryDate) {
		setDateAttribute(DELIVERY_DATE.name, deliveryDate);
	}

	@Override
	public Date getConfigurationDate() {
		return getDateAttribute(CONFIGURATION_DATE.name);
	}
	@Override
	public void setConfigurationDate(Date configurationDate) {
		setDateAttribute(CONFIGURATION_DATE.name, configurationDate);
	}

	@Override
	public MeterLocation getMeterLocation() {
		return getFolderVersionWrapperAttribute(METER_LOCATION.name, IecWarehouse.getInstance().getMeterLocationFactory());
	}
	@Override
	public void setMeterLocation(MeterLocation meterLocation) {
		setFolderVersionWrapperAttribute(METER_LOCATION.name, meterLocation);		
	}

	@Override
	public VirtualMeter getVirtualMeter() {
		return getVirtualMeterAttribute(VIRTUAL_METER.name);
	}
	@Override
	public void setVirtualMeter(VirtualMeter virtualMeter) {
		setVirtualMeterAttribute(VIRTUAL_METER.name, virtualMeter);
	}

	@Override
	public String getComments() {
		return getStringAttribute(COMMENTS.name);
	}
	@Override
	public void setComments(String comments) {
		setStringAttribute(COMMENTS.name, comments);
	}


	@Override
	public String getMacAddress() {
		return getStringAttribute(MAC_ADDRESS.name);
	}
	@Override
	public void setMacAddress(String macAddress) {
		setStringAttribute(MAC_ADDRESS.name, macAddress);
	}
	
	@Override
	public String getSapId() {
		return FolderTypes.extractFolderKey(this.getExternalName());
	}
}


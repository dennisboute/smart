package com.ericsson.iec.pluggable.imp;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_CREATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.CANNOT_UPDATE_OBJECT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.MESSAGE_PROCESSING_FAILED;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_DOES_NOT_MATCH_FORMAT;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.FIELD_NOT_FOUND_IN_LOOKUP_TABLE;
import static com.ericsson.iec.core.exception.IecExceptionReference.ValidationExceptionMessage.OBJECT_ALREADY_EXISTS;

import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.ShadowList;
import com.energyict.cpo.TypedProperties;
import com.energyict.hsm.worldline.imp.shipment.ShipmentFileProcessor;
import com.energyict.hsm.worldline.model.Attribute;
import com.energyict.hsm.worldline.model.Device;
import com.energyict.hsm.worldline.model.Header;
import com.energyict.hsm.worldline.model.Key;
import com.energyict.hsm.worldline.model.ShipmentInfo;
import com.energyict.mdc.shadow.protocol.security.SecurityProperties;
import com.energyict.mdw.shadow.DeviceShadow;
import com.energyict.projects.common.exceptions.CustomerException;
import com.ericsson.iec.constants.ProtoTypeConstants.ProtoTypeObjectType;
import com.ericsson.iec.constants.Warehouses;
import com.ericsson.iec.core.IecSystemParameters;
import com.ericsson.iec.core.IecWarehouse;
import com.ericsson.iec.core.exception.AbstractConfigurationException;
import com.ericsson.iec.core.exception.AbstractExecutionException;
import com.ericsson.iec.core.exception.AbstractValidationException;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.FatalValidationException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonObjectFactory;
import com.ericsson.iec.mdus.handler.consumptionrequest.CommonValidatorFactory;
import com.ericsson.iec.model.FirmwareVersion;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.Warehouse;
import com.ericsson.iec.prototype.MeterProtoType;
import com.ericsson.iec.service.MeterDeviceUpdateService;
import com.ericsson.iec.service.MeterDeviceUpdateServiceImpl;
import com.ericsson.iec.service.NewEqForRadiusRequestExecuter;

class IecShipmentFileProcessor implements ShipmentFileProcessor {

    private final Logger logger;
    private final boolean isRadiusActive;

    IecShipmentFileProcessor(Logger logger, boolean isRadiusActive) {
        this.logger = logger;
        this.isRadiusActive = isRadiusActive;
    }

    @Override
    public void process(ShipmentInfo shipment) throws CustomerException {
//		NewEquipmentForRadiusResponder r = new NewEquipmentForRadiusResponder();
//		r.testCreateMessage();
//		r.testService();
//		if (true)
//			return;
		CustomerException lastException = null;

		logger.info("Importing Shipment: " + shipment.getHeader().getManufacturer() + " - " + shipment.getHeader().getDeviceType() + " - " + shipment.getHeader().getBatchId());

		ProcessContext pc = new ProcessContext(shipment.getHeader());
		ShipmentFileValidator.validateHeader(pc);

		for (Device device : shipment.getBody().getDevices()) {
			try {
				logger.info("Importing meter " + device.getUniqueIdentifier());
				pc.initializeDevice(device);
				ShipmentFileValidator.validateDevice(pc);
				ShipmentFileProcessor.processDevice(pc, isRadiusActive);
				logger.info("Meter " + pc.serialId + " Created");
			} catch (CustomerException e) {
				lastException = e;
				logger.info("Import failed for meter " + pc.serialId + ": " + e.toString());
			}
		}
		if (lastException != null) {
			logger.info("Importer failed, performing rollback");
			throw lastException;
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class ProcessContext {
		// Header:
		String manufacturer;
		String customer;
		String batchId;
		Date deliveryDate;
		Date certificationDate;
		String meterModel;
		ProtoTypeObjectType meterType;
		Warehouse meterWarehouse;
		boolean isFirstBatch;

		// Device:
		String serialNumber;
		String serialId;
		String macAddress;
		String firmwareVersionName;
		String mk;
		String gak;
		String guek;
		FirmwareVersion firmwareVersion;
		MeterProtoType meterProtoType;
		GenericMeter meter;

		public ProcessContext(Header shipmentHeader) throws IecException {
			initializeHeader(shipmentHeader);
		}
		
		private void initializeHeader(Header shipmentHeader) throws AbstractConfigurationException {
			manufacturer = shipmentHeader.getManufacturer();
			customer = shipmentHeader.getCustomer();
			batchId = shipmentHeader.getBatchId();
			deliveryDate = shipmentHeader.getDeliveryDate();
			certificationDate = shipmentHeader.getCertificationDate();
			meterModel = shipmentHeader.getDeviceType(); // 061
			
			meterType = IecWarehouse.getInstance().getMeterProtoTypeFactory(meterModel).getObjectType();

			if (meterType == ProtoTypeObjectType.CELLULAR_METER)
				meterWarehouse = IecWarehouse.getInstance().getWarehouseFactory().find(Warehouses.CELLULAR_METER_WAREHOUSE); // contains validation for null
			else if (meterType == ProtoTypeObjectType.PLC_METER)
				meterWarehouse = IecWarehouse.getInstance().getWarehouseFactory().find(Warehouses.PLC_METER_WAREHOUSE); // contains validation for null
			else
				meterWarehouse = null;
			
	        String firstBatchId = IecSystemParameters.FIRST_SHIPMENT_FILE_BATCH_ID.getStringValue();
	        isFirstBatch = (firstBatchId.equals(batchId));

		}

		private void initializeDevice(Device device) throws AbstractConfigurationException {
			serialNumber = device.getSerialNumber(); // just for validation
			serialId = device.getUniqueIdentifier();
			macAddress = device.getMacAdress();
			firmwareVersionName = getFirmwareVersionName(device);

			firmwareVersion = IecWarehouse.getInstance().getFirmwareVersionFactory().findByKey(firmwareVersionName);
			meterProtoType = IecWarehouse.getInstance().getMeterProtoTypeFactory(meterModel).find(); // Throws Configuration Exception
			meter = IecWarehouse.getInstance().getMeterFactory().findBySerialId(serialId);
			
			for (Key key : device.getKeys()) { // GAK, GUEK, MK
				if (key.getName().equals("MK"))
					mk = key.getValue();
				else if (key.getName().equals("GAK"))
					gak = key.getValue();
				else if (key.getName().equals("GUEK"))
					guek = key.getValue();
			}
		}
		
		private String getFirmwareVersionName(Device device) {
			List<Attribute> attributeList = device.getFirmwareVersion();
			if (attributeList != null) {
				for (Attribute attribute : attributeList) {
					if (attribute.getName().equals("Firmware"))
						return attribute.getValue();
				}
			}
			return null;
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class ShipmentFileValidator {
		public static void validateHeader(ProcessContext pc) throws AbstractValidationException {
			CommonValidatorFactory.validateLookup(pc.manufacturer, "Meter Manufacturer", "manufacturer", CommonObjectFactory.getPlcMeterFolderType());
			CommonValidatorFactory.validateValue(pc.customer, "IEC", "Customer");
			CommonValidatorFactory.validateLookup(pc.meterModel, "Meter Model", "meterModel", CommonObjectFactory.getPlcMeterFolderType());
			CommonValidatorFactory.validateMandatoryValue(pc.deliveryDate, "deliveryDate");
			CommonValidatorFactory.validateMandatoryValue(pc.certificationDate, "certificationDate");
		}

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		public static void validateDevice(ProcessContext pc) throws AbstractValidationException, AbstractConfigurationException {
//			CommonValidatorFactory.validateMandatoryValue(pc.macAddress, "macAddress");
			CommonValidatorFactory.validateMandatoryValue(pc.mk, "mk key");
			CommonValidatorFactory.validateMandatoryValue(pc.gak, "gak key");
			CommonValidatorFactory.validateMandatoryValue(pc.guek, "guek key");
			validateMeter(pc);
			validateFirmwareVersion(pc);
			validateSerialNumber(pc);
			validateSerialId(pc);
		}

		private static void validateMeter(ProcessContext pc) throws FatalValidationException {
			if (pc.meter != null) 
				throw new FatalValidationException(OBJECT_ALREADY_EXISTS, "Meter", pc.serialId);
		}

		private static void validateFirmwareVersion(ProcessContext pc) throws AbstractValidationException {
			if (pc.firmwareVersion == null)
				throw new FatalValidationException(FIELD_NOT_FOUND_IN_LOOKUP_TABLE, "Firmware Version", pc.firmwareVersionName);
		}
		
		
		
		private static void validateSerialNumber(ProcessContext pc) throws AbstractValidationException {
			int fullYear = pc.certificationDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear();
			String year = String.valueOf(fullYear).substring(2);
			String prefix;
			String suffix;
			if (pc.isFirstBatch) {
				String manufacturerCode = pc.manufacturer.equals("KAIFA") ? "1" : "2";
				prefix = "00" + year + manufacturerCode;
				suffix = "nnnnn";
			} else {
				String meterModelCode;
				if (pc.meterModel.equals("511"))
					meterModelCode = "01";
				else if (pc.meterModel.equals("512"))
					meterModelCode = "02";
				else if (pc.meterModel.equals("742"))
					meterModelCode = "03";
				else if (pc.meterModel.equals("061"))
					meterModelCode = "11";
				else if (pc.meterModel.equals("062"))
					meterModelCode = "12";
				else if (pc.meterModel.equals("510"))
					meterModelCode = "13";
				else if (pc.meterModel.equals("741"))
					meterModelCode = "14";
				else meterModelCode = "UnknownModelCode";
				prefix = meterModelCode + year;
				suffix = "nnnnnn";
			}
			if (!(pc.serialNumber.startsWith(prefix) && pc.serialNumber.length() == 10))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_FORMAT, "Serial Number", pc.serialNumber, prefix + suffix);
		}
	
		private static void validateSerialId(ProcessContext pc) throws AbstractValidationException {
			String manufacturerShortName = pc.manufacturer.equals("KAIFA") ? "KFM" : "ZIV";
			if (!pc.serialId.equals(manufacturerShortName + pc.serialNumber))
				throw new FatalValidationException(FIELD_DOES_NOT_MATCH_FORMAT, "Serial Id (XML UniqueIdentifier)", pc.serialId, manufacturerShortName + "<SerialNumber>");
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	private static class ShipmentFileProcessor {
		
		public static void processDevice(ProcessContext pc, boolean isRadiusActive) throws IecException {
			createMeter(pc);
            sendNewEqForRadius(pc, isRadiusActive);
			setMeterSecurity(pc);
		}

		private static NewEqForRadiusRequestExecuter newEqForRadiusRequestExecuter = new NewEqForRadiusRequestExecuter();
		private static MeterDeviceUpdateService meterDeviceUpdateService = new MeterDeviceUpdateServiceImpl();
		
        private static void sendNewEqForRadius(ProcessContext pc, Boolean isRadiusActive) throws IecException {
            if (isRadiusActive) {
                if (ProtoTypeObjectType.CELLULAR_METER.equals(pc.meterType)) {
        			String meterAddress = newEqForRadiusRequestExecuter.getMeterAddressFromRadius(pc.serialId);
        			meterDeviceUpdateService.updateIpAddress(pc.meter.getDevice(), meterAddress);
                }
            }
    		// Create MDUS Async Request:
//          NewEqForRadiusRequestBuilder newEqForRadiusRequestBuilder = new NewEqForRadiusRequestBuilder();
//          SapResponseMessage sapResponseMessage = newEqForRadiusRequestBuilder.build(pc.serialId);
//          new IecClassInjector().getMessageSender().sendMessage(sapResponseMessage);
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		@SuppressWarnings("deprecation")
		private static void createMeter(ProcessContext pc) throws IecException {
			try {
				pc.meterProtoType.setSerialId(pc.serialId);
				pc.meterProtoType.setMacAddress(pc.macAddress);
				pc.meterProtoType.setBatch(pc.batchId);
				pc.meterProtoType.setDeliveryDate(pc.deliveryDate);
				pc.meterProtoType.setFirmwareVersion(pc.firmwareVersion);
				pc.meterProtoType.setWarehouse(pc.meterWarehouse);
				pc.meterProtoType.setActiveDate(new Date(0));
			} catch (Exception e) {
				throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", pc.serialId);
			}

			try {
				pc.meter = pc.meterProtoType.save();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_CREATE_OBJECT, "Meter", pc.serialId);
			}
			
			// Update Additional Attributes:
			try {
				pc.meter.getDevice().getDevice().updateLastLogbook(new Date());
				pc.meter.getDevice().getDevice().updateLastReading(new Date());
//				pc.meter.setConfigurationDate(new Date());
			} catch (Exception e) {
				throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "Meter", pc.serialId);
			}
			
			try {
				pc.meter.saveChanges();
			} catch (BusinessException | SQLException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Meter", pc.serialId);
			}
		}
		
		private static void setMeterSecurity(ProcessContext pc) throws AbstractExecutionException {
			com.energyict.mdw.core.Device device = pc.meter.getDevice().getDevice();
			DeviceShadow shadow = device.getShadow();
			
			TypedProperties typedProperties = shadow.getProtocolProperties();
			typedProperties.setProperty("MeterMasterKey", pc.mk);

			ShadowList<SecurityProperties> shadowList = shadow.getSecurityProperties(); // not required: shadow.setSecurityProperties(shadowList);
			for (SecurityProperties props : shadowList) {
			    props.set("EncryptionKey", pc.guek);
			    props.set("AuthenticationKey", pc.gak);
			}

			try {
				device.update(shadow);
			} catch (SQLException | BusinessException e) {
				throw new FatalExecutionException(e, CANNOT_UPDATE_OBJECT, "Meter Device Security Set", pc.serialId);
			}      
		}
	}
}

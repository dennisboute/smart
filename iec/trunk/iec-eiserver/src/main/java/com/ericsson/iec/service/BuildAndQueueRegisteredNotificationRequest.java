package com.ericsson.iec.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import com.energyict.mdus.core.ResponseMessage;
import com.energyict.mdw.service.ServiceRequest;
import com.energyict.projects.common.model.processcase.exceptionhandling.AbstractServiceTransitionHandlerWithExceptionHandling;
import com.ericsson.iec.core.exception.FatalExecutionException;
import com.ericsson.iec.core.exception.IecException;
import com.ericsson.iec.mdus.IecClassInjector;
import com.energyict.mdus.core.SapResponseMessage;

import static com.ericsson.iec.core.exception.IecExceptionReference.ExecutionExceptionMessage.*;
import com.ericsson.iec.model.GenericMeter;
import com.ericsson.iec.model.workflow.MeterRegistrationNocNotification;


public class BuildAndQueueRegisteredNotificationRequest extends AbstractServiceTransitionHandlerWithExceptionHandling<MeterRegistrationNocNotification>{

	public static final String TRANSITION_NAME = "Build And Queue Registered Notification Request";
		
	public BuildAndQueueRegisteredNotificationRequest(Logger logger) {
		super(logger);
	}

	@Override
	protected void doProcess(MeterRegistrationNocNotification meterRegistrationNocNotification) throws IecException {
		try {
			GenericMeter genericMeter = meterRegistrationNocNotification.getMeter();
			RegisteredNotificationRequestBuilder builder = new RegisteredNotificationRequestBuilder();
			String utilitiesDeviceId = genericMeter.getSapId();
			String serialNumber = genericMeter.getDevice().getDevice().getSerialNumber();
			String manufactoringYear = new SimpleDateFormat("yyyy").format(genericMeter.getDeliveryDate());
			String meterFirmware = genericMeter.getFirmwareVersion().getFirmwareVersion();
			ResponseMessage responseMessage = builder.build(utilitiesDeviceId, serialNumber, manufactoringYear , meterFirmware);
			ServiceRequest serviceRequest = ((SapResponseMessage)responseMessage).getServiceRequest();
			meterRegistrationNocNotification.setServiceRequest(serviceRequest);
			new IecClassInjector().getMessageSender().sendMessage(responseMessage);
		} catch (IecException e) {
			throw e;
		} catch(Exception e) {
			e.printStackTrace();
			throw new FatalExecutionException(e, MESSAGE_PROCESSING_FAILED, "", "");
		}
	}

	@Override
	protected String getTransitionName() {
		return TRANSITION_NAME;
	}

}

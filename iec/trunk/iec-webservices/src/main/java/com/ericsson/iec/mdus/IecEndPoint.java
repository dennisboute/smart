package com.ericsson.iec.mdus;

import java.sql.SQLException;

import javax.xml.bind.JAXBElement;

import com.energyict.cbo.BusinessException;
import com.energyict.cpo.Transaction;
import com.energyict.mdus.core.model.MdusWarehouse;
import com.energyict.mdus.core.services.AbstractAsynchronousSmartMeterService;
import com.energyict.mdus.core.services.AsynchronousWebServiceParameters;
import com.energyict.projects.common.exceptions.CustomerException;

public abstract class IecEndPoint extends AbstractAsynchronousSmartMeterService {

	@Override
	protected String marshall(JAXBElement xml) throws CustomerException {
		return IecMarshallerHelper.getInstance().marshall(xml);
	}

	// Same as super.process, but throw exception to be returned by tomcat
	protected Void processForTomcat(final AsynchronousWebServiceParameters parameters) throws Exception
	{
		try {
			return MdusWarehouse.getCurrent().getMeteringWarehouse().execute(new Transaction<Void>() {
				@Override
				public Void doExecute() throws BusinessException, SQLException {
					return doProcess(parameters);
				}
			});
		} catch (Exception e) {
            logError(e);
            throw e;
		}
	}

}

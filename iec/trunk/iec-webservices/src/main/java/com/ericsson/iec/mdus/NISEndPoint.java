package com.ericsson.iec.mdus;

import static com.ericsson.iec.mdus.MdusWebservice.PREMISE_REQUEST;
import static com.ericsson.iec.mdus.MdusWebservice.TRANSFORMER_REQUEST;
import static com.ericsson.iec.mdus.MdusWebservice.TRANSFORMER_STATION_REQUEST;

import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.energyict.mdus.core.services.AsynchronousWebServiceParameters;

import nismdm01.interfaces.mdm.iec.MessageFault;
import nismdm01.interfaces.mdm.iec.ObjectFactory;
import nismdm01.interfaces.mdm.iec.PremiseRequest;
import nismdm01.interfaces.mdm.iec.PremiseResponse;
import nismdm01.interfaces.mdm.iec.TransformerRequest;
import nismdm01.interfaces.mdm.iec.TransformerResponse;
import nismdm01.interfaces.mdm.iec.TransformerStationRequest;
import nismdm01.interfaces.mdm.iec.TransformerStationResponse;

@WebService(endpointInterface = "nismdm01.interfaces.mdm.iec.NisMdm01PortType")
public class NISEndPoint extends IecEndPoint {

	public TransformerStationResponse setTransformerStation(TransformerStationRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<TransformerStationRequest> element = new JAXBElement<TransformerStationRequest>(new QName("ts", "TransformerStationRequest"), TransformerStationRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(TRANSFORMER_STATION_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildTransformerStationResponse("ERROR", e.toString());
		}

		return buildTransformerStationResponse("OK", "Also OK");
	}
	
	private TransformerStationResponse buildTransformerStationResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 		

		TransformerStationResponse response = factory.createTransformerStationResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}

	public PremiseResponse setPremise(PremiseRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<PremiseRequest> element = new JAXBElement<PremiseRequest>(new QName("pr", "PremiseRequest"), PremiseRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(PREMISE_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildPremiseResponse("ERROR", e.toString());
		}
		return buildPremiseResponse("OK", "Also OK");
	}
	
	private PremiseResponse buildPremiseResponse(String statusCode, String statusMessage) {
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 
		ObjectFactory factory = new ObjectFactory();

		PremiseResponse response = factory.createPremiseResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}

	public TransformerResponse setTransformer(TransformerRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<TransformerRequest> element = new JAXBElement<TransformerRequest>(new QName("tr", "TransformerRequest"), TransformerRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(TRANSFORMER_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildTransformerResponse("ERROR", e.toString());
		}

		return buildTransformerResponse("OK", "Really OK");

	}
	
	private TransformerResponse buildTransformerResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 
		
		TransformerResponse response = factory.createTransformerResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}
	
	

	

}

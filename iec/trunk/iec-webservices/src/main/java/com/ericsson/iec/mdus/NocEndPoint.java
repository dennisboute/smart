package com.ericsson.iec.mdus;

import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.energyict.mdus.core.services.AsynchronousWebServiceParameters;

import nocmdm01.interfaces.mdm.iec.DCAssetRequest;
import nocmdm01.interfaces.mdm.iec.DCAssetResponse;
import nocmdm01.interfaces.mdm.iec.DCEventReportRequest;
import nocmdm01.interfaces.mdm.iec.DCEventReportResponse;
import nocmdm01.interfaces.mdm.iec.MessageFault;
import nocmdm01.interfaces.mdm.iec.ObjectFactory;

import static com.ericsson.iec.mdus.MdusWebservice.*;

@WebService(endpointInterface = "nocmdm01.interfaces.mdm.iec.NocMdm01PortType")
public class NocEndPoint extends IecEndPoint {

	public DCAssetResponse setDCAsset(DCAssetRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<DCAssetRequest> element = new JAXBElement<DCAssetRequest>(new QName("dc", "DCAssetRequest"), DCAssetRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(DC_ASSET_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildDCAssetResponse("ERROR", e.toString());
		}
		return buildDCAssetResponse("OK", "Also OK");
	 }

	private DCAssetResponse buildDCAssetResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 		

		DCAssetResponse response = factory.createDCAssetResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}
	
	public DCEventReportResponse setDCEventReport(DCEventReportRequest request) throws MessageFault {
		String uuid = request.getMessageHeader().getMessageID();
		JAXBElement<DCEventReportRequest> element = new JAXBElement<DCEventReportRequest>(new QName("DCEventReportRequest"), DCEventReportRequest.class, request);
		try {
			processForTomcat(new AsynchronousWebServiceParameters(DC_EVENT_REPORT_REQUEST, element, uuid));
		} catch (Exception e) {
			return buildDCEventReportResponse("ERROR", e.toString());
		}
		return buildDCEventReportResponse("OK", "Also OK");
		
	}
	
	private DCEventReportResponse buildDCEventReportResponse(String statusCode, String statusMessage) {
		ObjectFactory factory = new ObjectFactory();
		core.mdm.iec.ObjectFactory coreFactory = new core.mdm.iec.ObjectFactory(); 		

		DCEventReportResponse response = factory.createDCEventReportResponse();
		response.setMessageResponse(coreFactory.createMessageResponse());
		response.getMessageResponse().setStatusCode(statusCode);
		response.getMessageResponse().setStatusMessage(statusMessage);
		return response;
	}
	

	

}
